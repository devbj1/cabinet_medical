<?php

namespace AppBundle\Services;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;


class Authentificated
{


    public function userConnect(){
        $session = new Session();
        $connect = $session->get('authenticated');

        if ($connect != true){
            $url = $this->generateUrl('login');
            return new RedirectResponse($url);
        }
    }

}