<?php

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     * @Method({"GET", "POST"})
     */
    public function loginAction(Request $request)
    {
        if ($request->getMethod() == 'GET') {
            return $this->render('security/login.html.twig', [
                'error' => "",
            ]);
        }

        if ($request->getMethod() == 'POST') {
            $login = $request->get("login");
            $mdp = $request->get("mdp");

            $em = $this->getDoctrine()->getManager();
            // get the login error if there is one
            $identifyValide = $em->getRepository("AppBundle:User")->findBy(array('login' => $login, 'mdp' => $mdp));
            $session = new Session();
            if ($identifyValide != []) {
                $session->set("authenticated", true);
                $session->set("id", $identifyValide[0]->getId());
                $session->set("nom", $identifyValide[0]->getNom());
                $session->set("prenom", $identifyValide[0]->getPrenom());
                $session->set("codefonction", $identifyValide[0]->getFonction()->getCodeFonction());
                $session->set("fonction", $identifyValide[0]->getFonction()->getLibelleFonction());
                $session->set("profilurl", $identifyValide[0]->getPhoto());

                return $this->redirectToRoute('homepage');
            } else {
                $session->set("authenticated", false);
                return $this->render('security/login.html.twig', [
                    'error' => "Identifiant incorrecte",
                ]);
            }

        }
    }


    public function userConnect()
    {
        $session = new Session();
        $connect = $session->get('authenticated');
        if ($connect != true) {
            $url = $this->generateUrl('login');
            $response = new RedirectResponse($url);
            $response->send();
            return;
        }
    }

    /**
     * @Route("/out", name="out")
     * @Method({"GET", "POST"})
     */
    public function outAction()
    {
        //dump("Ok");die();
        $session = new Session();
        $session->set("nom","");
        $session->set("prenom", "");
        $session->set("fonction", "");
        $session->set("authenticated", "");
        return $this->render('security/login.html.twig', [
            'error' => "",
        ]);

    }

    /**
     * @Route("/profil", name="profil")
     * @Method({"GET", "POST"})
     */
    public function updateProfilAction(Request $request)
    {
        //dump("Ok");die();
        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));
        $session = new Session();
        $user = $em->getRepository('AppBundle:User')->find($session->get("id"));
        if ($request->getMethod() === "POST"){
            if ($request->get('mdp') <> $request->get('confmdp')){
                $session->getFlashBag()->add('error', 'Les mots de passe ne sont pas identique !');
            }else{
                $user->setNom( $request->get('nom'));
                $user->setPrenom($request->get('prenom'));
                $user->setMdp($request->get('mdp'));
                $em->flush();
                if ($user->getId() == 0) {
                    $session->getFlashBag()->add('error', 'Erreur mise a jour !');
                } else {
                    $session->getFlashBag()->add('success', 'Mise a jour effectué avec succès !');
                }

            }   
            
        }
        $nom = $user->getNom();
        $prenom = $user->getPrenom();
        $mdp = $user->getMdp();

        return $this->render('security/edit.html.twig', compact('nom','prenom','mdp','consultationsEnCours'));

    }


}