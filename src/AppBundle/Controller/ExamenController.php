<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Examen;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Examan controller.
 *
 * @Route("examen")
 */
class ExamenController extends Controller
{
    /**
     * Lists all examan entities.
     *
     * @Route("/", name="examen_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $examens = $em->getRepository('AppBundle:Examen')->findBy(array('deleted'=>false));

        return $this->render('examen/index.html.twig', array(
            'examens' => $examens,
        ));
    }

    /**
     * Creates a new examan entity.
     *
     * @Route("/new", name="examen_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $examan = new Examen();
        $form = $this->createForm('AppBundle\Form\ExamenType', $examan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();
            $em = $this->getDoctrine()->getManager();
            $em->persist($examan);
            $em->flush();

            if ($examan->getId() == 0){
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            }else{
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('examen_index');
        }

        return $this->render('examen/new.html.twig', array(
            'examan' => $examan,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a examan entity.
     *
     * @Route("/{id}", name="examen_show")
     * @Method("GET")
     */
    public function showAction(Examen $examan)
    {
        $deleteForm = $this->createDeleteForm($examan);

        return $this->render('examen/show.html.twig', array(
            'examan' => $examan,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing examan entity.
     *
     * @Route("/{id}/edit", name="examen_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Examen $examan)
    {
        $deleteForm = $this->createDeleteForm($examan);
        $editForm = $this->createForm('AppBundle\Form\ExamenType', $examan);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();
            $this->getDoctrine()->getManager()->flush();
            if ($examan->getId() == 0){
                $session->getFlashBag()->add('error', 'Erreur modification !');
            }else{
                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
            }
            return $this->redirectToRoute('examen_index');
        }

        return $this->render('examen/edit.html.twig', array(
            'examan' => $examan,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a examan entity.
     *
     * @Route("/{id}", name="examen_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Examen $examan)
    {
        $form = $this->createDeleteForm($examan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $examan->setDeleted(true);
            $this->getDoctrine()->getManager()->flush();
            if ($examan->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur suppression !');
            } else {
                $session->getFlashBag()->add('success', 'Suppression effectué avec succès !');
            }
        }

        return $this->redirectToRoute('examen_index');
    }

    /**
     * Creates a form to delete a examan entity.
     *
     * @param Examen $examan The examan entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Examen $examan)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('examen_delete', array('id' => $examan->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
