<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{

    public function userConnect()
    {
        $session = new Session();
        $connect = $session->get('authenticated');
        if ($connect != true) {
            $url = $this->generateUrl('login');
            $response = new RedirectResponse($url);
            $response->send();
            return;
        }
    }

    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->userConnect();

        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));

        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('user/index.html.twig', array(
            'users' => $users,
            'consultationsEnCours' => $consultationsEnCours
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->userConnect();

        $user = new User();
        $em = $this->getDoctrine()->getManager();
        //$fonction = $em->getRepository('AppBundle:Fonction')->findAll();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();
            $em->persist($user);
            $em->flush();

            if ($user->getId() == 0){
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            }else{
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
            'consultationsEnCours' => $consultationsEnCours
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $this->userConnect();

        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();
            $this->getDoctrine()->getManager()->flush();
            if ($user->getId() == 0){
                $session->getFlashBag()->add('error', 'Erreur modification !');
            }else{
                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
            }
            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'consulationsEnCours' => $consultationsEnCours
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
