<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Medicament;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;


class DefaultController extends Controller
{

    /**
     * @Route("/calendar", name="cal")
     */
    public function calendarAction()
    {
        $snappy = $this->get('knp_snappy.pdf');

        $em = $this->getDoctrine()->getManager();
        $consultation = $em->getRepository('AppBundle:Consultation')->find(21);
        $consulMedicaments = $em->getRepository('AppBundle:ConsulMedicament')->findBy(array('consultation' => $consultation));
        $parametre = $em->getRepository('AppBundle:Parametre')->find(1);
        if ($consultation->getPatient()->getDateNaisPatient() != null) {
            $agePatient = date_format(new \DateTime("now"), ('Y')) - date_format($consultation->getPatient()->getDateNaisPatient()
                    , ('Y')) . " ans";
        } else {
            $agePatient = "";
        }

        $codebarre = $consultation->getMedecin()->getNom() . ' ' . $consultation->getMedecin()->getPrenom() . ' Contact : ' . $consultation->getMedecin()->getTel();

        //$qrCode = new QrCode($consultation->getPatient()->getNomPatient().' '.$consultation->getPatient()->getPrenomPatient());

        //$qrCode = $qrCodeFactory->create('QR Code', ['size' => 200]);
        //header('Content-Type: '.$qrCode->getContentType());
        //echo $qrCode->writeString();
        //die();
        // dump($consulMedicaments);
        // die();
        $html = $this->renderView('consultation/ordonnance.html.twig', compact('consultation',
            'consulMedicaments', 'parametre', 'agePatient', 'codebarre'), array(
            "title" => "Hello World !"
        ));

        $filename = 'myFirstSnappyPDF';

        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );

        // replace this example code with whatever you need


        //return $this->render('dashbord/calen.html.twig');
        //return $this->render('default/index.html.twig', [
        //    'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        //]);
    }


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {

        // replace this example code with whatever you need

        $this->userConnect();

        $em = $this->getDoctrine();
        $patients = $em->getRepository('AppBundle:Patient')->findBy(array('deleted' => 0));
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));
        $consultations = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => 0));
        $examens = $em->getRepository('AppBundle:Examen')->findBy(array('deleted' => 0));
        $medicaments = $em->getRepository('AppBundle:Medicament')->findBy(array('deleted' => 0));
        $nbrPatient = count($patients);
        $nbrConsultation = count($consultations);
        $nbrExamen = count($examens);
        $nbrMedicament = count($medicaments);
        $consultations = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'medecin' => !null));
        $patients = $em->getRepository('AppBundle:Patient')->findBy(array('deleted' => false));

        return $this->render('dashbord/index.html.twig', compact('nbrPatient', 'nbrConsultation', 'nbrExamen',
            'nbrMedicament','consultations','patients','consultationsEnCours'));
        //return $this->render('default/index.html.twig', [
        //    'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        //]);
    }


    public function userConnect()
    {
        $session = new Session();
        $connect = $session->get('authenticated');
        if ($connect != true) {
            $url = $this->generateUrl('login');
            $response = new RedirectResponse($url);
            $response->send();
            return;
        }
    }


    /**
     * Lists charges entities.
     *
     * @Route("/charge/medicament", name="charge_medicament_index")
     * @Method("GET")
     */
    public function indexChargeMedicamentAction()
    {

        $em = $this->getDoctrine()->getManager();

        // Nom du fichier à ouvrir
        $fichier = file("C:\Users\BEKPON\Downloads\CIS_bdpm.txt");
        // Nombre total de ligne du fichier
        $total = count($fichier);
        for ($i = 0; $i < $total; $i++) {
            $medicament = new Medicament();
            // On affiche ligne par ligne le contenu du fichier
            // avec la fonction nl2br pour ajouter les sauts de lignes
            $v = strstr($fichier[$i], ",", true);
            $v = explode('	', $v);
            $nbr = 0;
            $code = "";
            $libelle = "";
            foreach ($v as $mot) {
                if ($nbr == 0) {
                    //echo "Ligne #<b>{$mot}</b> : <br />\n";
                    $code = $mot;
                } else {
                    $libelle = $libelle . ' ' . $mot;
                }
                $nbr = 1;
            }

//            dump(substr($libelle,0,2));
            /*    if($libelle != ''){
                    if($libelle[0].''.$libelle[1] ===' b'){
                        str_replace(' b','',$libelle);
                    }
                }*/

            //dump(trim($code));
            //dump(trim($libelle));
            if (trim($code) != "63152345") {
                if (trim($code) != "66513085") {
                    if (trim($code) != "64332894") {
                        if (trim($code) != "61815891") {
                            if (trim($code) != "69636666") {
                                if (trim($code) != "62809126") {
                                    if (trim($code) != "61995884") {
                                        if (trim($code) != "61054685") {
                                            if (trim($code) != "60486279") {
                                                if (trim($code) != "64130608") {
                                                    if (trim($code) != "62679768") {
                                                        $findLibelle = $em->getRepository('AppBundle:Medicament')->findBy(array('libelleMedicament' => trim($libelle)));
                                                        $findCode = $em->getRepository('AppBundle:Medicament')->findBy(array('codeMedicament' => trim($code)));
                                                        if ($findLibelle == null and $findCode == null) {
                                                            $medicament->setCodeMedicament(trim($code));
                                                            $medicament->setLibelleMedicament(html_entity_decode(htmlentities(trim($libelle))));
                                                            $em->persist($medicament);
                                                            $em->flush();
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }

                }
            }

            //echo nl2br($fichier[$i]);

        }
        dump('ok');
        die();

        // OU en ayant les numéros de ligne et l'ajout des sauts de lignes

        // Affiche toute les lignes du tableau
        // avec les numéros de ligne
        $a = ',';
        foreach ($fichier as $line_num => $line) {
            $v = strstr($line_num, $a);
            $v = explode(' ', $v);
            echo "Ligne #<b>{$line_num}</b> : " . htmlspecialchars($line) .
                "<br />\n";
        }
        die();

        $fichier = "test.txt";
        //ouverture en lecture et modification
        $text = fopen($fichier, 'r') or die("Fichier manquant");
        // la variable contenu reçoit tout le texte du document
        $contenu = file_get_contents($fichier);
        echo $contenu;
        // chercher la variable a
        $a = '?';
        // retourne la chaine après la chaine cherchée
        while (!feof($text)) {
            $v = strstr($contenu, $a);
            // retourne un tableau de chaînes, chacune d'elle étant une sous-chaîne de la variable contenu extraite en utilisant le séparateur delimiter espace
            $v = explode(' ', $v);
            // Supprimer le caractere '?'
            $remplace = str_replace('?', '', $v[0]);
            // Afficher premiere chaire //
            echo $v[0];
            // permet de chercher une sous chaine et de la remplacer. Dans notre cas on chaine la sous chaine $a et on la remplace par la sous chaine $v dans la chaine $contenu
            $contenuMod = str_replace($v[0], '<input type="text" id="' . $remplace . '">', $contenu);
            echo $contenuMod;
        }
        fclose($text);
        //$consultations = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false));
        /*$diagnostics = $em->getRepository('AppBundle:Diagnostic')->findBy(array('deleted' => false));
        $examens = $em->getRepository('AppBundle:Examen')->findBy(array('deleted' => false));
        $examenParaCliniques = $em->getRepository('AppBundle:ExamenParaMedical')->findBy(array('deleted' => false));
        $medicaments = $em->getRepository('AppBundle:Medicament')->findBy(array('deleted' => false));
        $formes = $em->getRepository('AppBundle:Forme')->findBy(array('deleted' => false));
        $posologies = $em->getRepository('AppBundle:Posologie')->findBy(array('deleted' => false));
        $durees = $em->getRepository('AppBundle:DureeTraitement')->findBy(array('deleted' => false));*/
        //$patients = $em->getRepository('AppBundle:Patient')->findBy(array('deleted' => false));


        return $this->render('statistique/index.html.twig');

    }

//
//    /**
//     * @Route("/pdf", name="HN")
//     */
//    public function pdfAction()
//    {
//        $html = $this->renderView('consultation/list_patient.html.twig');
//
//        return new PdfResponse(
//            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
//            'file.pdf'
//        );
//    }


}
