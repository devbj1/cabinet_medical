<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Consultation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Diagnostic controller.
 *
 * @Route("consultation")
 */
class ConsultationController extends Controller
{

    public function userConnect()
    {
        $session = new Session();
        $connect = $session->get('authenticated');
        if ($connect != true) {
            $url = $this->generateUrl('login');
            $response = new RedirectResponse($url);
            $response->send();
            return;
        }
    }

    /**
     * Lists all consultation entities.
     *
     * @Route("/", name="consultation_index")
     * @Method("GET")
     */
    public function indexAction()
    {

        $this->userConnect();

        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateFinConsul' => null));

        //$consultations = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'medecin' => !null));

        $consultations = $em->getRepository('AppBundle:Consultation')->listConsultationOrdonnance();
        //dump($consultations);die();
        /*$diagnostics = $em->getRepository('AppBundle:Diagnostic')->findBy(array('deleted' => false));
        $examens = $em->getRepository('AppBundle:Examen')->findBy(array('deleted' => false));
        $examenParaCliniques = $em->getRepository('AppBundle:ExamenParaMedical')->findBy(array('deleted' => false));
        $medicaments = $em->getRepository('AppBundle:Medicament')->findBy(array('deleted' => false));
        $formes = $em->getRepository('AppBundle:Forme')->findBy(array('deleted' => false));
        $posologies = $em->getRepository('AppBundle:Posologie')->findBy(array('deleted' => false));
        $durees = $em->getRepository('AppBundle:DureeTraitement')->findBy(array('deleted' => false));*/
        $patients = $em->getRepository('AppBundle:Patient')->findBy(array('deleted' => false));
       // dump($consultations);die();
        return $this->render('consultation/index.html.twig',compact('consultations','patients','consultationsEnCours'));
    }

    /**
     * Lists all consultation entities.
     *
     * @Route("/statistique", name="consultation_statistique_index")
     * @Method("GET")
     */
    public function indexStatistiqueAction()
    {

        $this->userConnect();

        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));

        //$consultations = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false));
        /*$diagnostics = $em->getRepository('AppBundle:Diagnostic')->findBy(array('deleted' => false));
        $examens = $em->getRepository('AppBundle:Examen')->findBy(array('deleted' => false));
        $examenParaCliniques = $em->getRepository('AppBundle:ExamenParaMedical')->findBy(array('deleted' => false));
        $medicaments = $em->getRepository('AppBundle:Medicament')->findBy(array('deleted' => false));
        $formes = $em->getRepository('AppBundle:Forme')->findBy(array('deleted' => false));
        $posologies = $em->getRepository('AppBundle:Posologie')->findBy(array('deleted' => false));
        $durees = $em->getRepository('AppBundle:DureeTraitement')->findBy(array('deleted' => false));*/
        //$patients = $em->getRepository('AppBundle:Patient')->findBy(array('deleted' => false));
        return $this->render('statistique/index.html.twig',compact('consultationsEnCours'));
    }

    /**
     * Lists all begin consultation entities.
     *
     * @Route("/debut", name="consultation_debut_index")
     * @Method("GET")
     */
    public function indexBeginAction()
    {

        $this->userConnect();

        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));
        $consultations = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));
        /*$diagnostics = $em->getRepository('AppBundle:Diagnostic')->findBy(array('deleted' => false));
        $examens = $em->getRepository('AppBundle:Examen')->findBy(array('deleted' => false));
        $examenParaCliniques = $em->getRepository('AppBundle:ExamenParaMedical')->findBy(array('deleted' => false));
        $medicaments = $em->getRepository('AppBundle:Medicament')->findBy(array('deleted' => false));
        $formes = $em->getRepository('AppBundle:Forme')->findBy(array('deleted' => false));
        $posologies = $em->getRepository('AppBundle:Posologie')->findBy(array('deleted' => false));
        $durees = $em->getRepository('AppBundle:DureeTraitement')->findBy(array('deleted' => false));*/
        $patients = $em->getRepository('AppBundle:Patient')->findBy(array('deleted' => false));
        $typePatients = $em->getRepository('AppBundle:TypePatient')->findBy(array('deleted' => false));
        return $this->render('consultation/index_begin.html.twig',compact('consultations','patients','consultationsEnCours','typePatients'));
    }


    /**
     * Creates a new consultation entity.
     *
     * @Route("/new", name="consultation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction()
    {
        $this->userConnect();

        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));
        $motifs = $em->getRepository('AppBundle:Motif')->findBy(array('deleted' => false));
        $diagnostics = $em->getRepository('AppBundle:Diagnostic')->findBy(array('deleted' => false));
        $examens = $em->getRepository('AppBundle:Examen')->findBy(array('deleted' => false));
        $examenParaCliniques = $em->getRepository('AppBundle:ExamenParaMedical')->findBy(array('deleted' => false));
        $medicaments = $em->getRepository('AppBundle:Medicament')->findBy(array('deleted' => false));
        $formes = $em->getRepository('AppBundle:Forme')->findBy(array('deleted' => false));
        $posologies = $em->getRepository('AppBundle:Posologie')->findBy(array('deleted' => false));
        $durees = $em->getRepository('AppBundle:DureeTraitement')->findBy(array('deleted' => false));

        return $this->render('consultation/new.html.twig',compact('motifs','diagnostics','examens','medicaments'
            ,'formes','posologies','durees','examenParaCliniques','consultationsEnCours'));
    }


//    /**
//     * Displays a form to edit an existing consultation entity.
//     *
//     * @Route("/{id}/edit", name="consultation_edit")
//     * @Method({"GET", "POST"})
//     */
//    public function editAction(Request $request, Consultation $consultation)
//    {
//        $deleteForm = $this->createDeleteForm($diagnostic);
//        $editForm = $this->createForm('AppBundle\Form\DiagnosticType', $diagnostic);
//        $editForm->handleRequest($request);
//
//        if ($editForm->isSubmitted() && $editForm->isValid()) {
//            $session = new Session();
//            $this->getDoctrine()->getManager()->flush();
//            if ($diagnostic->getId() == 0) {
//                $session->getFlashBag()->add('error', 'Erreur modification !');
//            } else {
//                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
//            }
//            return $this->redirectToRoute('diagnostic_index');
//        }
//
//        return $this->render('diagnostic/edit.html.twig', array(
//            'diagnostic' => $diagnostic,
//            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
//        ));
//    }
//
//    /**
//     * Deletes a consultation entity.
//     *
//     * @Route("/{id}", name="consultation_delete")
//     * @Method("DELETE")
//     */
//    public function deleteAction(Request $request, Consultation $consultation)
//    {
//        $form = $this->createDeleteForm($diagnostic);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $session = new Session();
//            $diagnostic->setDeleted(true);
//            $this->getDoctrine()->getManager()->flush();
//            if ($diagnostic->getId() == 0) {
//                $session->getFlashBag()->add('error', 'Erreur suppression !');
//            } else {
//                $session->getFlashBag()->add('success', 'Suppression effectué avec succès !');
//            }
//        }
//
//        return $this->redirectToRoute('diagnostic_index');
//    }

}
