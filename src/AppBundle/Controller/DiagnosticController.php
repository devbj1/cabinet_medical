<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Diagnostic;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Diagnostic controller.
 *
 * @Route("diagnostic")
 */
class DiagnosticController extends Controller
{
    /**
     * Lists all diagnostic entities.
     *
     * @Route("/", name="diagnostic_index")
     * @Method("GET")
     */
    public function indexAction()
    {

       // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));
        $diagnostics = $em->getRepository('AppBundle:Diagnostic')->findBy(array('deleted' => false));

        return $this->render('diagnostic/index.html.twig', compact('diagnostics','consultationsEnCours'));
    }

    /**
     * Creates a new diagnostic entity.
     *
     * @Route("/new", name="diagnostic_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $diagnostic = new Diagnostic();
        $form = $this->createForm('AppBundle\Form\DiagnosticType', $diagnostic);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));
        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            $em->persist($diagnostic);
            $em->flush();

            if ($diagnostic->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('diagnostic_index');
        }

        return $this->render('diagnostic/new.html.twig', array(
            'diagnostic' => $diagnostic,
            'form' => $form->createView(),
            'consultationEnCours' => $consultationsEnCours
        ));
    }

    /**
     * Finds and displays a diagnostic entity.
     *
     * @Route("/{id}", name="diagnostic_show")
     * @Method("GET")
     */
    public function showAction(Diagnostic $diagnostic)
    {
        $deleteForm = $this->createDeleteForm($diagnostic);

        return $this->render('diagnostic/show.html.twig', array(
            'diagnostic' => $diagnostic,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing diagnostic entity.
     *
     * @Route("/{id}/edit", name="diagnostic_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Diagnostic $diagnostic)
    {
        $deleteForm = $this->createDeleteForm($diagnostic);
        $editForm = $this->createForm('AppBundle\Form\DiagnosticType', $diagnostic);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();
            $this->getDoctrine()->getManager()->flush();
            if ($diagnostic->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
            }
            return $this->redirectToRoute('diagnostic_index');
        }

        return $this->render('diagnostic/edit.html.twig', array(
            'diagnostic' => $diagnostic,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a diagnostic entity.
     *
     * @Route("/{id}", name="diagnostic_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $session = new Session();
        //$diagnostic = new Diagnostic();
        $em = $this->getDoctrine()->getManager();
        $diagnostic = $em->getRepository('AppBundle:Diagnostic')->find($id);
        //dump($diagnostic);die();
        $diagnostic->setDeleted(true);
        $em->persist($diagnostic);
        $em->flush();
        if ($diagnostic->getId() == 0) {
            $session->getFlashBag()->add('error', 'Erreur suppression !');
        } else {
            $session->getFlashBag()->add('success', 'Suppression effectué avec succès !');
        }
        return $this->redirectToRoute('diagnostic_index');

        //return $this->redirectToRoute('diagnostic_index');
    }

    /**
     * Creates a form to delete a diagnostic entity.
     *
     * @param Diagnostic $diagnostic The diagnostic entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Diagnostic $diagnostic)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('diagnostic_delete', array('id' => $diagnostic->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
