<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ExamenParaMedical;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Examenparamedical controller.
 *
 * @Route("examenparamedical")
 */
class ExamenParaMedicalController extends Controller
{
    /**
     * Lists all examenParaMedical entities.
     *
     * @Route("/", name="examenparamedical_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $examenParaMedicals = $em->getRepository('AppBundle:ExamenParaMedical')->findBy(array('deleted'=>false));

        return $this->render('examenparamedical/index.html.twig', array(
            'examenParaMedicals' => $examenParaMedicals,
        ));
    }

    /**
     * Creates a new examenParaMedical entity.
     *
     * @Route("/new", name="examenparamedical_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $examenParaMedical = new Examenparamedical();
        $form = $this->createForm('AppBundle\Form\ExamenParaMedicalType', $examenParaMedical);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($examenParaMedical);
            $em->flush();
            $session = new Session();
            if ($examenParaMedical->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('examenparamedical_index');
        }

        return $this->render('examenparamedical/new.html.twig', array(
            'examenParaMedical' => $examenParaMedical,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a examenParaMedical entity.
     *
     * @Route("/{id}", name="examenparamedical_show")
     * @Method("GET")
     */
    public function showAction(ExamenParaMedical $examenParaMedical)
    {
        $deleteForm = $this->createDeleteForm($examenParaMedical);

        return $this->render('examenparamedical/show.html.twig', array(
            'examenParaMedical' => $examenParaMedical,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing examenParaMedical entity.
     *
     * @Route("/{id}/edit", name="examenparamedical_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ExamenParaMedical $examenParaMedical)
    {
        $deleteForm = $this->createDeleteForm($examenParaMedical);
        $editForm = $this->createForm('AppBundle\Form\ExamenParaMedicalType', $examenParaMedical);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();
            $this->getDoctrine()->getManager()->flush();
            if ($examenParaMedical->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
            }
            return $this->redirectToRoute('examenparamedical_index');
        }

        return $this->render('examenparamedical/edit.html.twig', array(
            'examenParaMedical' => $examenParaMedical,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a examenParaMedical entity.
     *
     * @Route("/{id}", name="examenparamedical_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ExamenParaMedical $examenParaMedical)
    {
        $form = $this->createDeleteForm($examenParaMedical);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($examenParaMedical);
            $em->flush();
        }

        return $this->redirectToRoute('examenparamedical_index');
    }

    /**
     * Creates a form to delete a examenParaMedical entity.
     *
     * @param ExamenParaMedical $examenParaMedical The examenParaMedical entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ExamenParaMedical $examenParaMedical)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('examenparamedical_delete', array('id' => $examenParaMedical->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
