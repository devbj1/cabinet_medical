<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Forme;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Forme controller.
 *
 * @Route("forme")
 */
class FormeController extends Controller
{
    /**
     * Lists all forme entities.
     *
     * @Route("/", name="forme_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $formes = $em->getRepository('AppBundle:Forme')->findBy(array('deleted'=>false));

        return $this->render('forme/index.html.twig', array(
            'formes' => $formes,
        ));
    }

    /**
     * Creates a new forme entity.
     *
     * @Route("/new", name="forme_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $forme = new Forme();
        $form = $this->createForm('AppBundle\Form\FormeType', $forme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($forme);
            $em->flush();

            $session = new Session();
            if ($forme->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('forme_index');
        }

        return $this->render('forme/new.html.twig', array(
            'forme' => $forme,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a forme entity.
     *
     * @Route("/{id}", name="forme_show")
     * @Method("GET")
     */
    public function showAction(Forme $forme)
    {
        $deleteForm = $this->createDeleteForm($forme);

        return $this->render('forme/show.html.twig', array(
            'forme' => $forme,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing forme entity.
     *
     * @Route("/{id}/edit", name="forme_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Forme $forme)
    {
        $deleteForm = $this->createDeleteForm($forme);
        $editForm = $this->createForm('AppBundle\Form\FormeType', $forme);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $session = new Session();
            $this->getDoctrine()->getManager()->flush();
            if ($forme->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
            }
            return $this->redirectToRoute('forme_index');
        }

        return $this->render('forme/edit.html.twig', array(
            'forme' => $forme,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a forme entity.
     *
     * @Route("/{id}", name="forme_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Forme $forme)
    {
        $form = $this->createDeleteForm($forme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($forme);
            $em->flush();
        }

        return $this->redirectToRoute('forme_index');
    }

    /**
     * Creates a form to delete a forme entity.
     *
     * @param Forme $forme The forme entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Forme $forme)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('forme_delete', array('id' => $forme->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
