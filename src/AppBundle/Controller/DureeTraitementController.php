<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DureeTraitement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Dureetraitement controller.
 *
 * @Route("dureetraitement")
 */
class DureeTraitementController extends Controller
{
    /**
     * Lists all dureeTraitement entities.
     *
     * @Route("/", name="dureetraitement_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dureeTraitements = $em->getRepository('AppBundle:DureeTraitement')->findBy(array('deleted'=>false));

        return $this->render('dureetraitement/index.html.twig', array(
            'dureeTraitements' => $dureeTraitements,
        ));
    }

    /**
     * Creates a new dureeTraitement entity.
     *
     * @Route("/new", name="dureetraitement_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $dureeTraitement = new Dureetraitement();
        $form = $this->createForm('AppBundle\Form\DureeTraitementType', $dureeTraitement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dureeTraitement);
            $em->flush();

            $session = new Session();
            if ($dureeTraitement->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            } else {
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('dureetraitement_index');
        }

        return $this->render('dureetraitement/new.html.twig', array(
            'dureeTraitement' => $dureeTraitement,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a dureeTraitement entity.
     *
     * @Route("/{id}", name="dureetraitement_show")
     * @Method("GET")
     */
    public function showAction(DureeTraitement $dureeTraitement)
    {
        $deleteForm = $this->createDeleteForm($dureeTraitement);

        return $this->render('dureetraitement/show.html.twig', array(
            'dureeTraitement' => $dureeTraitement,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing dureeTraitement entity.
     *
     * @Route("/{id}/edit", name="dureetraitement_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, DureeTraitement $dureeTraitement)
    {
        $deleteForm = $this->createDeleteForm($dureeTraitement);
        $editForm = $this->createForm('AppBundle\Form\DureeTraitementType', $dureeTraitement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();
            $this->getDoctrine()->getManager()->flush();
            if ($dureeTraitement->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur modification !');
            } else {
                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
            }
            return $this->redirectToRoute('dureetraitement_index');
        }

        return $this->render('dureetraitement/edit.html.twig', array(
            'dureeTraitement' => $dureeTraitement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a dureeTraitement entity.
     *
     * @Route("/{id}", name="dureetraitement_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, DureeTraitement $dureeTraitement)
    {
        $form = $this->createDeleteForm($dureeTraitement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dureeTraitement);
            $em->flush();
        }

        return $this->redirectToRoute('dureetraitement_index');
    }

    /**
     * Creates a form to delete a dureeTraitement entity.
     *
     * @param DureeTraitement $dureeTraitement The dureeTraitement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DureeTraitement $dureeTraitement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dureetraitement_delete', array('id' => $dureeTraitement->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
