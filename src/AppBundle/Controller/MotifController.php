<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Motif;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Motif controller.
 *
 * @Route("motif")
 */
class MotifController extends Controller
{
    /**
     * Lists all motif entities.
     *
     * @Route("/", name="motif_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $motifs = $em->getRepository('AppBundle:Motif')->findBy(array('deleted'=>false));

        return $this->render('motif/index.html.twig', array(
            'motifs' => $motifs,
        ));
    }

    /**
     * Creates a new motif entity.
     *
     * @Route("/new", name="motif_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $motif = new Motif();
        $form = $this->createForm('AppBundle\Form\MotifType', $motif);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();
            $em = $this->getDoctrine()->getManager();
            $em->persist($motif);
            $em->flush();

            if ($motif->getId() == 0){
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            }else{
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('motif_index');
        }

        return $this->render('motif/new.html.twig', array(
            'motif' => $motif,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a motif entity.
     *
     * @Route("/{id}", name="motif_show")
     * @Method("GET")
     */
    public function showAction(Motif $motif)
    {
        $deleteForm = $this->createDeleteForm($motif);

        return $this->render('motif/show.html.twig', array(
            'motif' => $motif,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing motif entity.
     *
     * @Route("/{id}/edit", name="motif_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Motif $motif)
    {
        $deleteForm = $this->createDeleteForm($motif);
        $editForm = $this->createForm('AppBundle\Form\MotifType', $motif);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();
            $this->getDoctrine()->getManager()->flush();
            if ($motif->getId() == 0){
                $session->getFlashBag()->add('error', 'Erreur modification !');
            }else{
                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
            }
            return $this->redirectToRoute('motif_index');
        }

        return $this->render('motif/edit.html.twig', array(
            'motif' => $motif,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a motif entity.
     *
     * @Route("/{id}", name="motif_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Motif $motif)
    {
        $form = $this->createDeleteForm($motif);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $motif->setDeleted(true);
            $this->getDoctrine()->getManager()->flush();
            if ($motif->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur suppression !');
            } else {
                $session->getFlashBag()->add('success', 'Suppression effectué avec succès !');
            }
        }

        return $this->redirectToRoute('motif_index');
    }

    /**
     * Creates a form to delete a motif entity.
     *
     * @param Motif $motif The motif entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Motif $motif)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('motif_delete', array('id' => $motif->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
