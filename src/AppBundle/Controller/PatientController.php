<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Patient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Patient controller.
 *
 * @Route("patient")
 */
class PatientController extends Controller
{

    public function userConnect()
    {
        $session = new Session();
        $connect = $session->get('authenticated');
        if ($connect != true) {
            $url = $this->generateUrl('login');
            $response = new RedirectResponse($url);
            $response->send();
            return;
        }
    }
    /**
     * Lists all patient entities.
     *
     * @Route("/", name="patient_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->userConnect();

        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));

        $patients = $em->getRepository('AppBundle:Patient')->findBy(array('deleted'=>false));

        return $this->render('patient/index.html.twig', array(
            'patients' => $patients,
            'consultationsEnCours' => $consultationsEnCours,
        ));
    }

    /**
     * Creates a new patient entity.
     *
     * @Route("/new", name="patient_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->userConnect();

        $patient = new Patient();
        $form = $this->createForm('AppBundle\Form\PatientType', $patient);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));

        if ($form->isSubmitted() && $form->isValid()) {
            $session  = new Session();

            $em->persist($patient);
            $em->flush();

            if ($patient->getId() == 0){
                $session->getFlashBag()->add('error', 'Erreur enregistrement !');
            }else{
                $session->getFlashBag()->add('success', 'Enregistrement effectué avec succès !');
            }
            return $this->redirectToRoute('patient_index');
        }

        return $this->render('patient/new.html.twig', array(
            'patient' => $patient,
            'form' => $form->createView(),
            'consultationsEnCours' => $consultationsEnCours
        ));
    }

    /**
     * Finds and displays a patient entity.
     *
     * @Route("/{id}", name="patient_show")
     * @Method("GET")
     */
    public function showAction(Patient $patient)
    {
        $deleteForm = $this->createDeleteForm($patient);

        return $this->render('patient/show.html.twig', array(
            'patient' => $patient,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing patient entity.
     *
     * @Route("/{id}/edit", name="patient_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Patient $patient)
    {
        $this->userConnect();

        $deleteForm = $this->createDeleteForm($patient);
        $editForm = $this->createForm('AppBundle\Form\PatientType', $patient);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $consultationsEnCours = $em->getRepository('AppBundle:Consultation')->findBy(array('deleted' => false, 'dateDebConsul' => null));

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();
            $this->getDoctrine()->getManager()->flush();
            if ($patient->getId() == 0){
                $session->getFlashBag()->add('error', 'Erreur modification !');
            }else{
                $session->getFlashBag()->add('success', 'Modification effectué avec succès !');
            }
            return $this->redirectToRoute('patient_index');
        }

        return $this->render('patient/edit.html.twig', array(
            'patient' => $patient,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'consultationsEnCours' => $consultationsEnCours
        ));
    }

    /**
     * Deletes a patient entity.
     *
     * @Route("/{id}", name="patient_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Patient $patient)
    {
        $form = $this->createDeleteForm($patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $patient->setDeleted(true);
            $this->getDoctrine()->getManager()->flush();
            if ($patient->getId() == 0) {
                $session->getFlashBag()->add('error', 'Erreur suppression !');
            } else {
                $session->getFlashBag()->add('success', 'Suppression effectué avec succès !');
            }
        }

        return $this->redirectToRoute('patient_index');
    }

    /**
     * Creates a form to delete a patient entity.
     *
     * @param Patient $patient The patient entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Patient $patient)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('patient_delete', array('id' => $patient->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
