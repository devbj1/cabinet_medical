<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TypePatient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Typepatient controller.
 *
 * @Route("typepatient")
 */
class TypePatientController extends Controller
{
    /**
     * Lists all typePatient entities.
     *
     * @Route("/", name="typepatient_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typePatients = $em->getRepository('AppBundle:TypePatient')->findAll();

        return $this->render('typepatient/index.html.twig', array(
            'typePatients' => $typePatients,
        ));
    }

    /**
     * Creates a new typePatient entity.
     *
     * @Route("/new", name="typepatient_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typePatient = new Typepatient();
        $form = $this->createForm('AppBundle\Form\TypePatientType', $typePatient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typePatient);
            $em->flush();

            return $this->redirectToRoute('typepatient_show', array('id' => $typePatient->getId()));
        }

        return $this->render('typepatient/new.html.twig', array(
            'typePatient' => $typePatient,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typePatient entity.
     *
     * @Route("/{id}", name="typepatient_show")
     * @Method("GET")
     */
    public function showAction(TypePatient $typePatient)
    {
        $deleteForm = $this->createDeleteForm($typePatient);

        return $this->render('typepatient/show.html.twig', array(
            'typePatient' => $typePatient,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typePatient entity.
     *
     * @Route("/{id}/edit", name="typepatient_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypePatient $typePatient)
    {
        $deleteForm = $this->createDeleteForm($typePatient);
        $editForm = $this->createForm('AppBundle\Form\TypePatientType', $typePatient);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('typepatient_edit', array('id' => $typePatient->getId()));
        }

        return $this->render('typepatient/edit.html.twig', array(
            'typePatient' => $typePatient,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a typePatient entity.
     *
     * @Route("/{id}", name="typepatient_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TypePatient $typePatient)
    {
        $form = $this->createDeleteForm($typePatient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typePatient);
            $em->flush();
        }

        return $this->redirectToRoute('typepatient_index');
    }

    /**
     * Creates a form to delete a typePatient entity.
     *
     * @param TypePatient $typePatient The typePatient entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypePatient $typePatient)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typepatient_delete', array('id' => $typePatient->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
