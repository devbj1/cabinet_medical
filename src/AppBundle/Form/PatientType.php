<?php

namespace AppBundle\Form;

use AppBundle\Entity\TypePatient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PatientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomPatient')
            ->add('prenomPatient')
            ->add('dateNaisPatient', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'label' => 'Date de naissance du patient',
            ])
            ->add('telPatient',TextType::class,[
                'label' => 'Numéro de téléphone du patient',
            ])
            ->add('emailPatient')
            ->add('sexePatient', ChoiceType::class, [
                'choices' => [
                    '' => '',
                    'Masculin' =>'Masculin',
                    'Feminin' => 'Féminin'
                ],
            ])
            ->add('profession')
            ->add('persContact',TextType::class,[
                'label' => 'Personne a contacter',
            ])
            ->add('taille')
            ->add('assurePatient', CheckboxType::class, [
                'label'    => 'Assuré ?',
                'required' => false,
            ])
            ->add('adressePatient')
            ->add('medTraitant',TextType::class,[
                'label' => 'Médécin traitant du patient',
            ])
            ->add('situaFamiliale',TextType::class,[
                'label' => 'Situation familiale du patient',
            ])
            ->add('antPerMedic',TextareaType::class, [
                'label' => 'Antécédent para medical du patient',
                'attr' => ['rows' => 7],
            ])
            ->add('antPercChir',TextareaType::class,[
                'label' => 'Antécédent chirurgicale du patient',
                'attr' => ['rows' => 7],
            ])
            ->add('antFamili',TextareaType::class,[
                'label' => 'Antécédent familiale du patient',
                'attr' => ['rows' => 7],
            ])
            ->add('autres',TextareaType::class,[
                'attr' => ['rows' => 7],
            ])
            ->add('typePatient',EntityType::class,[
                'class' => TypePatient::class,
                'choice_label' => 'libelleTypePatient',
            ])
            //->add('deleted')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Patient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_patient';
    }


}
