<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DureeTraitement
 *
 * @ORM\Table(name="duree_traitement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DureeTraitementRepository")
 */
class DureeTraitement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleDureeTraitement", type="string", length=255, unique=true)
     */
    private $libelleDureeTraitement;


    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;


    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLibelleDureeTraitement()
    {
        return $this->libelleDureeTraitement;
    }

    /**
     * @param string $libelleDureeTraitement
     */
    public function setLibelleDureeTraitement($libelleDureeTraitement)
    {
        $this->libelleDureeTraitement = $libelleDureeTraitement;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }




    /**
     * Get deleted.
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
