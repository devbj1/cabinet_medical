<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypePatient
 *
 * @ORM\Table(name="type_patient")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TypePatientRepository")
 */
class TypePatient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="libelle_type_patient", type="string", length=255, unique=true)
     */
    private $libelleTypePatient;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;


    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLibelleTypePatient()
    {
        return $this->libelleTypePatient;
    }

    /**
     * @param string $libelleTypePatient
     */
    public function setLibelleTypePatient($libelleTypePatient)
    {
        $this->libelleTypePatient = $libelleTypePatient;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

}
