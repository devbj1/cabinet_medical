<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsulMedicament
 *
 * @ORM\Table(name="consul_medicament")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConsulMedicamentRepository")
 */
class ConsulMedicament
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Consultation")
     */
    private $consultation;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Medicament")
     */
    private $medicament;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DureeTraitement")
     */
    private $duree;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Forme")
     */
    private $forme;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Posologie")
     */
    private $posologie;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="decimal")
     */
    private $qte;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param int $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return int
     */
    public function getMedicament()
    {
        return $this->medicament;
    }

    /**
     * @param int $medicament
     */
    public function setMedicament($medicament)
    {
        $this->medicament = $medicament;
    }

    /**
     * @return int
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * @param int $duree
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;
    }

    /**
     * @return int
     */
    public function getForme()
    {
        return $this->forme;
    }

    /**
     * @param int $forme
     */
    public function setForme($forme)
    {
        $this->forme = $forme;
    }

    /**
     * @return int
     */
    public function getPosologie()
    {
        return $this->posologie;
    }

    /**
     * @param int $posologie
     */
    public function setPosologie($posologie)
    {
        $this->posologie = $posologie;
    }

    /**
     * @return int
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * @param int $qte
     */
    public function setQte($qte)
    {
        $this->qte = $qte;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }



    /**
     * Get deleted.
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
