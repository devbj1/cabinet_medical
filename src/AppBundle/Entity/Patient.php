<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Patient
 *
 * @ORM\Table(name="patient")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PatientRepository")
 */
class Patient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_patient", type="string", length=255)
     */
    private $nomPatient;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_patient", type="string", length=255)
     */
    private $prenomPatient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_nais_patient", type="datetime", nullable=true)
     */
    private $dateNaisPatient;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_patient", type="string", length=255, nullable=true)
     */
    private $telPatient;

    /**
     * @var string
     *
     * @ORM\Column(name="email_patient", type="string", length=255, nullable=true)
     */
    private $emailPatient;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe_patient", type="string", length=255)
     */
    private $sexePatient;

    /**
     * @var boolean
     *
     * @ORM\Column(name="profession", type="string", length=255, nullable=true)
     */
    private $profession;

    /**
     * @var string
     *
     * @ORM\Column(name="assure_patient", type="boolean", nullable=true)
     */
    private $assurePatient;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_patient", type="string", length=255, nullable=true)
     */
    private $adressePatient;

    /**
     * @var string
     *
     * @ORM\Column(name="med_traitant", type="string", length=255, nullable=true)
     */
    private $medTraitant;

    /**
     * @var string
     *
     * @ORM\Column(name="situa_familiale", type="string", length=255, nullable=true)
     */
    private $situaFamiliale;

    /**
     * @var string
     *
     * @ORM\Column(name="ant_per_medic", type="string", length=255, nullable=true)
     */
    private $antPerMedic;

    /**
     * @var string
     *
     * @ORM\Column(name="ant_perc_chir", type="string", length=255, nullable=true)
     */
    private $antPercChir;

    /**
     * @var string
     *
     * @ORM\Column(name="ant_famili", type="string", length=255, nullable=true)
     */
    private $antFamili;

    /**
     * @var string
     *
     * @ORM\Column(name="autres", type="string", length=255, nullable=true)
     */
    private $autres;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_contact", type="string", length=255, nullable=true)
     */
    private $persContact;

    /**
     * @var string
     *
     * @ORM\Column(name="taille", type="string", length=255, nullable=true)
     */
    private $taille;


    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypePatient")
     */
    private $typePatient;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomPatient()
    {
        return $this->nomPatient;
    }

    /**
     * @param string $nomPatient
     */
    public function setNomPatient($nomPatient)
    {
        $this->nomPatient = $nomPatient;
    }

    /**
     * @return string
     */
    public function getPrenomPatient()
    {
        return $this->prenomPatient;
    }

    /**
     * @param string $prenomPatient
     */
    public function setPrenomPatient($prenomPatient)
    {
        $this->prenomPatient = $prenomPatient;
    }

    /**
     * @return \DateTime
     */
    public function getDateNaisPatient()
    {
        return $this->dateNaisPatient;
    }

    /**
     * @param \DateTime $dateNaisPatient
     */
    public function setDateNaisPatient($dateNaisPatient)
    {
        $this->dateNaisPatient = $dateNaisPatient;
    }

    /**
     * @return string
     */
    public function getTelPatient()
    {
        return $this->telPatient;
    }

    /**
     * @param string $telPatient
     */
    public function setTelPatient($telPatient)
    {
        $this->telPatient = $telPatient;
    }

    /**
     * @return string
     */
    public function getEmailPatient()
    {
        return $this->emailPatient;
    }

    /**
     * @param string $emailPatient
     */
    public function setEmailPatient($emailPatient)
    {
        $this->emailPatient = $emailPatient;
    }

    /**
     * @return string
     */
    public function getSexePatient()
    {
        return $this->sexePatient;
    }

    /**
     * @param string $sexePatient
     */
    public function setSexePatient($sexePatient)
    {
        $this->sexePatient = $sexePatient;
    }

    /**
     * @return bool
     */
    public function isProfession()
    {
        return $this->profession;
    }

    /**
     * @param bool $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }

    /**
     * @return string
     */
    public function getAssurePatient()
    {
        return $this->assurePatient;
    }

    /**
     * @param string $assurePatient
     */
    public function setAssurePatient($assurePatient)
    {
        $this->assurePatient = $assurePatient;
    }

    /**
     * @return string
     */
    public function getAdressePatient()
    {
        return $this->adressePatient;
    }

    /**
     * @param string $adressePatient
     */
    public function setAdressePatient($adressePatient)
    {
        $this->adressePatient = $adressePatient;
    }

    /**
     * @return string
     */
    public function getMedTraitant()
    {
        return $this->medTraitant;
    }

    /**
     * @param string $medTraitant
     */
    public function setMedTraitant($medTraitant)
    {
        $this->medTraitant = $medTraitant;
    }

    /**
     * @return string
     */
    public function getSituaFamiliale()
    {
        return $this->situaFamiliale;
    }

    /**
     * @param string $situaFamiliale
     */
    public function setSituaFamiliale($situaFamiliale)
    {
        $this->situaFamiliale = $situaFamiliale;
    }

    /**
     * @return string
     */
    public function getAntPerMedic()
    {
        return $this->antPerMedic;
    }

    /**
     * @param string $antPerMedic
     */
    public function setAntPerMedic($antPerMedic)
    {
        $this->antPerMedic = $antPerMedic;
    }

    /**
     * @return string
     */
    public function getAntPercChir()
    {
        return $this->antPercChir;
    }

    /**
     * @param string $antPercChir
     */
    public function setAntPercChir($antPercChir)
    {
        $this->antPercChir = $antPercChir;
    }

    /**
     * @return string
     */
    public function getAntFamili()
    {
        return $this->antFamili;
    }

    /**
     * @param string $antFamili
     */
    public function setAntFamili($antFamili)
    {
        $this->antFamili = $antFamili;
    }

    /**
     * @return string
     */
    public function getAutres()
    {
        return $this->autres;
    }

    /**
     * @param string $autres
     */
    public function setAutres($autres)
    {
        $this->autres = $autres;
    }

    /**
     * @return string
     */
    public function getPersContact()
    {
        return $this->persContact;
    }

    /**
     * @param string $persContact
     */
    public function setPersContact($persContact)
    {
        $this->persContact = $persContact;
    }

    /**
     * @return string
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param string $taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * @return int
     */
    public function getTypePatient()
    {
        return $this->typePatient;
    }

    /**
     * @param int $typePatient
     */
    public function setTypePatient($typePatient)
    {
        $this->typePatient = $typePatient;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

}


