<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Examen
 *
 * @ORM\Table(name="examen")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExamenRepository")
 */
class Examen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_examen", type="string", length=255, unique=true, nullable=true)
     */
    private $codeExamen;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle_examen", type="string", length=255, unique=true)
     */
    private $libelleExamen;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeExamen
     *
     * @param string $codeExamen
     *
     * @return Examen
     */
    public function setCodeExamen($codeExamen)
    {
        $this->codeExamen = $codeExamen;

        return $this;
    }

    /**
     * Get codeExamen
     *
     * @return string
     */
    public function getCodeExamen()
    {
        return $this->codeExamen;
    }

    /**
     * Set libelleExamen
     *
     * @param string $libelleExamen
     *
     * @return Examen
     */
    public function setLibelleExamen($libelleExamen)
    {
        $this->libelleExamen = $libelleExamen;

        return $this;
    }

    /**
     * Get libelleExamen
     *
     * @return string
     */
    public function getLibelleExamen()
    {
        return $this->libelleExamen;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Examen
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
