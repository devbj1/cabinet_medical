<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsulExamenParaMedical
 *
 * @ORM\Table(name="consul_examen_para_medical")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConsulExamenParaMedicalRepository")
 */
class ConsulExamenParaMedical
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Consultation")
     */
    private $consultation;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ExamenParaMedical")
     */
    private $examenParaMedical;

    /**
     * @var string
     *
     * @ORM\Column(name="resultat_para_medical", type="string", length=255)
     */
    private $resultatParaMedical;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="string", length=255, nullable=true)
     */
    private $observation;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param int $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return int
     */
    public function getExamenParaMedical()
    {
        return $this->examenParaMedical;
    }

    /**
     * @param int $examenParaMedical
     */
    public function setExamenParaMedical($examenParaMedical)
    {
        $this->examenParaMedical = $examenParaMedical;
    }

    /**
     * @return string
     */
    public function getResultatParaMedical()
    {
        return $this->resultatParaMedical;
    }

    /**
     * @param string $resultatParaMedical
     */
    public function setResultatParaMedical($resultatParaMedical)
    {
        $this->resultatParaMedical = $resultatParaMedical;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }




    /**
     * Get deleted.
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
