<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consultation
 *
 * @ORM\Table(name="consultation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConsultationRepository")
 */
class Consultation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_consul", type="datetime")
     */
    private $dateConsul;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_prochaine_consul", type="datetime", nullable=true)
     */
    private $dateProchaineConsul;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_deb_consul", type="datetime", nullable=true)
     */
    private $dateDebConsul;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin_consul", type="datetime", nullable=true)
     */
    private $dateFinConsul;

    /**
     * @var int
     *
     * @ORM\Column(name="poids_consul", type="float", nullable=true)
     */
    private $poidsConsul;

    /**
     * @var int
     *
     * @ORM\Column(name="taille_consul", type="float", nullable=true)
     */
    private $tailleConsul;

    /**
     * @var int
     *
     * @ORM\Column(name="temp_consul", type="float", nullable=true)
     */
    private $tempConsul;

    /**
     * @var int
     *
     * @ORM\Column(name="tension_consul", type="float", nullable=true)
     */
    private $tensionConsul;

    /**
     * @var string
     *
     * @ORM\Column(name="result_exam_para", type="string", length=255, nullable=true)
     */
    private $resultExamPara;

    /**
     * @var string
     *
     * @ORM\Column(name="conduit_a_tenir", type="string", length=255, nullable=true)
     */
    private $conduitATenir;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Patient")
     */
    private $patient;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    private $medecin;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CentreMedical")
     */
    private $centreMedical;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="string", length=255, nullable=true)
     */
    private $observation;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDateConsul()
    {
        return $this->dateConsul;
    }

    /**
     * @param \DateTime $dateConsul
     */
    public function setDateConsul($dateConsul)
    {
        $this->dateConsul = $dateConsul;
    }

    /**
     * @return \DateTime
     */
    public function getDateProchaineConsul()
    {
        return $this->dateProchaineConsul;
    }

    /**
     * @param \DateTime $dateProchaineConsul
     */
    public function setDateProchaineConsul($dateProchaineConsul)
    {
        $this->dateProchaineConsul = $dateProchaineConsul;
    }

    /**
     * @return \DateTime
     */
    public function getDateDebConsul()
    {
        return $this->dateDebConsul;
    }

    /**
     * @param \DateTime $dateDebConsul
     */
    public function setDateDebConsul($dateDebConsul)
    {
        $this->dateDebConsul = $dateDebConsul;
    }

    /**
     * @return \DateTime
     */
    public function getDateFinConsul()
    {
        return $this->dateFinConsul;
    }

    /**
     * @param \DateTime $dateFinConsul
     */
    public function setDateFinConsul($dateFinConsul)
    {
        $this->dateFinConsul = $dateFinConsul;
    }

    /**
     * @return int
     */
    public function getPoidsConsul()
    {
        return $this->poidsConsul;
    }

    /**
     * @param int $poidsConsul
     */
    public function setPoidsConsul($poidsConsul)
    {
        $this->poidsConsul = $poidsConsul;
    }

    /**
     * @return int
     */
    public function getTailleConsul()
    {
        return $this->tailleConsul;
    }

    /**
     * @param int $tailleConsul
     */
    public function setTailleConsul($tailleConsul)
    {
        $this->tailleConsul = $tailleConsul;
    }

    /**
     * @return int
     */
    public function getTempConsul()
    {
        return $this->tempConsul;
    }

    /**
     * @param int $tempConsul
     */
    public function setTempConsul($tempConsul)
    {
        $this->tempConsul = $tempConsul;
    }

    /**
     * @return int
     */
    public function getTensionConsul()
    {
        return $this->tensionConsul;
    }

    /**
     * @param int $tensionConsul
     */
    public function setTensionConsul($tensionConsul)
    {
        $this->tensionConsul = $tensionConsul;
    }

    /**
     * @return string
     */
    public function getResultExamPara()
    {
        return $this->resultExamPara;
    }

    /**
     * @param string $resultExamPara
     */
    public function setResultExamPara($resultExamPara)
    {
        $this->resultExamPara = $resultExamPara;
    }

    /**
     * @return string
     */
    public function getConduitATenir()
    {
        return $this->conduitATenir;
    }

    /**
     * @param string $conduitATenir
     */
    public function setConduitATenir($conduitATenir)
    {
        $this->conduitATenir = $conduitATenir;
    }

    /**
     * @return int
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * @param int $patient
     */
    public function setPatient($patient)
    {
        $this->patient = $patient;
    }

    /**
     * @return int
     */
    public function getMedecin()
    {
        return $this->medecin;
    }

    /**
     * @param int $medecin
     */
    public function setMedecin($medecin)
    {
        $this->medecin = $medecin;
    }

    /**
     * @return int
     */
    public function getCentreMedical()
    {
        return $this->centreMedical;
    }

    /**
     * @param int $centreMedical
     */
    public function setCentreMedical($centreMedical)
    {
        $this->centreMedical = $centreMedical;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }



}
