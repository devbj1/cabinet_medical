<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamenParaMedical
 *
 * @ORM\Table(name="examen_para_medical")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExamenParaMedicalRepository")
 */
class ExamenParaMedical
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_examen_para", type="string", length=255, unique=true, nullable=true)
     */
    private $codeExamenPara;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle_examen_para", type="string", length=255, unique=true)
     */
    private $libelleExamenPara;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCodeExamenPara()
    {
        return $this->codeExamenPara;
    }

    /**
     * @param string $codeExamenPara
     */
    public function setCodeExamenPara($codeExamenPara)
    {
        $this->codeExamenPara = $codeExamenPara;
    }

    /**
     * @return string
     */
    public function getLibelleExamenPara()
    {
        return $this->libelleExamenPara;
    }

    /**
     * @param string $libelleExamenPara
     */
    public function setLibelleExamenPara($libelleExamenPara)
    {
        $this->libelleExamenPara = $libelleExamenPara;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }



    /**
     * Get deleted.
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
