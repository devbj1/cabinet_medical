<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserCentreMedical
 *
 * @ORM\Table(name="user_centre_medical")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserCentreMedicalRepository")
 */
class UserCentreMedical
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Patient")
     */
    private $patient;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CentreMedical")
     */
    private $centreMedical;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * @param int $patient
     */
    public function setPatient($patient)
    {
        $this->patient = $patient;
    }

    /**
     * @return int
     */
    public function getCentreMedical()
    {
        return $this->centreMedical;
    }

    /**
     * @param int $centreMedical
     */
    public function setCentreMedical($centreMedical)
    {
        $this->centreMedical = $centreMedical;
    }

    
}
