<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Motif
 *
 * @ORM\Table(name="motif")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MotifRepository")
 */
class Motif
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_motif", type="string", length=255, unique=true, nullable=true)
     */
    private $codeMotif;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle_motif", type="string", length=255, unique=true)
     */
    private $libelleMotif;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;


    public function __construct()
    {
        $this->deleted = false;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeMotif
     *
     * @param string $codeMotif
     *
     * @return Motif
     */
    public function setCodeMotif($codeMotif)
    {
        $this->codeMotif = $codeMotif;

        return $this;
    }

    /**
     * Get codeMotif
     *
     * @return string
     */
    public function getCodeMotif()
    {
        return $this->codeMotif;
    }

    /**
     * Set libelleMotif
     *
     * @param string $libelleMotif
     *
     * @return Motif
     */
    public function setLibelleMotif($libelleMotif)
    {
        $this->libelleMotif = $libelleMotif;

        return $this;
    }

    /**
     * Get libelleMotif
     *
     * @return string
     */
    public function getLibelleMotif()
    {
        return $this->libelleMotif;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Motif
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
