<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diagnostic
 *
 * @ORM\Table(name="diagnostic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiagnosticRepository")
 */
class Diagnostic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_diagnostic", type="string", length=255, unique=true, nullable=true)
     */
    private $codeDiagnostic;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle_diagnostic", type="string", length=255, unique=true)
     */
    private $libelleDiagnostic;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;


    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeDiagnostic
     *
     * @param string $codeDiagnostic
     *
     * @return Diagnostic
     */
    public function setCodeDiagnostic($codeDiagnostic)
    {
        $this->codeDiagnostic = $codeDiagnostic;

        return $this;
    }

    /**
     * Get codeDiagnostic
     *
     * @return string
     */
    public function getCodeDiagnostic()
    {
        return $this->codeDiagnostic;
    }

    /**
     * Set libelleDiagnostic
     *
     * @param string $libelleDiagnostic
     *
     * @return Diagnostic
     */
    public function setLibelleDiagnostic($libelleDiagnostic)
    {
        $this->libelleDiagnostic = $libelleDiagnostic;

        return $this;
    }

    /**
     * Get libelleDiagnostic
     *
     * @return string
     */
    public function getLibelleDiagnostic()
    {
        return $this->libelleDiagnostic;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Diagnostic
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
