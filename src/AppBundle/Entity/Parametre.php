<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parametre
 *
 * @ORM\Table(name="parametre")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ParametreRepository")
 */
class Parametre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="centre_medical", type="string", length=255)
     */
    private $centreMedical;

    /**
     * @var string
     *
     * @ORM\Column(name="centre_medical_adresse", type="string", length=255)
     */
    private $centreMedicalAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="centre_medical_contact", type="string", length=255)
     */
    private $centreMedicalContact;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCentreMedical()
    {
        return $this->centreMedical;
    }

    /**
     * @param string $centreMedical
     */
    public function setCentreMedical($centreMedical)
    {
        $this->centreMedical = $centreMedical;
    }

    /**
     * @return string
     */
    public function getCentreMedicalAdresse()
    {
        return $this->centreMedicalAdresse;
    }

    /**
     * @param string $centreMedicalAdresse
     */
    public function setCentreMedicalAdresse($centreMedicalAdresse)
    {
        $this->centreMedicalAdresse = $centreMedicalAdresse;
    }

    /**
     * @return string
     */
    public function getCentreMedicalContact()
    {
        return $this->centreMedicalContact;
    }

    /**
     * @param string $centreMedicalContact
     */
    public function setCentreMedicalContact($centreMedicalContact)
    {
        $this->centreMedicalContact = $centreMedicalContact;
    }

}
