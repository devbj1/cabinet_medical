<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medicament
 *
 * @ORM\Table(name="medicament")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MedicamentRepository")
 */
class Medicament
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_medicament", type="string", length=255, unique=true)
     */
    private $codeMedicament;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle_medicament", type="string", length=255, unique=true)
     */
    private $libelleMedicament;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    public function __construct()
    {
        $this->deleted = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeMedicament
     *
     * @param string $codeMedicament
     *
     * @return Medicament
     */
    public function setCodeMedicament($codeMedicament)
    {
        $this->codeMedicament = $codeMedicament;

        return $this;
    }

    /**
     * Get codeMedicament
     *
     * @return string
     */
    public function getCodeMedicament()
    {
        return $this->codeMedicament;
    }

    /**
     * Set libelleMedicament
     *
     * @param string $libelleMedicament
     *
     * @return Medicament
     */
    public function setLibelleMedicament($libelleMedicament)
    {
        $this->libelleMedicament = $libelleMedicament;

        return $this;
    }

    /**
     * Get libelleMedicament
     *
     * @return string
     */
    public function getLibelleMedicament()
    {
        return $this->libelleMedicament;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Medicament
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return bool
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
