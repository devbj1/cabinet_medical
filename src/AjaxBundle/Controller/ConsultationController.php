<?php

namespace AjaxBundle\Controller;

use AppBundle\Entity\ConsulDiagnostic;
use AppBundle\Entity\ConsulExamen;
use AppBundle\Entity\ConsulExamenParaMedical;
use AppBundle\Entity\ConsulMedicament;
use AppBundle\Entity\ConsulMotif;
use AppBundle\Entity\Consultation;
use AppBundle\Entity\Diagnostic;
use AppBundle\Entity\DureeTraitement;
use AppBundle\Entity\Examen;
use AppBundle\Entity\ExamenParaMedical;
use AppBundle\Entity\Forme;
use AppBundle\Entity\Medicament;
use AppBundle\Entity\Motif;
use AppBundle\Entity\Patient;
use AppBundle\Entity\Posologie;
use AppBundle\Entity\TypePatient;
use http\Env\Response;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Consultation controller.
 *
 * @Route("consultation")
 */
class ConsultationController extends Controller
{

    /**
     * @Route("/addconsultation", name="ajax_add_consultation")
     * @Method({"GET", "POST"})
     */
    public function addConsultationAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        //On enregistrer la consultation
        $session = new Session();
        $consultation = $em->getRepository('AppBundle:Consultation')->find($request->get('idticket'));


        //dump($session->get('idticket'));die();
        $patient = new Patient();
        $patient = $em->getRepository('AppBundle:Patient')->find($consultation->getPatient()->getId());
        $patient->setSituaFamiliale($session->get('antfamilial'));
        $patient->setAntPerMedic($session->get('antmedical'));
        $patient->setAntPercChir($session->get('antchirugical'));
        $patient->setAutres($session->get('autres'));
        $em->persist($patient);
        $em->flush();

        $session = new Session();
        $medecin = $em->getRepository('AppBundle:User')->find($session->get('id'));

        $consultation->setPatient($patient);
        $consultation->setConduitATenir($request->get('conduiteatenir'));
        $consultation->setMedecin($medecin);
        $consultation->setTempConsul($request->get('temp'));
        $consultation->setPoidsConsul($request->get('poids'));
        $consultation->setTensionConsul($request->get('tens'));
        $consultation->setObservation($request->get('obser'));
        $consultation->setDateFinConsul(new \DateTime("now"));
        $em->persist($consultation);
        $em->flush();


        //On enregistrer les motifs
        $listMotifs = $request->get('listmotif');
        if ($listMotifs != null) {
            foreach ($listMotifs as $idMotif) {
                $consultationMotif = new ConsulMotif();
                $motif = $em->getRepository('AppBundle:Motif')->find($idMotif);
                $consultationMotif->setConsultation($consultation);
                $consultationMotif->setMotif($motif);
                $em->persist($consultationMotif);
                $em->flush();
            }
        }


        //On enregistrer les examens para clinique

//        $consultationExameParaClinique = new ConsulExamenParaMedical();
//        $examenPara = $em->getRepository('AppBundle:ExamenParaMedical')->find($request->get('idexampara'));
//        $consultationExameParaClinique->setConsultation($consultation);
//        $consultationExameParaClinique->setExamenParaMedical($examenPara);
//        $consultationExameParaClinique->setResultatParaMedical($request->get('resultatexampara'));
//        $em->persist($consultationExameParaClinique);
//        $em->flush();
        $listExamenParaCliniques = $request->get('listexamenpara');

        if ($listExamenParaCliniques != null) {
            $resultat = $request->get('resultatexampara');
            foreach ($listExamenParaCliniques as $idExamenParaClinique) {
                $consultationExameParaClinique = new ConsulExamenParaMedical();
                $examenParaClinique = $em->getRepository('AppBundle:Examen')->find($idExamenParaClinique);
                $consultationExameParaClinique->setConsultation($consultation);
                $consultationExameParaClinique->setExamenParaMedical($examenParaClinique);
                $consultationExameParaClinique->setResultatParaMedical($resultat);
                $em->persist($examenParaClinique);
                $em->flush();
            }
        }


        //On enregistrer les examens clinique
        $listExamenCliniques = $request->get('listexamencli');
        if ($listExamenCliniques != null) {

            foreach ($listExamenCliniques as $examenClinique) {
                if ($examenClinique['resultat'] != null) {
                    $consultationExamenClinique = new ConsulExamen();
                    $examen = $em->getRepository('AppBundle:Examen')->find($examenClinique['idexamen']);
                    $consultationExamenClinique->setConsultation($consultation);
                    $consultationExamenClinique->setExamen($examen);
                    $consultationExamenClinique->setResultat($examenClinique['resultat']);
                    $em->persist($consultationExamenClinique);
                    $em->flush();
                }
            }
        }


        //On enregistrer les diagnostics
        $listDiagnostics = $request->get('listdiagnostic');
        if ($listDiagnostics != null) {
            foreach ($listDiagnostics as $idDiagnostic) {
                $diagnosticConsultation = new ConsulDiagnostic();
                $diagnostic = $em->getRepository('AppBundle:Diagnostic')->find($idDiagnostic);
                $diagnosticConsultation->setConsultation($consultation);
                $diagnosticConsultation->setDiagnostic($diagnostic);
                $em->persist($diagnosticConsultation);
                $em->flush();
            }
        }


        //On enregistrer les traitements / medicament
        $listTraitements = $request->get('listtraitement');

        if ($listTraitements != null) {
            foreach ($listTraitements as $traitement) {
                if ($traitement['idmedicament'] != null) {
                    $consultationMedicament = new ConsulMedicament();
                    $medicament = $em->getRepository('AppBundle:Medicament')->find($traitement['idmedicament']);
                    $forme = $em->getRepository('AppBundle:Forme')->find($traitement['idforme']);
                    $posologie = $em->getRepository('AppBundle:Posologie')->find($traitement['idposologie']);
                    $qteduree = $em->getRepository('AppBundle:DureeTraitement')->find($traitement['idduree']);
                    $consultationMedicament->setMedicament($medicament);
                    $consultationMedicament->setForme($forme);
                    $consultationMedicament->setPosologie($posologie);
                    $consultationMedicament->setDuree($qteduree);
                    $consultationMedicament->setConsultation($consultation);
                    $consultationMedicament->setQte($traitement['qte']);
                    $em->persist($consultationMedicament);
                    $em->flush();
                }
            }
        }
        //dump($listTraitements);die();
        //dump($patient);die();

        //$this->pdfAction($consultation->getId());

        if ($consultation->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deletedconsultation", name="ajax_deleted_consultation")
     * @Method({"GET", "POST"})
     */
    public function deletedConsultationAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $consultation = $em->getRepository('AppBundle:Consultation')->find($id);
        $consultation->setDeleted(1);
        $em->persist($consultation);
        $em->flush();

        if ($consultation->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/dupliqueconsultation", name="ajax_duplique_consultation")
     * @Method({"GET", "POST"})
     */
    public function dupliqueConsultationAction(Request $request)
    {
        $idpatient = $request->get('idpatient');
        $idconsultation = $request->get('idconsultation');

        $em = $this->getDoctrine()->getManager();

        $patient = $em->getRepository('AppBundle:Patient')->find($idpatient);
        $consultation = $em->getRepository('AppBundle:Consultation')->find($idconsultation);

        $newConsultation = new Consultation();
        $newConsultation->setPatient($patient);
        $newConsultation->setDateFinConsul($consultation->getDateFinConsul());
        $newConsultation->setDateDebConsul($consultation->getDateDebConsul());
        $newConsultation->setTensionConsul($consultation->getTensionConsul());
        $newConsultation->setPoidsConsul($consultation->getPoidsConsul());
        $newConsultation->setTempConsul($consultation->getTempConsul());
        $newConsultation->setMedecin($consultation->getMedecin());
        $newConsultation->setObservation($consultation->getObservation());
        $newConsultation->setDateConsul($consultation->getDateConsul());
        $newConsultation->setConduitATenir($consultation->getConduitATenir());
        $em->persist($newConsultation);
        $em->flush();


        //On enregistrer les motifs
        $consultationMotifs = $em->getRepository('AppBundle:ConsulMotif')->findBy(array('consultation' =>$idconsultation));
        foreach ($consultationMotifs as $consultationMotif){
            $consultationMotifNew = new ConsulMotif();
            $consultationMotifNew->setConsultation($newConsultation);
            $consultationMotifNew->setMotif($consultationMotif->getMotif());
            $em->persist($consultationMotifNew);
            $em->flush();
        }


        //On enregistrer les examens para clinique

        $listExamenParaCliniques = $em->getRepository('AppBundle:ConsulExamenParaMedical')->findBy(array('consultation' =>$idconsultation));
        foreach ($listExamenParaCliniques as $listExamenParaClinique){
            $consultationExamenParaCliniqueNew = new ConsulExamenParaMedical();
            $consultationExamenParaCliniqueNew->setConsultation($newConsultation);
            $consultationExamenParaCliniqueNew->setExamenParaMedical($listExamenParaClinique->getExamenParaMedical());
            $consultationExamenParaCliniqueNew->setResultatParaMedical($listExamenParaClinique->getResultatParaMedical());
            $em->persist($consultationExamenParaCliniqueNew);
            $em->flush();
        }


        //On enregistrer les examens clinique

        $listExamenCliniques = $em->getRepository('AppBundle:ConsulExamen')->findBy(array('consultation' =>$idconsultation));
        foreach ($listExamenCliniques as $listExamenClinique){
            $consultationExamenCliniqueNew = new ConsulExamen();
            $consultationExamenCliniqueNew->setConsultation($newConsultation);
            $consultationExamenCliniqueNew->setExamen($listExamenClinique->getExamen());
            $consultationExamenCliniqueNew->setResultat($listExamenClinique->getResultat());
            $em->persist($consultationExamenCliniqueNew);
            $em->flush();
        }

        //On enregistrer les diagnostics

        $listDiagnostics = $em->getRepository('AppBundle:ConsulDiagnostic')->findBy(array('consultation' =>$idconsultation));
        foreach ($listDiagnostics as $listDiagnostic){
            $consultationDiagnosticNew = new ConsulDiagnostic();
            $consultationDiagnosticNew->setConsultation($newConsultation);
            $consultationDiagnosticNew->setDiagnostic($listDiagnostic->getDiagnostic());
            $em->persist($consultationDiagnosticNew);
            $em->flush();
        }


        //On enregistrer les traitements / medicament

        $listTraitements = $em->getRepository('AppBundle:ConsulMedicament')->findBy(array('consultation' =>$idconsultation));
        foreach ($listTraitements as $listTraitement){
            $consultationTraitementNew = new ConsulMedicament();
            $consultationTraitementNew->setConsultation($newConsultation);
            $consultationTraitementNew->setQte($listTraitement->getQte());
            $consultationTraitementNew->setPosologie($listTraitement->getPosologie());
            $consultationTraitementNew->setForme($listTraitement->getForme());
            $consultationTraitementNew->setDuree($listTraitement->getDuree());
            $consultationTraitementNew->setMedicament($listTraitement->getMedicament());
            $em->persist($consultationTraitementNew);
            $em->flush();
        }


        if ($consultation != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/detailconsultation", name="ajax_detail_consultation")
     * @Method({"GET", "POST"})
     */
    public function detailConsultationAction(Request $request)
    {

        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $consultation = $em->getRepository('AppBundle:Consultation')->find($id);

        $infoPatient = $this->getDoctrine()->getRepository('AppBundle:Patient')->
        findBy(array('id' => $consultation->getPatient()->getId(), 'deleted' => 0));

        $infosPatient = [];
        $infosPatient['id'] = $infoPatient[0]->getId();
        $infosPatient['adresse'] = $infoPatient[0]->getAdressePatient();
        $infosPatient['antFamili'] = $infoPatient[0]->getAntFamili();
        $infosPatient['antPercChir'] = $infoPatient[0]->getAntPercChir();
        $infosPatient['antPerMedic'] = $infoPatient[0]->getAntPerMedic();
        $infosPatient['assure'] = $infoPatient[0]->getAssurePatient();
        $infosPatient['autres'] = $infoPatient[0]->getAutres();
        if ($infoPatient[0]->getDateNaisPatient() != null){
            $infosPatient['age'] = date_format(new \DateTime("now"), ('Y')) - date_format($infoPatient[0]->getDateNaisPatient(), ('Y')) . " ans";

        }else{
            $infosPatient['age'] = "";
        }
        $infosPatient['email'] = $infoPatient[0]->getEmailPatient();
        $infosPatient['medTrait'] = $infoPatient[0]->getMedTraitant();
        $infosPatient['nom'] = $infoPatient[0]->getNomPatient();
        $infosPatient['prenom'] = $infoPatient[0]->getPrenomPatient();
        $infosPatient['sexe'] = $infoPatient[0]->getSexePatient();
        $infosPatient['situaFamili'] = $infoPatient[0]->getSituaFamiliale();
        $infosPatient['tel'] = $infoPatient[0]->getTelPatient();
        $infosPatient['poids'] = $consultation->getPoidsConsul();
        $infosPatient['temp'] = $consultation->getTempConsul();
        $infosPatient['tens'] = $consultation->getTensionConsul();
        $infosPatient['observation'] = $consultation->getObservation();


        $listeInfosMotif = [];
        //On enregistrer les motifs
        $consultationMotifs = $em->getRepository('AppBundle:ConsulMotif')->findBy(array('consultation' =>$id));
        foreach ($consultationMotifs as $consultationMotif){
            $infosMotif['codemotif'] = $consultationMotif->getMotif()->getId();
            $infosMotif['codemotif'] = $consultationMotif->getMotif()->getId();
            $consultationMotifNew->setMotif($consultationMotif->getMotif());
            $em->persist($consultationMotifNew);
            $em->flush();
        }


        die();
        //On enregistrer les examens para clinique

        $listExamenParaCliniques = $em->getRepository('AppBundle:ConsulExamenParaMedical')->findBy(array('consultation' =>$idconsultation));
        foreach ($listExamenParaCliniques as $listExamenParaClinique){
            $consultationExamenParaCliniqueNew = new ConsulExamenParaMedical();
            $consultationExamenParaCliniqueNew->setConsultation($newConsultation);
            $consultationExamenParaCliniqueNew->setExamenParaMedical($listExamenParaClinique->getExamenParaMedical());
            $consultationExamenParaCliniqueNew->setResultatParaMedical($listExamenParaClinique->getResultatParaMedical());
            $em->persist($consultationExamenParaCliniqueNew);
            $em->flush();
        }


        //On enregistrer les examens clinique

        $listExamenCliniques = $em->getRepository('AppBundle:ConsulExamen')->findBy(array('consultation' =>$idconsultation));
        foreach ($listExamenCliniques as $listExamenClinique){
            $consultationExamenCliniqueNew = new ConsulExamen();
            $consultationExamenCliniqueNew->setConsultation($newConsultation);
            $consultationExamenCliniqueNew->setExamen($listExamenClinique->getExamen());
            $consultationExamenCliniqueNew->setResultat($listExamenClinique->getResultat());
            $em->persist($consultationExamenCliniqueNew);
            $em->flush();
        }

        //On enregistrer les diagnostics

        $listDiagnostics = $em->getRepository('AppBundle:ConsulDiagnostic')->findBy(array('consultation' =>$idconsultation));
        foreach ($listDiagnostics as $listDiagnostic){
            $consultationDiagnosticNew = new ConsulDiagnostic();
            $consultationDiagnosticNew->setConsultation($newConsultation);
            $consultationDiagnosticNew->setDiagnostic($listDiagnostic->getDiagnostic());
            $em->persist($consultationDiagnosticNew);
            $em->flush();
        }


        //On enregistrer les traitements / medicament

        $listTraitements = $em->getRepository('AppBundle:ConsulMedicament')->findBy(array('consultation' =>$idconsultation));
        foreach ($listTraitements as $listTraitement){
            $consultationTraitementNew = new ConsulMedicament();
            $consultationTraitementNew->setConsultation($newConsultation);
            $consultationTraitementNew->setQte($listTraitement->getQte());
            $consultationTraitementNew->setPosologie($listTraitement->getPosologie());
            $consultationTraitementNew->setForme($listTraitement->getForme());
            $consultationTraitementNew->setDuree($listTraitement->getDuree());
            $consultationTraitementNew->setMedicament($listTraitement->getMedicament());
            $em->persist($consultationTraitementNew);
            $em->flush();
        }


        if ($consultation != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/ordonnance/pdf/{id}", name="ajax_ordonnance")
     * @Method({"GET", "POST"})
     */
    public function pdfAction($id)
    {


 //       $snappy = $this->get('knp_snappy.pdf');


        $em = $this->getDoctrine()->getManager();
        $consultation = $em->getRepository('AppBundle:Consultation')->find($id);
        $consulMedicaments = $em->getRepository('AppBundle:ConsulMedicament')->findBy(array('consultation' => $consultation));
        $parametre = $em->getRepository('AppBundle:Parametre')->find(1);
        if ($consultation->getPatient()->getDateNaisPatient() != null){
            $agePatient = date_format(new \DateTime("now"), ('Y')) - date_format($consultation->getPatient()->getDateNaisPatient()
                    , ('Y')) . " ans";
        }else{
            $agePatient = "";
        }

        $codebarre = $consultation->getMedecin()->getNom().' '.$consultation->getMedecin()->getPrenom().' Contact : '.$consultation->getMedecin()->getTel();

        //$qrCode = new QrCode($consultation->getPatient()->getNomPatient().' '.$consultation->getPatient()->getPrenomPatient());

        //$qrCode = $qrCodeFactory->create('QR Code', ['size' => 200]);
        //header('Content-Type: '.$qrCode->getContentType());
        //echo $qrCode->writeString();
        //die();
        // dump($consulMedicaments);
        // die();
        $html = $this->renderView('consultation/ordonnance.html.twig', compact('consultation',
            'consulMedicaments', 'parametre','agePatient','codebarre'), array(
                "title" => "testetst"
        ));


//        $filename = "test";
//
//        return new PdfResponse(
//            $snappy->getOutputFromHtml($html),
//            200,
//            array(
//                'Content-Type' => 'application/pdf',
//                'Content-Disposition' => 'inline; filename="'.$filename.'.pdf"'
//            )
//        );


        $pageUrl = $this->generateUrl('homepage', array(), true); // use absolute path!

       return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'file.pdf'
        );
    }


    /**
     * @Route("/addmotif", name="ajax_add_motif")
     * @Method({"GET", "POST"})
     */
    public function addMotifAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $motif = new Motif();
        $motif->setCodeMotif($request->get('code'));
        $motif->setLibelleMotif($request->get('libelle'));
        $em->persist($motif);
        $em->flush();

        if ($motif->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/addmedicament", name="ajax_add_medicament")
     * @Method({"GET", "POST"})
     */
    public function addMedicamentAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $medicament = new Medicament();
        $medicament->setCodeMedicament($request->get('code'));
        $medicament->setLibelleMedicament($request->get('libelle'));
        $em->persist($medicament);
        $em->flush();
//        dump($medicament);die();

        if ($medicament->getId() != null) {
            return new JsonResponse(array('success' => true, 'id' => $medicament->getId()));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/addforme", name="ajax_add_forme")
     * @Method({"GET", "POST"})
     */
    public function addFormeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $forme = new Forme();
//        $forme->se($request->get('code'));
        $forme->setLibelleForme($request->get('libelle'));
        $em->persist($forme);
        $em->flush();
//        dump($medicament);die();

        if ($forme->getId() != null) {
            return new JsonResponse(array('success' => true, 'id' => $forme->getId()));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }



    /**
     * @Route("/addposologie", name="ajax_add_posologie")
     * @Method({"GET", "POST"})
     */
    public function addPosologieAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $posologie = new Posologie();
//        $forme->se($request->get('code'));
        $posologie->setLibellePosologie($request->get('libelle'));
        $em->persist($posologie);
        $em->flush();
//        dump($medicament);die();

        if ($posologie->getId() != null) {
            return new JsonResponse(array('success' => true, 'id' => $posologie->getId()));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }



    /**
     * @Route("/addduree", name="ajax_add_duree")
     * @Method({"GET", "POST"})
     */
    public function addDureeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $duree = new DureeTraitement();
//        $forme->se($request->get('code'));
        $duree->setLibelleDureeTraitement($request->get('libelle'));
        $em->persist($duree);
        $em->flush();
//        dump($medicament);die();

        if ($duree->getId() != null) {
            return new JsonResponse(array('success' => true, 'id' => $duree->getId()));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }



    /**
     * @Route("/addexampara", name="ajax_add_exam_para")
     * @Method({"GET", "POST"})
     */
    public function addExamParaAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $examPara = new ExamenParaMedical();
        $examPara->setCodeExamenPara($request->get('code'));
        $examPara->setLibelleExamenPara($request->get('libelle'));
        $em->persist($examPara);
        $em->flush();

        if ($examPara->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/addexamcli", name="ajax_add_exam_cli")
     * @Method({"GET", "POST"})
     */
    public function addExamCliAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $examCli = new Examen();
        $examCli->setCodeExamen($request->get('code'));
        $examCli->setLibelleExamen($request->get('libelle'));
        $em->persist($examCli);
        $em->flush();

        if ($examCli->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }

    /**
     * @Route("/addpatient", name="ajax_add_patient")
     * @Method({"GET", "POST"})
     */
    public function addPatientAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $patient = new Patient();
        $patient->setNomPatient($request->get('nom'));
        $patient->setPrenomPatient($request->get('prenom'));
        $patient->setTelPatient($request->get('telpatient'));
        $patient->setAdressePatient($request->get('adrespatient'));
        //$typePatient = new TypePatient();
        $typePatient = $em->getRepository('AppBundle:TypePatient')->find($request->get('typepatient'));
        $patient->setTypePatient($typePatient);
        if ($request->get('sexe') == 1){
            $patient->setSexePatient('Masculin');
        }else{
            $patient->setSexePatient('Féminin');
        }

        $em->persist($patient);
        $em->flush();

        if ($patient->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/adddiagnostic", name="ajax_add_diagnostic")
     * @Method({"GET", "POST"})
     */
    public function addDiagnosticAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $diagnostic = new Diagnostic();
        $diagnostic->setCodeDiagnostic($request->get('code'));
        $diagnostic->setLibelleDiagnostic($request->get('libelle'));
        $em->persist($diagnostic);
        $em->flush();

        if ($diagnostic->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/listpatient", name="ajax_list_patient")
     * @Method({"GET", "POST"})
     */
    public function listPatientAction(Request $request)
    {
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');

        $listePatients = $this->getDoctrine()->getRepository('AppBundle:Patient')->searchPatient($nom, $prenom);

        return $this->render('consultation/list_patient.html.twig', compact('listePatients'));
    }

    /**
     * @Route("/infopatient", name="ajax_info_patient")
     * @Method({"GET", "POST"})
     */
    public function infoPatientAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $consultation = $em->getRepository('AppBundle:Consultation')->find($id);

        if ($consultation != null){
            $consultation->setDateDebConsul(new \DateTime('now'));
            $em->persist($consultation);
            $em->flush();
        }
        $infoConsultation = $this->getDoctrine()->getRepository('AppBundle:Consultation')->
        findBy(array('id' => $id, 'deleted' => 0));
        $infoPatient = $this->getDoctrine()->getRepository('AppBundle:Patient')->
        findBy(array('id' => $infoConsultation[0]->getPatient()->getId(), 'deleted' => 0));

        $infosPatient = [];
        $infosPatient['id'] = $infoPatient[0]->getId();
        $infosPatient['adresse'] = $infoPatient[0]->getAdressePatient();
        $infosPatient['antFamili'] = $infoPatient[0]->getAntFamili();
        $infosPatient['antPercChir'] = $infoPatient[0]->getAntPercChir();
        $infosPatient['antPerMedic'] = $infoPatient[0]->getAntPerMedic();
        $infosPatient['assure'] = $infoPatient[0]->getAssurePatient();
        $infosPatient['autres'] = $infoPatient[0]->getAutres();
        if ($infoPatient[0]->getDateNaisPatient() != null){
            $infosPatient['age'] = date_format(new \DateTime("now"), ('Y')) - date_format($infoPatient[0]->getDateNaisPatient(), ('Y')) . " ans";

        }else{
            $infosPatient['age'] = "";
        }
        $infosPatient['email'] = $infoPatient[0]->getEmailPatient();
        $infosPatient['medTrait'] = $infoPatient[0]->getMedTraitant();
        $infosPatient['nom'] = $infoPatient[0]->getNomPatient();
        $infosPatient['prenom'] = $infoPatient[0]->getPrenomPatient();
        $infosPatient['sexe'] = $infoPatient[0]->getSexePatient();
        $infosPatient['situaFamili'] = $infoPatient[0]->getSituaFamiliale();
        $infosPatient['tel'] = $infoPatient[0]->getTelPatient();
        $infosPatient['poids'] = $infoConsultation[0]->getPoidsConsul();
        $infosPatient['temp'] = $infoConsultation[0]->getTempConsul();
        $infosPatient['tens'] = $infoConsultation[0]->getTensionConsul();
        $infosPatient['tail'] = $infoConsultation[0]->getTailleConsul();
        $infosPatient['observation'] = $infoConsultation[0]->getObservation();


        return new JsonResponse($infosPatient);
    }


    /**
     * @Route("/listconsultationperiode", name="ajax_list_consultation_periode")
     * @Method({"GET", "POST"})
     */
    public function listConsulationPeriodeAction(Request $request)
    {
        $dateDebut = $request->get('datedeb').' 00:00:00';
        $dateFin = $request->get('datefin').' 23:59:59';
        $dateDebut = new \DateTime($dateDebut);
        $dateFin = new \DateTime($dateFin);

        $listeConsultations = $this->getDoctrine()->getRepository('AppBundle:Consultation')
            ->listConsultationPeriode($dateDebut, $dateFin);
        $listeConsuls = [];
        foreach ($listeConsultations as $consultation) {
            $listeConsul = [];
            $listeConsul['dateConsultation'] = $consultation->getDateConsul();
            $listeConsul['patient'] = $consultation->getPatient()->getNomPatient() . ' ' . $consultation->getPatient()->getPrenomPatient();
            $listeConsul['telPatient'] = $consultation->getPatient()->getTelPatient();
            $listeConsul['sexe'] = $consultation->getPatient()->getSexePatient();
            $listeConsul['conduiteATenir'] = $consultation->getConduitATenir();
            if($consultation->getMedecin() != null){
                $listeConsul['consulterPar'] = $consultation->getMedecin()->getNom() . ' ' . $consultation->getMedecin()->getPrenom();
            }else{
                $listeConsul['consulterPar'] = "";
            }
            array_push($listeConsuls, $listeConsul);
        }

        return new JsonResponse($listeConsuls);
    }



    /**
     * @Route("/addpreconsultation", name="ajax_add_pre_consultation")
     * @Method({"GET", "POST"})
     */
    public function addPreConsultationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //dump($request->get('taille'));die();
        $consultation = new  Consultation();
        $patient = $em->getRepository('AppBundle:Patient')->find($request->get('idpatient'));
        $consultation->setPatient($patient);
        $date = new \DateTime('now');
        $consultation->setDateConsul($date);
        $consultation->setPoidsConsul($request->get('poids'));
        $consultation->setTempConsul($request->get('temp'));
        $consultation->setTailleConsul($request->get('taille'));
        $consultation->setTensionConsul($request->get('tension'));
        $consultation->setObservation($request->get('observation'));
        $em->persist($consultation);
        $em->flush();
        return new JsonResponse(array('ticket' => $consultation->getId(),
            'date' => date_format($date,'d/m/Y H:i'),
            'lepatient' => $patient->getNomPatient(). " " .$patient->getPrenomPatient()));
        /*if ($consultation->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }*/
    }

    /**
     * @Route("/controleticket", name="ajax_controle_ticket_consultation")
     * @Method({"GET", "POST"})
     */
    public function controleTicketConsultationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $consultation = $em->getRepository('AppBundle:Consultation')->findOneBy(array('id'=> $request->get('leticket'),
            'deleted' => false, 'dateFinConsul' => null));
        /*if ($consultation != null){
            $consultation->setDateDebConsul(new \DateTime('now'));
            $em->persist($consultation);
            $em->flush();
        }*/
        if ($consultation != null) {
            return new JsonResponse(array('success' => true, 'idPatient' => $consultation->getPatient()->getId()));
        } else {
            return new JsonResponse(array('success' => false, 'idPatient' => ''));
        }
    }


    /**
     * @Route("/debutconsultation", name="ajax_debut_consultation")
     * @Method({"GET", "POST"})
     */
    public function debutConsultationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $consultation = $em->getRepository('AppBundle:Consultation')->find($request->get('leticket'));
        if ($consultation != null){
            $consultation->setDateDebConsul(new \DateTime('now'));
            $em->persist($consultation);
            $em->flush();
        }

        if ($consultation != null) {
            return new JsonResponse(array('success' => true,
                'consultation'=>$consultation));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }
}
