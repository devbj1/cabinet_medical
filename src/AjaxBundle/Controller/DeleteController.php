<?php

namespace AjaxBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Delete controller.
 *
 * @Route("delete")
 */
class DeleteController extends Controller
{


    /**
     * @Route("/deletedpatient", name="ajax_deleted_patient")
     * @Method({"GET", "POST"})
     */
    public function deletedPatientAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $patient = $em->getRepository('AppBundle:Patient')->find($id);
        $patient->setDeleted(1);
        $em->persist($patient);
        $em->flush();

        if ($patient->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deletedposologie", name="ajax_deleted_posologie")
     * @Method({"GET", "POST"})
     */
    public function deletedPosologieAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $posologie = $em->getRepository('AppBundle:Posologie')->find($id);
        $posologie->setDeleted(1);
        $em->persist($posologie);
        $em->flush();

        if ($posologie->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deleteddiagnostic", name="ajax_deleted_diagnostic")
     * @Method({"GET", "POST"})
     */
    public function deletedDiagnosticAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $diagnostic = $em->getRepository('AppBundle:Diagnostic')->find($id);
        $diagnostic->setDeleted(1);
        $em->persist($diagnostic);
        $em->flush();

        if ($diagnostic->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deleteddureetraitement", name="ajax_deleted_duree_traitement")
     * @Method({"GET", "POST"})
     */
    public function deletedDureeTraitementAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $dureeTraitement = $em->getRepository('AppBundle:DureeTraitement')->find($id);
        $dureeTraitement->setDeleted(1);
        $em->persist($dureeTraitement);
        $em->flush();

        if ($dureeTraitement->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deletedexamen", name="ajax_deleted_examen")
     * @Method({"GET", "POST"})
     */
    public function deletedExamenAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $examen = $em->getRepository('AppBundle:Examen')->find($id);
        $examen->setDeleted(1);
        $em->persist($examen);
        $em->flush();

        if ($examen->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deletedexamenparamedical", name="ajax_deleted_examen_para_medical")
     * @Method({"GET", "POST"})
     */
    public function deletedExamenParaMedicalAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $examenParaMedical = $em->getRepository('AppBundle:ExamenParaMedical')->find($id);
        $examenParaMedical->setDeleted(1);
        $em->persist($examenParaMedical);
        $em->flush();

        if ($examenParaMedical->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deletedforme", name="ajax_deleted_forme")
     * @Method({"GET", "POST"})
     */
    public function deletedFormeAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $forme = $em->getRepository('AppBundle:Forme')->find($id);
        $forme->setDeleted(1);
        $em->persist($forme);
        $em->flush();

        if ($forme->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deletedmedicament", name="ajax_deleted_medicament")
     * @Method({"GET", "POST"})
     */
    public function deletedMedicamentAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $medicament = $em->getRepository('AppBundle:Medicament')->find($id);
        $medicament->setDeleted(1);
        $em->persist($medicament);
        $em->flush();

        if ($medicament->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }


    /**
     * @Route("/deletedmotif", name="ajax_deleted_motif")
     * @Method({"GET", "POST"})
     */
    public function deletedMotifAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $motif = $em->getRepository('AppBundle:Motif')->find($id);
        $motif->setDeleted(1);
        $em->persist($motif);
        $em->flush();

        if ($motif->getId() != null) {
            return new JsonResponse(array('success' => true));
        } else {
            return new JsonResponse(array('success' => false));
        }
    }
}
