<?php

namespace AjaxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/ajax")
     */
    public function indexAction()
    {
        return $this->render('AjaxBundle:Default:index.html.twig');
    }
}
