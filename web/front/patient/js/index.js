//Datatable

var table = $("#tablelistepatient").DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});



$("#tablelistepatient").on('click', '.deleted', function (e) {
    e.preventDefault();
    var btn = $(this);
    var id = $(this).attr('data-id');


    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "Voulez-vous vraiment supprimer cet patient ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            btn.button('loader');
            $.ajax({
                url: deletedPatientUrl,
                type: "POST",
                data: {
                    id: id
                },
                success: function (data) {

                    if (data['success'] === true) {
                        swalWithBootstrapButtons.fire(
                            'Supprimer !',
                            'Patient supprimer avec success !',
                            'success'
                        );
                        btn.closest('td').closest('tr').fadeOut(2000, function () {
                            btn.closest('td').closest('tr').remove();
                        });
                    } else {
                        swalWithBootstrapButtons.fire(
                            'Supprimer!',
                            'Echec de la suppression du patient!',
                            'error'
                        )

                        //$.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                    }

                },
                error: function (error) {

                }
            })

        }
    });
});