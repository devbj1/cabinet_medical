//Action modal Demande
var modalAjoutDemande = $('#ModalAjouterDemande');
var ModalDropPieceDemande = $('#ModalDropPieceDemande');
var ModalAjoutProspect = $('#ModalAjoutProspect');
var ModalProspect = $('#ModalProspect');
var ModalHistorique = $('#ModalHistoriqueDemande');
var ModalAffectDemande = $('#ModalAffectationDemande');
var ModalAffectDemande2 = $('#ModalAffectationDemande2');
var loader = '<div class="loader text-center mt-3 mb-3"><i class="fa fa-spinner fa-spin fa-3x"></i></div>';
var leProspect = [];
var allAgent = [];
var allDelete = [];
var iddemande = '';
var boutonselectionner;
var detailshow = false;


////Gestion des nombre de mot du champs observations
$(document).ready(function (e) {

    $('#observationdemande').keyup(function () {

        var nombreCaractere = $(this).val().length;

        var nombreMots = jQuery.trim($(this).val()).split(' ').length;
        if ($(this).val() === '') {
            nombreMots = 0;
        }

        var msg = 'Observation ' + nombreMots + 'mot(s)| ' + nombreCaractere + 'Caractere(s)/200';
        // var msg = 'Observation ' + nombreMots + ' mot(s) | ' + nombreCaractere + ' Caractere(s)/200';
        $('#compteur').text(msg);
        if (nombreCaractere > 150) {
            $('#compteur').addClass("mauvais");
        } else {
            $('#compteur').removeClass("mauvais");
        }

    })


});

function videmodalaffectation() {
    ModalAffectDemande.find('#affecteworkflow').val('');
    ModalAffectDemande.find('#affecteworkflow').attr('data-id', 0);
    ModalAffectDemande.find('.line').each(function () {
        var line = $(this);
        if (line.attr('id') === '1') {
            // $('#societe1').html(contenucombosociete);
            $('#delaiaffectationdemande1').val('');
        } else {
            line.remove();
        }
    });
}


var contentmodalimage = $('#contenudropzone').html();


$("#datedebutaffecteworkflow").datetimepicker({
    format: 'YYYY-MM-DD HH:mm',
    defaultDate: moment(),
    locale: 'fr'
});

//Action modal DropZone
// var modalCloseDemande = $('#closemodalworkflow');

// $('#closemodalPiece').on('click', function (e) {
//     e.preventDefault();
//     var modalPiece = $('#ModalDropPieceDemande');
//     modalPiece.modal('hide');
// });

// $('#rechargemodalPiece').on('click', function (e) {
//     e.preventDefault();
//     $('#my-dropzone').html(contentmodalimage);
//     $('#dropfilelabel').addClass('default');
// });


//Datatable

var table = $('#tablelistedemande').DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});

$('#tablelistedemande_length').addClass('form-control');
$('#tablelistedemande_filter').find('input').addClass('form-control');

// $('#tablelistedemande input').addClass('form-group');
// var modalCloseDemande = $('#closemodalworkflow');

$('#btnopenDemande').on('click', function (e) {
    e.preventDefault();
    modalAjoutDemande.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
    modalAjoutDemande.modal('show');
    $("#datereceptiondemande").datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        defaultDate: moment(),
        locale: 'fr'
    });
});


// $('#btnopenmodaladdprospect').on('click', function (e) {
//     e.preventDefault();
//
//     // ModalAjoutProspect.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
//     ModalAjoutProspect.modal('show');
// });

// $('#btnjoindrepiece').on('click', function (e) {
//     e.preventDefault();
//     ModalDropPieceDemande.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
//     ModalDropPieceDemande.modal('show');
// });

// $('#closemodalDemande').on('click', function (e) {
//     e.preventDefault();
//     // alert('ok')
//     modalAjoutDemande.removeClass('animated bounceInLeft').addClass('animated bounceOutDown');
//     modalAjoutDemande.modal('hide');
//
// });

// $('#closemodalProspect').on('click', function (e) {
//     e.preventDefault();
//     // alert('ok')
//     ModalProspect.removeClass('animated rollIn').addClass('animated rollOut');
//     ModalProspect.modal('hide');
//
// });

// $('#closemodaladdProspect').on('click', function (e) {
//     e.preventDefault();
//     // alert('ok')
//     // ModalProspect.removeClass('animated rollIn').addClass('animated rollOut');
//     ModalAjoutProspect.modal('hide');
//
// });


// $('#btnopenprospectrecherche').on('click', function (e) {
//     e.preventDefault();
//     ModalProspect.removeClass('animated rollOut').addClass('animated rollIn');
//     ModalProspect.modal('show');
// });


// $('#btnsearchprospect').on('click', function (e) {
//     e.preventDefault();
//     var formPanel = $(this).closest('.modal-body').find('#listeprospect');
//     // formPanel.hide('slow');
//
//     var nom = $('#nomprospect');
//     var prenom = $('#prenomprospect');
//     var btnrecherche = $(this);
//
//
//     if (nom.val() ==='' && prenom.val() ===''){
//         $.notify('Veuillez saisir au moins le nom ou le prénom','error');
//         nom.focus();
//     }else{
//         formPanel.html(loader);
//         btnrecherche.button('loading');
//         $.ajax({
//             url: prospectListUrl,
//             type: "GET",
//             data: {
//                 nom: nom.val(),
//                 prenom: prenom.val(),
//             },
//             success: function (data) {
//                 // console.log(data)
//                 btnrecherche.button('reset');
//                 if (data === null) {
//                     $.notify(
//                         "Aucun prospect trouvé",
//                         {position: "right"},
//                         "warn"
//                     );
//                 } else {
//                     $('.footermodalprospect').show('slow');
//                     $('.footermodalprospect').html('<div class="pull-left" >\n' +
//                         '                            <button type="button" class="btn" id="closemodalProspect">\n' +
//                         '                                Annuler\n' +
//                         '                            </button>\n' +
//                         '                        </div>\n' +
//                         '                        <div class="pull-right">\n' +
//                         '                            <button data-url="{{ path(\'ajax_demande_new\') }}"\n' +
//                         '                                    class="btn btn-primary" id="btnopenmodaladdprospect"\n' +
//                         '                                    type="submit"> <i class="fa fa-plus"></i> Nouveau\n' +
//                         '                            </button>\n' +
//                         '                        </div>')
//
//                     $('#btnopenmodaladdprospect').on('click', function (e) {
//                         e.preventDefault();
//                         // ModalAjoutProspect.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
//                         ModalAjoutProspect.modal('show');
//                     });
//                     $('#closemodalProspect').on('click', function (e) {
//                         e.preventDefault();
//                         // alert('ok')
//                         ModalProspect.removeClass('animated rollIn').addClass('animated rollOut');
//                         ModalProspect.modal('hide');
//
//                     });
//
//                 }
//
//                 formPanel.html(data);
//                 $('.btnselectionneProspect').on('click', function (e) {
//                     e.preventDefault();
//                     // alert('ok')
//                     var idPublicProspect = $(this).attr('id');
//                     var nomProspect = $(this).closest('.ligne').find('.nomProspect').text();
//                     var prenomProspect = $(this).closest('.ligne').find('.prenomProspect').text();
//                     var sexeProspect = $(this).closest('.ligne').find('.sexeProspect').text();
//                     var contactProspect = $(this).closest('.ligne').find('.contactProspect').text();
//                     var emailProspect = $(this).closest('.ligne').find('.emailProspect').text();
//
//                     leProspect = {
//                         'idPublic': idPublicProspect,
//                         'nom': nomProspect,
//                         'prenom': prenomProspect,
//                         'sexe': sexeProspect,
//                         'contact': contactProspect,
//                         'email': emailProspect
//                     };
//
//                     // console.log(leProspect)
//                     $('#prospectdemande').val(nomProspect + ' ' + prenomProspect);
//                     ModalProspect.modal('hide');
//                     // $('#closemodaladdProspect').click();
//
//                 });
//             },
//             error: function (error) {
//                 btn.button('reset');
//                 swal({
//                     title: "Alert!",
//                     icon: "error",
//                     button: false,
//                     text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
//                     timer: 1500
//                 });
//                 formPanel.html('');
//                 btnrecherche.button('reset');
//             }
//
//
//         })
//     }
//
//
//     // ModalProspect.modal('show');
// });


// $('#btnadddemande').on('click', function (e) {
//     e.preventDefault();
//     var reference = $("#referencedemande");
//     var idsociete = $("#societedemande");
//     var idtypedemande = $("#typedemandedemande");
//     var datereception = $("#datereceptiondemande");
//     var prospect = $("#prospectdemande");
//     var source = $("#sourcedemande");
//     var observation = $("#observationdemande");
//     var priorite = $("#prioritedemande");
//
//     if (reference.val().trim() === '') {
//         reference.notify(
//             "Veuillez saisir la reference de la demande",
//             {position: "right"},
//             "warn"
//         );
//         reference.focus();
//     } else {
//         if (datereception.data("date") === '') {
//             datereception.notify(
//                 "Veuillez selectionner une date et saisir une heure",
//                 {position: "right"},
//                 "warn"
//             );
//             datereception.focus();
//         } else {
//             if (idtypedemande.val() === null) {
//                 idtypedemande.notify(
//                     "Veuillez selectionner le type de demande",
//                     {position: "right"},
//                     "warn"
//                 );
//                 idtypedemande.focus();
//             } else {
//                 if (prospect.val() === null) {
//                     prospect.notify(
//                         "Veuillez selectionner le prospect",
//                         {position: "right"},
//                         "warn"
//                     );
//                     prospect.focus();
//                 } else {
//                     if (source.val() === null) {
//                         source.notify(
//                             "Veuillez selectionner la source",
//                             {position: "right"},
//                             "warn"
//                         );
//                         source.focus();
//                     } else {
//                         if (priorite.val() === null) {
//                             priorite.notify(
//                                 "Veuillez selectionner la priorite",
//                                 {position: "right"},
//                                 "warn"
//                             );
//                             priorite.focus();
//                         } else {
//                             if (idsociete.val() === null) {
//                                 idsociete.notify(
//                                     "Veuillez selectionner la societe",
//                                     {position: "right"},
//                                     "warn"
//                                 );
//                                 idsociete.focus();
//                             } else {
//                                 var btn = $(this);
//                                 var tab = {
//                                     'reference': reference.val(),
//                                     'idsociete': idsociete.val(),
//                                     'idtypedemande': idtypedemande.val(),
//                                     'datereception': datereception.data("date"),
//                                     // 'prospect': prospect.val(),
//                                     'idsource': source.val(),
//                                     'observation': observation.val(),
//                                     'idpriorite': priorite.val(),
//                                 };
//                                 // console.log(leProspect)
//                                 btn.button('loading');
//                                 $.ajax({
//                                     url: demandeAddUrl,
//                                     type: "POST",
//                                     data: {
//                                         'tab': tab,
//                                         'prospect': leProspect,
//                                     },
//                                     success: function (data) {
//                                         btn.button('reset');
//                                         swal({
//                                             title: "Alert!",
//                                             icon: "success",
//                                             button: false,
//                                             text: "Enregistrement effectué avec succes",
//                                             timer: 1500
//                                         });
//                                         // console.log(data);
//                                         modalAjoutDemande.modal('hide');
//                                         table.row.add([
//                                             data.reference,
//                                             data.dateReception + " " + data.heureReception,
//                                             data.societe,
//                                             data.typeDemande,
//                                             data.prospect,
//                                             data.transitionEnCours,
//                                             "<td class=\"col-md-2 centered\">\n" +
//                                             "\n" +
//                                             "                                    <a type=\"button\"\n" +
//                                             "                                                class=\"btn btn-theme affect-demande\"\n" +
//                                             "                                       data-loading-text=\"<i class='fa fa-refresh fa-spin'></i>\"\n" +
//                                             "                                       id="+data.id+"\n" +
//                                             "                                    >\n" +
//                                             "                                        <i class=\"fa fa-forward\"></i></a>\n" +
//                                             "                                    <a type=\"button\"\n" +
//                                             "                                       class=\"btn btn-success affect-detail\"\n" +
//                                             "                                       data-loading-text=\"<i class='fa fa-refresh fa-spin'></i>\"\n" +
//                                             "                                       id="+data.id+"\n" +
//                                             "                                    >\n" +
//                                             "                                        <i class=\"fa fa-book\"></i></a>\n" +
//                                             "                                    <a type=\"button\"\n" +
//                                             "                                                class=\"btn btn-danger delete-demande\"\n" +
//                                             "                                       data-loading-text=\"<i class='fa fa-refresh fa-spin'></i>\"\n" +
//                                             "                                       id="+data.id+"\n" +
//                                             "                                    >\n" +
//                                             "                                        <i class=\"fa fa-trash\"></i></a>\n" +
//                                             "                                </td>"
//                                         ]).draw();
//                                         var nbrligneafficher = $('#tablelistedemande_length').find('select').val();
//                                         $('input').val('');
//                                         $('select').val(0);
//                                         $('#typedemandedemande').html('');
//                                         $('#my-dropzone').html(contentmodalimage);
//                                         $('#tablelistedemande_length').find('select').val(nbrligneafficher);
//                                         // ModalAffectDemande.removeClass('animated bounceInLeft').addClass('animated bounceOutDown');
//                                         // ModalAffectDemande.modal('hide');
//                                         btndemandeblindtable();
//                                     },
//                                     error: function (error) {
//                                         btn.button('reset');
//                                         swal({
//                                             title: "Alert!",
//                                             icon: "error",
//                                             button: false,
//                                             text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
//                                             timer: 1500
//                                         });
//                                     }
//                                 });
//                             }
//
//
//                         }
//                     }
//                 }
//
//
//             }
//         }
//     }
// });

// $('#btnenregistrerprospect').on('click', function (e) {
//     e.preventDefault();
//     var nom = $("#nomprospectenreg");
//     var prenom = $("#prenomprospectenreg");
//     var telephone = $("#customer_phone");
//     var sexe = $("#sexeprospectenreg");
//     var mail = $("#emailprospectenreg");
//
//     if (nom.val().trim() === '') {
//         nom.notify(
//             "Veuillez saisir le nom",
//             {position: "right"},
//             "warn"
//         );
//         nom.focus();
//     } else {
//         if (prenom.val().trim() === '') {
//             prenom.notify(
//                 "Veuillez saisir le prenom",
//                 {position: "right"},
//                 "warn"
//             );
//             prenom.focus();
//         } else {
//             if (telephone.val().trim() === '') {
//                 telephone.notify(
//                     "Veuillez saisir le telephone",
//                     {position: "right"},
//                     "warn"
//                 );
//                 telephone.focus();
//             } else {
//                 if (sexe.val() === null) {
//                     sexe.notify(
//                         "Veuillez selectionner le sexe",
//                         {position: "right"},
//                         "warn"
//                     );
//                     sexe.focus();
//                 } else {
//                     var btn = $(this);
//                     var tab = {
//                         'nom': nom.val(),
//                         'prenom': prenom.val(),
//                         'sexe': sexe.val(),
//                         'telephone': telephone.val(),
//                         'email': mail.val(),
//                     };
//                     btn.button('loading');
//                     $.ajax({
//                         url: prospectAddUrl,
//                         type: "POST",
//                         data: {
//                             'prospect': tab,
//                         },
//                         success: function (data) {
//                             btn.button('reset');
//                             // $.notify("Enregistrement effectué avec succes !", "success");
//                             swal({
//                                 title: "Alert!",
//                                 icon: "success",
//                                 button: false,
//                                 text: "Enregistrement effectué avec succes",
//                                 timer: 1500
//                             });
//                             ModalAjoutProspect.modal('hide');
//                             ModalProspect.modal('hide');
//                             ModalAjoutProspect.find('input').val('');
//                         },
//                         error: function (data) {
//                             btn.button('reset');
//                             // $.notify("Enregistrement effectué avec succes !", "success");
//                             swal({
//                                 title: "Alert!",
//                                 icon: "error",
//                                 button: false,
//                                 text: "Echec enregistrement",
//                                 timer: 1500
//                             });
//                             // modalAjoutDemande.modal('hide');
//                         }
//                     });
//                 }
//
//
//             }
//         }
//     }
//
// });


$('#btnretourdemande').click(function () {
    // ModalAffectDemande.removeClass('animated slideOutLeft').addClass('animated slideInLeft');
    ModalAffectDemande.modal('show');

    // ModalAffectDemande2.removeClass('animated slideInRight').addClass('animated slideOutLeft');
    ModalAffectDemande2.modal('hide');
});


$('#btnsuivantdemande').click(function () {

    var btnsuivant = $(this);

    if ($('#interrupteur').attr('data-value') === 'off') {
        var service = $("#serviceaffectationdemande1");
        var agent = $("#agentaffectationdemande1");
        var delai = $("#delaiaffectationdemande1");
        // console.log(agent.val(),service.val(),delai.val())
        if (service.val() === '0') {
            service.notify(
                "Veuillez selectionner le service",
                {position: "right"},
                "warn"
            );
            service.focus();
        } else {
            if (agent.val() === null) {
                agent.notify(
                    "Veuillez selectionner l'agent",
                    {position: "right"},
                    "warn"
                );
                agent.focus();
            } else {
                if (delai.val() === '') {
                    delai.notify(
                        "Veuillez saisir le delai",
                        {position: "right"},
                        "warn"
                    );
                    delai.focus();
                } else {
                    // ModalAffectDemande2.removeClass('animated slideOutLeft').addClass('animated slideInRight');
                    ModalAffectDemande2.modal('show');

                    // ModalAffectDemande.removeClass('animated slideInRight').addClass('animated slideOutLeft');
                    ModalAffectDemande.modal('hide');
                }
                ;
            }
            ;
        }
        ;
    } else {
        if ($('#affecteworkflow').attr('data-id') === '0') {
            $.notify(
                "Aucun processus normal défini ! Veuillez passez au processus exceptionnelle",
                "warn"
            );
        } else {
            // ModalAffectDemande2.removeClass('animated slideOutLeft').addClass('animated slideInRight');
            ModalAffectDemande2.modal('show');

            // ModalAffectDemande.removeClass('animated slideInRight').addClass('animated slideOutLeft');
            ModalAffectDemande.modal('hide');
        }
    }


});


$('#btnaffectedemande').click(function (e) {
    e.preventDefault();
    var idworkflow = $("#affecteworkflow").attr('data-id');
    var btn = $(this);
    var datedebutdecompte = $('#datedebutaffecteworkflow');
    var dateecheancedecompte = $('#dateecheanceaffecteworkflow');
    var rappel = $('#delairappelaffectationdemande');

    swal({
        title: "Confirmation !",
        text: "Voulez-vous vraiment affecter cette demande!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function (isConfirm) {
        if (isConfirm) {

            if ($('#interrupteur').attr('data-value') === 'on') {
                var delai = $('#delaiaffecteworkflow');
                if (delai.val() === '') {
                    delai.notify(
                        "Veuillez saisir le délai de la demande",
                        {position: "right"},
                        "warn"
                    );
                    delai.focus();
                } else {
                    if (datedebutdecompte.data("date") === '') {
                        datedebutdecompte.notify(
                            "Veuillez sélectionner la date début décompte",
                            {position: "down"},
                            "warn"
                        );
                        datedebutdecompte.focus();
                    } else {
                        if (dateecheancedecompte.data("date") === '') {
                            dateecheancedecompte.notify(
                                "Veuillez sélectionner la date d'écheance",
                                {position: "down"},
                                "warn"
                            );
                            dateecheancedecompte.focus();
                        } else {
                            btn.button('loading');
                            $.ajax({
                                url: affectdemandeUrl,
                                type: "GET",
                                data: {
                                    'iddemande': iddemande,
                                    'idworkflow': idworkflow,
                                    'action': 'next',
                                    'datedebutdecompte': datedebutdecompte.data("date"),
                                    'datefindecompte': dateecheancedecompte.data("date"),
                                    'rappel': rappel.val(),
                                },
                                success: function (data) {
                                    btn.button('reset');
                                    swal({
                                        title: "Alert!",
                                        icon: "success",
                                        button: false,
                                        text: "Affectation effectué avec succes",
                                        timer: 1500
                                    });
                                    videmodalaffectation();
                                    // ModalAffectDemande2.removeClass('animated bounceInLeft').addClass('animated bounceOutDown');
                                    ModalAffectDemande2.modal('hide');
                                    ModalAffectDemande.modal('hide');
                                    boutonselectionner.closest('tr').find('.affect-demande').addClass('disabled').removeClass('affect-demande');
                                    boutonselectionner.closest('tr').find('.delete-demande').addClass('disabled').removeClass('delete-demande');
                                    var idcolonne = 1;
                                    boutonselectionner.closest('tr').find('td').each(function () {
                                        if (idcolonne === 6) {
                                            $(this).text(data);
                                        }
                                        idcolonne = idcolonne + 1;
                                    });
                                    allAgent = [];
                                },
                                error: function (error) {
                                    btn.button('reset');
                                    swal({
                                        title: "Alert!",
                                        icon: "error",
                                        button: false,
                                        text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                                        timer: 1500
                                    });
                                }

                            })
                        }
                    }
                }


            } else {

                if (addAgent() !== false) {

                    //Recuperer la liste des agents pour la procedure execptionnelle
                    var tablisteagent = [];
                    $('#blockagent').find('.line').each(function () {
                        tablisteagent.push({
                            'agent': $(this).find('.agentaffectationdemande').val(),
                            'delai': $(this).find('.delaiaffectationdemande').val(),
                        });
                    });

                    // console.log(tablisteagent);
                    btn.button('loading');
                    $.ajax({
                        url: affectdemandeProcedureExceptionnelleUrl,
                        type: "GET",
                        data: {
                            'iddemande': iddemande,
                            'idworkflow': idworkflow,
                            'action': 'next',
                            'listeagent': tablisteagent,
                            'datedebutdecompte': datedebutdecompte.data("date"),
                            'datefindecompte': dateecheancedecompte.data("date"),
                            'rappel': rappel.val(),
                        },
                        success: function (data) {
                            btn.button('reset');
                            swal({
                                title: "Alert!",
                                icon: "success",
                                button: false,
                                text: "Affectation effectué avec succes",
                                timer: 1500
                            });
                            videmodalaffectation();
                            // ModalAffectDemande2.removeClass('animated bounceInLeft').addClass('animated bounceOutDown');
                            ModalAffectDemande2.modal('hide');
                            ModalAffectDemande.modal('hide');
                            boutonselectionner.closest('tr').find('.affect-demande').addClass('disabled').removeClass('affect-demande');
                            boutonselectionner.closest('tr').find('.delete-demande').addClass('disabled').removeClass('delete-demande');
                            var idcolonne = 1;
                            boutonselectionner.closest('tr').find('td').each(function () {
                                if (idcolonne === 6) {
                                    $(this).text(data);
                                }
                                idcolonne = idcolonne + 1;
                            });
                            allAgent = [];

                        },
                        error: function (error) {
                            btn.button('reset');
                            swal({
                                title: "Alert!",
                                icon: "error",
                                button: false,
                                text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                                timer: 1500
                            });
                        }

                    })

                }
            }
        }
    });


});


$('#closemodalAffecteDemande').on('click', function (e) {
    e.preventDefault();
    // alert('ok')
    ModalAffectDemande.removeClass('animated bounceInLeft').addClass('animated bounceOutDown');
    ModalAffectDemande.modal('hide');

});


// Supprimer une demande

// Affecter une demande a un workflow
function btndemandeblindtable() {
    $("#tablelistedemande").on('click', '.affect-demande', function (e) {
        e.preventDefault();

        // console.log('ok1')
        boutonselectionner = $(this);
        iddemande = $(this).attr('id');
        boutonselectionner.button('loading');
        $.ajax({
            url: demandedetailworkflowListUrl.replace('lecode', iddemande),
            type: "GET",
            success: function (data) {
                boutonselectionner.button('reset');
                // console.log('ok');

                $('#affecteworkflow').attr('data-id', 0);
                $('#affecteworkflow').val('');
                $('#delaiaffecteworkflow').val(0);
                data.forEach(function (g) {
                    $('#affecteworkflow').attr('data-id', g.id);
                    $('#affecteworkflow').val(g.libelleworkflow);
                    $('#delaiaffecteworkflow').val(g.delai);
                });
                // console.log($('#affecteworkflow').attr('data-id'));

                if ($('#affecteworkflow').attr('data-id') === '0') {
                    $('#alert-processus').removeClass('hidden');
                    $('.show-workflow-demande').addClass('disabled');
                } else {
                    $('#alert-processus').addClass('hidden');
                    $('.show-workflow-demande').removeClass('disabled');
                }
                // var lelienaffichage = $(".show-workflow-demande").attr('data-href');
                // console.log($("#affecteworkflow").attr('data-id'))
                $(".show-workflow-demande").attr('id', $("#affecteworkflow").attr('data-id'));
                // $(".show-workflow-demande").attr('data-href', lelienaffichage.replace('lecode', $("#affecteworkflow").attr('data-id')));
                // console.log($(".show-workflow-demande").attr('id'),$(".show-workflow-demande").attr('data-href'))
                // ModalAffectDemande.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
                ModalAffectDemande.modal('show');
                //On recuperer la date
                var dateaffect = $("#datedebutaffecteworkflow");
                //On le met dans une variable de type date
                var hours = $("#delaiaffecteworkflow");
                var result = new Date(dateaffect.data("date"));
                var momentdate = moment(result, 'YYYY-MM-DD HH:mm').add(hours.val(), 'hours');
                $("#dateecheanceaffecteworkflow").datetimepicker({
                    format: 'YYYY-MM-DD HH:mm',
                    defaultDate: momentdate,
                    locale: 'fr'
                });
            },
            error: function (error) {
                boutonselectionner.button('reset');
                swal({
                    title: "Alert!",
                    icon: "error",
                    button: false,
                    text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                    timer: 1500
                });
            }
        });


    });

    $("#tablelistedemande").on('click', '.delete-demande', function (e) {
        e.preventDefault();
        iddemande = $(this).attr('id');
        var btn = $(this);
        swal({
            title: "Confirmation !",
            text: "Voulez-vous vraiment supprimer cet enregistrement!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (isConfirm) {
            if (isConfirm) {
                btn.button('loading');
                $.ajax({
                    url: demandeDeleteUrl.replace('lecode', iddemande),
                    type: "GET",
                    success: function (data) {

                        btn.button('reset');
                        // swal({
                        //     title: "Alert!",
                        //     icon: "success",
                        //     button: false,
                        //     text: "Suppression effectué avec succes",
                        //     timer: 1500
                        // });
                        btn.closest('td').closest('tr').fadeOut(2000, function () {
                            btn.closest('td').closest('tr').remove();
                        });
                    },
                    error: function (error) {
                        btn.button('reset');
                        swal({
                            title: "Alert!",
                            icon: "error",
                            button: false,
                            text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                            timer: 1500
                        });
                    }
                });
            }
        });


    });


    $("#tablelistedemande").on('click', '.historique-demande', function (e) {
        e.preventDefault();
        var btnhistorique = $(this);

        iddemande = $(this).attr('id');
        // btnhistorique.button('loading');
        ModalHistorique.modal('show');

        var blockdescription = $("#description");
        var blockprocedureworkflow = $("#procedureworkflow");
        var blocksuividemande = $("#suividemande");
        var blockpiecejointe = $("#lespiecesjointes");
        var blockcommentaire = $("#lescommentaires");
        blockdescription.html(loader);
        blocksuividemande.html(loader);
        blockpiecejointe.html(loader);
        blockcommentaire.html(loader);
        blockprocedureworkflow.html(loader);

        // btnhistorique.button('reset');

        //Ajax block description
        $.ajax({
            url: historiquedetaildemandeUrl.replace('lecode',iddemande),
            type: "POST",
            data: iddemande,
            success: function (data) {
                blockdescription.show('slow');
                // console.log(data);
                $("#description").html("<div class=\"block row\">\n" +
                    "                                    <div class=\"form-group col-md-3\">\n" +
                    "                                        <label>Reférence: </label>\n" +
                    "                                        <label class=\"label-custom\" id=\"referencedemande\">"+data[0].reference+"</label>\n" +
                    "                                    </div>\n" +
                    "                                    <div class=\"form-group col-md-5\">\n" +
                    "                                        <label> Date Réception: </label>\n" +
                    "                                        <label class=\"label-custom\" id=\"datereceptiondemande\">"+data[0].datereception+"</label>\n" +
                    "                                    </div>\n" +
                    "                                    <div class=\"form-group col-md-4\">\n" +
                    "                                        <label>Source: </label>\n" +
                    "                                        <label class=\"label-custom\" id=\"sourcedemande\">"+data[0].source+"</label>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"block row\">\n" +
                    "                                    <div class=\"form-group col-md-3\">\n" +
                    "                                        <label>Societe: </label>\n" +
                    "                                        <label class=\"label-custom\" id=\"societedemande\">"+data[0].societe+"</label>\n" +
                    "                                    </div>\n" +
                    "                                    <div class=\"form-group col-md-5\">\n" +
                    "                                        <label>Type Demande: </label>\n" +
                    "                                        <label class=\"label-custom\" id=\"typedemandedemande\">"+data[0].typedemande+"</label>\n" +
                    "                                    </div>\n" +
                    "                                    <div class=\"form-group col-md-4\">\n" +
                    "\n" +
                    "                                        <label>Prospect: </label>\n" +
                    "                                        <label class=\"label-custom\" id=\"prospectdemande\">"+data[0].prospect+"</label>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"block row\">\n" +
                    "\n" +
                    "                                    <div class=\"form-group col-md-3\">\n" +
                    "                                        <label>Priorité: </label>\n" +
                    "                                        <label class=\"label-custom\" id=\"prioritedemande\">"+data[0].priorite+"</label>\n" +
                    "\n" +
                    "                                    </div>\n" +
                    "                                    <div class=\"form-group col-md-9\">\n" +
                    "                                        <label> Observation: </label>\n" +
                    "                                        <label class=\"label-custom\" id=\"observationdemande\">"+data[0].observation+"</label>\n" +
                    "                                    </div>\n" +
                    "                                </div>");

                // btnhistorique.button('reset');
                // btn.closest('td').closest('tr').fadeOut(2000, function () {
                //     btn.closest('td').closest('tr').remove();
                // });
            },
            error: function (error) {
                // btn.button('reset');
                swal({
                    title: "Alert!",
                    icon: "error",
                    button: false,
                    text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                    timer: 1500
                });
            }
        });

        //Ajax block procedureworkflow
        $.ajax({
            url: historiqueprocessusworkflowdemandeUrl.replace('lecode',iddemande),
            type: "POST",
            data: iddemande,
            success: function (data) {

                blockprocedureworkflow.html(data);
                blockprocedureworkflow.show('slow');

            },
            error: function (error) {
                // btn.button('reset');
                swal({
                    title: "Alert!",
                    icon: "error",
                    button: false,
                    text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                    timer: 1500
                });
            }
        });

        //Ajax block suivi demande
        $.ajax({
            url: historiquesuividemandeUrl.replace('lecode',iddemande),
            type: "POST",
            data: iddemande,
            success: function (data) {

                blocksuividemande.html(data);
                blocksuividemande.show('slow');

            },
            error: function (error) {
                // btn.button('reset');
                swal({
                    title: "Alert!",
                    icon: "error",
                    button: false,
                    text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                    timer: 1500
                });
            }
        });


        //Ajax block image
        $.ajax({
            url: historiquelistepiecejointedemandeUrl.replace('lecode', iddemande),
            type: "POST",
            success: function (data) {
                $("#lespiecesjointes").text('');
                // console.log(data)
                data.forEach(function (g) {
                    newImageHistoriquedemande(g.id, g.libelle, g.chemin)
                })
            },
            error: function (error) {

            }
        });


        //Ajax block commentaire
        $.ajax({
            url: historiquelistecommentairedemandeUrl.replace('lecode', iddemande),
            type: "POST",
            data: iddemande,
            success: function (data) {
                commentsArray = data;
                var saveComment = function (dataCommentaire) {
                    $.ajax({
                        url: historiqueaddcommentaireUrl.replace('lecode', iddemande),
                        type: "POST",
                        dataType: "json",
                        data: dataCommentaire,
                        success: function (data) {
                        },
                        error: function (error) {

                        }
                    });
                    return dataCommentaire;
                };
                var updateComment = function (dataCommentaire) {
                    $.ajax({
                        url: historiqueupdatecommentaireUrl.replace('lecode', iddemande),
                        type: "POST",
                        dataType: "json",
                        data: dataCommentaire,
                        success: function (data) {
                        },
                        error: function (error) {

                        }
                    });
                    return dataCommentaire;
                };

                $.ajax({
                    url: historiquelistutilisateurUrl,
                    type: "GET",
                    success: function (data) {
                        usersArray = data;

                        // console.log('ok')
                        $("#lescommentaires").comments({
                            // User
                            profilePictureURL: historiqueprofilpictureUrl,
                            currentUserIsAdmin: false,
                            currentUserId: 14,

                            // Font awesome icon overrides
                            spinnerIconURL: '',
                            upvoteIconURL: '',
                            replyIconURL: '',
                            uploadIconURL: '',
                            attachmentIconURL: '',
                            fileIconURL: '',
                            noCommentsIconURL: '',

                            // Strings to be formatted (for example localization)
                            textareaPlaceholderText: 'Ajouter un commentaire',
                            newestText: 'Plus récent',
                            oldestText: 'Le plus ancien',
                            popularText: 'Populaire',
                            attachmentsText: ' Les pièces jointes',
                            sendText: 'Envoyer',
                            replyText: 'Repondre',
                            editText: 'Modifier',
                            editedText: 'Modifié',
                            youText: 'Vous',
                            saveText: 'Sauvegarder',
                            deleteText: 'Supprimer',
                            newText: 'Nouveau',
                            viewAllRepliesText: 'Afficher toutes les réponses',
                            hideRepliesText: 'Cacher la réponse',
                            noCommentsText: 'Pas de commentaire',
                            noAttachmentsText: 'Pas d' / 'attachement',
                            attachmentDropText: 'Deposez les fichiers ici',
                            textFormatter: function (text) {
                                return text
                            },

                            // Functionalities
                            enableReplying: true,
                            enableEditing: false,
                            enableUpvoting: false,
                            enableDeleting: true,
                            enableAttachments: false,
                            enableHashtags: true,
                            enablePinging: true,
                            enableDeletingCommentWithReplies: false,
                            // enable<a href="https://www.jqueryscript.net/tags.php?/Navigation/">Navigation</a>: true,
                            postCommentOnEnter: true,
                            forceResponsive: false,
                            readOnly: false,
                            defaultNavigationSortKey: 'newest',

                            // Colors
                            highlightColor: '#2793e6',
                            deleteButtonColor: '#C9302C',

                            scrollContainer: this.$el,
                            roundProfilePictures: false,
                            textareaRows: 2,
                            textareaRowsOnFocus: 2,
                            textareaMaxRows: 5,
                            maxRepliesVisible: 2,

                            fieldMappings: {
                                id: 'id',
                                parent: 'parent',
                                created: 'created',
                                modified: 'modified',
                                content: 'content',
                                file: 'file',
                                fileURL: 'file_url',
                                fileMimeType: 'file_mime_type',
                                pings: 'pings',
                                creator: 'creator',
                                fullname: 'fullname',
                                profileURL: 'profile_url',
                                profilePictureURL: 'profile_picture_url',
                                isNew: 'is_new',
                                createdByAdmin: 'created_by_admin',
                                createdByCurrentUser: 'created_by_current_user',
                                upvoteCount: 'upvote_count',
                                userHasUpvoted: 'user_has_upvoted'
                            },
                            searchUsers: function (term, success, error) {
                                setTimeout(function () {
                                    success(usersArray.filter(function (user) {
                                        var containsSearchTerm = user.fullname.toLowerCase().indexOf(term.toLowerCase()) != -1;
                                        // var isNotSelf = '';
                                        // if ($("#employerconnecter").text() === user.fullname ){
                                        //     isNotSelf = "Vous";
                                        // }else{
                                        //     isNotSelf = user.fullname
                                        // }

                                        var isNotSelf = user.id != 1;
                                        // console.log(user.fullname)
                                        return containsSearchTerm && isNotSelf;
                                    }));
                                }, 500);
                            },
                            // getUsers: function (success, error) {
                            //     // console.log('1');
                            //     setTimeout(function () {
                            //         success(usersArray);
                            //     }, 500);
                            // },
                            getComments: function (success, error) {
                                setTimeout(function () {
                                    success(commentsArray);
                                    // success(usersArray);
                                }, 500);
                            },
                            postComment: function (data, success, error) {
                                // console.log(data);
                                setTimeout(function () {
                                    success(saveComment(data));
                                }, 500);
                            },
                            putComment: function (data, success, error) {
                                // console.log('4');
                                setTimeout(function () {
                                    success(updateComment(data));
                                }, 500);
                            },
                            deleteComment: function (data, success, error) {
                                // console.log('5');
                                setTimeout(function () {
                                    success();
                                }, 500);
                            },
                            upvoteComment: function (data, success, error) {
                                setTimeout(function () {
                                    // success(data);
                                }, 500);
                            },
                            uploadAttachments: function (dataArray, success, error) {


                                console.log(dataArray);
                                setTimeout(function () {
                                    success(dataArray);
                                }, 500);
                            },

                        });


                    },
                    error: function (error) {

                    }
                });


            },
            error: function (error) {

                swal({
                    title: "Alert!",
                    icon: "error",
                    button: false,
                    text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                    timer: 1500
                });
            }
        });
        

    });
}

btndemandeblindtable();


function remplisDelaiEcheance() {
    //On recuperer la date
    var dateaffect = $("#datedebutaffecteworkflow");
    var dateecheance = $("#dateecheanceaffecteworkflow");
    //On le met dans une variable de type date
    // var hours = $("#delaiaffecteworkflow");
    var start = new Date(dateaffect.data("date"));
    var end = new Date(dateecheance.data("date"));
    if (start <= end) {
        var heures = (end - start) / (1000 * 60 * 60 * 24) * 24;
        $("#delaiaffecteworkflow").val(heures);
    } else {
        $("#delaiaffecteworkflow").val(0);
        $.notify('Veuillez vous assurer que la date début est antérieur a la date fin', 'error');
    }
}

// $("#delaiaffecteworkflow").on('change', function (e) {
//     e.preventDefault();
//     remplisDateEcheance();
// });

$("#datedebutaffecteworkflow").on('dp.change', function (e) {
    e.preventDefault();
    remplisDelaiEcheance();
});

$("#dateecheanceaffecteworkflow").on('dp.change', function (e) {
    e.preventDefault();
    remplisDelaiEcheance();
});


$(".show-workflow-demande").on('click', function (e) {
    e.preventDefault();
    var formPanel = $("#afficherdetailworkflow");
    var btnworkflow = $(this);
    if (detailshow === false) {
        // btnworkflow.replace('fa-eye','fa-eye-slash');
        // btnworkflow.button('loading');
        $(this).find('.fa-eye').removeClass('fa-eye').addClass('fa-eye-slash');
        formPanel.html(loader);
        formPanel.show('slow');
        var id = btnworkflow.attr('id');
        $.ajax({
            url: detailWorkflowUrl.replace('lecode', id),
            type: "POST",
            success: function (data) {
                btnworkflow.button('reset');
                formPanel.html(data);
            }
        });
        detailshow = true;
    } else {
        formPanel.html('');
        formPanel.hide('slow');
        // btnworkflow.replace('fa-eye-slash','fa-eye');
        btnworkflow.find('.fa-eye-slash').removeClass('fa-eye-slash').addClass('fa-eye');
        detailshow = false;
    }


    // workFlowListHidden = showBody($(this), workFlowListHidden, 'workflow')
});


$('#interrupteur').on('click', function () {

    var interrup = $(this);
    if (interrup.attr('data-value') === "on") {
        interrup.removeClass('switch-on');
        interrup.addClass('switch-off');
        interrup.attr('data-value', 'off');
        $('#procedureexceptionnelle').show(1500);
        $('#procedurenormal').hide(1500);


    } else {
        interrup.removeClass('switch-off');
        interrup.addClass('switch-on');
        interrup.attr('data-value', 'on');
        $('#procedureexceptionnelle').hide(1500);
        $('#procedurenormal').show(1500);


    }

});


//Ajoute des lignes ( nouvelle div )
$('#addligneagent').on('click', function () {
    var id = addAgent();
    if (id !== false) {
        var b = parseInt(id) + 1;
        if (b <= 5) {
            newDivAffectation(b);
            $("#idDiv").val(b);
            $("#addligneagent").val(b);

            $('.deleteLine').unbind('click');
            $('.deleteLine').on('click', function (e) {
                e.preventDefault();
                removeLine($(this));
            });

            $('.serviceaffectationdemande').unbind('change');
            $('.serviceaffectationdemande').on('change', function (e) {
                e.preventDefault();
                affectDemande($(this));
                ;
            });
        } else {
            $.notify('Limite d\'ajout des agents atteint', 'error')
        }
    }
    ;
});


//Verifie le contenu des champs avant ajout d'une ligne
function addAgent(edit) {
    if (typeof edit !== "undefined") {
        edit = '-edit'
    } else {
        edit = ''
    }
    var idligne = $("#addligneagent" + edit).val();
    var service = $("#serviceaffectationdemande" + edit + idligne);
    var agent = $("#agentaffectationdemande" + edit + idligne);
    var delai = $("#delaiaffectationdemande" + edit + idligne);
    // console.log(agent.val(),service.val(),delai.val())
    if (agent.val() === null) {
        agent.notify(
            "Veuillez selectionner l'agent",
            {position: "right"},
            "warn"
        );
        agent.focus();
        return false;
    } else {
        if (service.val() === null) {
            service.notify(
                "Veuillez selectionner le service",
                {position: "right"},
                "warn"
            );
            service.focus();
            return false;
        } else {
            if (delai.val() === '') {
                delai.notify(
                    "Veuillez saisir le delai",
                    {position: "right"},
                    "warn"
                );
                delai.focus();
                return false;
            } else {
                return idligne;
            }
            ;
        }
        ;
    }
    ;
};


function removeLine(nodeElement) {
    var tr = nodeElement.closest('.line');
    var id = tr.attr('id');
    var tab = [];
    allAgent.forEach(function (value) {
        if (value['id'] !== parseInt(id)) {
            tab.push(value)
        }
    });
    allDelete.push(id);
    allAgent = tab;
    var nbrligne = $("#addligneagent").val();
    $("#addligneagent").val(parseInt(nbrligne) - 1);
    tr.remove();
}

function affectDemande(element) {
    var idservice = element.val();
    var listeagentcombo = "<option value='0' disabled selected>...</option>";
    var lecombolistagent = element.closest('.line').find('.agentaffectationdemande');
    lecombolistagent.html(listeagentcombo);
    $.ajax({
        url: agentselonserviceListUrl.replace('lecode', idservice),
        type: "GET",
        success: function (data) {
            listeagentcombo = "<option value='0' disabled selected>L'agent</option>";
            data.forEach(function (g) {
                listeagentcombo += '<option value="' + g.id + '">' + g.agent + '</option>';
            });
            lecombolistagent.html(listeagentcombo);
            // var classdemo = element.closest('.line').find('.demo');

        },
        error: function (error) {
            lecombolistagent.html("<option value='0'>L'agent</option>");
            swal({
                title: "Alert!",
                icon: "error",
                button: false,
                text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                timer: 1500
            });
        }
    })
}

$('.serviceaffectationdemande').on('change', function () {
    affectDemande($(this));
});