//Action modal Demande
var modalAjoutDemande = $('#ModalAjouterDemande');
var ModalDropPieceDemande = $('#ModalDropPieceDemande');
var ModalAjoutProspect = $('#ModalAjoutProspect');
var ModalAjoutProspectMoral = $('#ModalAjoutProspectMoral');
var ModalProspect = $('#ModalProspect');
var loader = '<div class="loader text-center mt-3 mb-3"><i class="fa fa-spinner fa-spin fa-3x"></i></div>';
var leProspect = [];
var allAgent = [];
var allDelete = [];
var iddemande = '';
var prospectPhysiquePourRecherche = true;


////Gestion des nombre de mot du champs observations
$(document).ready(function(e) {

    $('#observationdemande').keyup(function() {

        var nombreCaractere = $(this).val().length;

        var nombreMots = jQuery.trim($(this).val()).split(' ').length;
        if($(this).val() === '') {
            nombreMots = 0;
        }

        var msg = 'Objet ' + nombreMots + 'mot(s)| ' + nombreCaractere + 'Caractere(s)/200';
        // var msg = 'Observation ' + nombreMots + ' mot(s) | ' + nombreCaractere + ' Caractere(s)/200';
        $('#compteur').text(msg);
        if (nombreCaractere > 150) { $('#compteur').addClass("mauvais"); } else { $('#compteur').removeClass("mauvais"); }

    })


});

var contentmodalimage = $('#contenudropzone').html();




//Action modal DropZone
// var modalCloseDemande = $('#closemodalworkflow');

$('#closemodalPiece').on('click', function (e) {
    e.preventDefault();
    var modalPiece = $('#ModalDropPieceDemande');
    modalPiece.modal('hide');
});

$('#rechargemodalPiece').on('click', function (e) {
    e.preventDefault();
    $('#my-dropzone').html(contentmodalimage);
    $('#dropfilelabel').addClass('default');
});

$("#btnopenrapideDemande").on('click', function (e) {
    e.preventDefault();
    modalAjoutDemande.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
    modalAjoutDemande.modal('show');
    $("#datereceptiondemande").datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        defaultDate: moment(),
        locale: 'fr'
    });
});

$("#btnopenmodaladdprospect").on('click', function (e) {
    e.preventDefault();

    // ModalAjoutProspect.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
    ModalAjoutProspect.modal('show');
});

$("#btnjoindrepiece").on('click', function (e) {
    e.preventDefault();
    ModalDropPieceDemande.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
    ModalDropPieceDemande.modal('show');
});

$("#closemodalDemande").on('click', function (e) {
    e.preventDefault();
    // alert('ok')
    modalAjoutDemande.removeClass('animated bounceInLeft').addClass('animated bounceOutDown');
    modalAjoutDemande.modal('hide');

});

$("#closemodalProspect").on('click', function (e) {
    e.preventDefault();
    ModalProspect.removeClass('animated rollIn').addClass('animated rollOut');
    ModalProspect.modal('hide');

});

$("#closemodaladdProspect").on('click', function (e) {
    e.preventDefault();
    ModalAjoutProspect.modal('hide');

});

$("#btnopenprospectrecherche").on('click', function (e) {
    e.preventDefault();
    ModalProspect.removeClass('animated rollOut').addClass('animated rollIn');
    ModalProspect.modal('show');
});

$("#btnsearchprospect").on('click', function (e) {
    e.preventDefault();
    var formPanel = $(this).closest('.modal-body').find('#listeprospect');
    // formPanel.hide('slow');


    var btnrecherche = $(this);

    // alert(prospectPhysiquePourRecherche);
    if (prospectPhysiquePourRecherche === true){
        var nom = $('#nomprospect');
        var prenom = $('#prenomprospect');
        if (nom.val() ==='' && prenom.val() ===''){
            $.notify('Veuillez saisir au moins le nom ou le prénom','error');
            nom.focus();
        }else{
            formPanel.html(loader);
            btnrecherche.button('loading');
            $.ajax({
                url: prospectListUrl,
                type: "GET",
                data: {
                    nom: nom.val(),
                    prenom: prenom.val(),
                },
                success: function (data) {
                    // console.log(data)
                    btnrecherche.button('reset');
                    if (data === null) {
                        $.notify(
                            "Aucun prospect trouvé",
                            {position: "right"},
                            "warn"
                        );
                    } else {
                        $('.footermodalprospect').show('slow');
                        $('.footermodalprospect').html('<div class="pull-left" >\n' +
                            '                            <button type="button" class="btn" id="closemodalProspect">\n' +
                            '                                Annuler\n' +
                            '                            </button>\n' +
                            '                        </div>\n' +
                            '                        <div class="pull-right">\n' +
                            '                            <button data-url="{{ path(\'ajax_demande_new\') }}"\n' +
                            '                                    class="btn btn-primary" id="btnopenmodaladdprospect"\n' +
                            '                                    type="submit"> <i class="fa fa-plus"></i> Nouveau\n' +
                            '                            </button>\n' +
                            '                        </div>')

                        $('#btnopenmodaladdprospect').on('click', function (e) {
                            e.preventDefault();
                            // ModalAjoutProspect.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
                            ModalAjoutProspect.modal('show');
                        });
                        $('#closemodalProspect').on('click', function (e) {
                            e.preventDefault();
                            // alert('ok')
                            ModalProspect.removeClass('animated rollIn').addClass('animated rollOut');
                            ModalProspect.modal('hide');

                        });

                    }

                    formPanel.html(data);
                    $('.btnselectionneProspect').on('click', function (e) {
                        e.preventDefault();
                        // alert('ok')
                        var idPublicProspect = $(this).attr('id');
                        var nomProspect = $(this).closest('.ligne').find('.nomProspect').text();
                        var prenomProspect = $(this).closest('.ligne').find('.prenomProspect').text();
                        var sexeProspect = $(this).closest('.ligne').find('.sexeProspect').text();
                        var contactProspect = $(this).closest('.ligne').find('.contactProspect').text();
                        var emailProspect = $(this).closest('.ligne').find('.emailProspect').text();

                        leProspect = {
                            'idPublic': idPublicProspect,
                            'nom': nomProspect,
                            'prenom': prenomProspect,
                            'sexe': sexeProspect,
                            // 'sigle': sigle.val(),
                            'raisonsociale': '',
                            'contact': contactProspect,
                            'email': emailProspect,
                        };

                        // console.log(leProspect)
                        $('#prospectdemande').val(nomProspect + ' ' + prenomProspect);
                        ModalProspect.modal('hide');
                        // $('#closemodaladdProspect').click();

                    });
                },
                error: function (error) {
                    btnrecherche.button('reset');
                    swal({
                        title: "Alert!",
                        icon: "error",
                        button: false,
                        text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                        timer: 1500
                    });
                    formPanel.html('');
                    btnrecherche.button('reset');
                }
            }).always(function () {
                btnrecherche.button('reset')
            });
        }
    }else{
        var raisonsociale = $('#raisonsociale');
        if (raisonsociale.val() ===''){
            $.notify('Veuillez saisir au moins la raison sociale','error');
            raisonsociale.focus();
        }else{
            formPanel.html(loader);
            btnrecherche.button('loading');
            $.ajax({
                url: prospectMoralListUrl,
                type: "GET",
                data: {
                    raisonSociale: raisonsociale.val(),
                },
                success: function (data) {
                    // console.log(data)
                    btnrecherche.button('reset');
                    if (data === null) {
                        $.notify(
                            "Aucun prospect trouvé",
                            {position: "right"},
                            "warn"
                        );
                    } else {
                        $('.footermodalprospect').show('slow');
                        $('.footermodalprospect').html('<div class="pull-left" >\n' +
                            '                            <button type="button" class="btn" id="closemodalProspect">\n' +
                            '                                Annuler\n' +
                            '                            </button>\n' +
                            '                        </div>\n' +
                            '                        <div class="pull-right">\n' +
                            '                            <button data-url="{{ path(\'ajax_demande_new\') }}"\n' +
                            '                                    class="btn btn-primary" id="btnopenmodaladdprospect"\n' +
                            '                                    type="submit"> <i class="fa fa-plus"></i> Nouveau\n' +
                            '                            </button>\n' +
                            '                        </div>')

                        $('#btnopenmodaladdprospect').on('click', function (e) {
                            e.preventDefault();
                            // ModalAjoutProspect.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
                            ModalAjoutProspectMoral.modal('show');
                        });
                        $('#closemodalProspect').on('click', function (e) {
                            e.preventDefault();
                            // alert('ok')
                            ModalProspect.removeClass('animated rollIn').addClass('animated rollOut');
                            ModalProspect.modal('hide');

                        });

                    }

                    formPanel.html(data);
                    $('.btnselectionneProspect').on('click', function (e) {
                        e.preventDefault();
                        // alert('ok')
                        var idPublicProspect = $(this).attr('id');
                        var raisonProspect = $(this).closest('.ligne').find('.raisonSociale').text();
                        // var prenomProspect = $(this).closest('.ligne').find('.prenomProspect').text();
                        // var sexeProspect = $(this).closest('.ligne').find('.sexeProspect').text();
                        var contactProspect = $(this).closest('.ligne').find('.contactProspect').text();
                        var emailProspect = $(this).closest('.ligne').find('.emailProspect').text();

                        // leProspect = {
                        //     'idPublic': idPublicProspect,
                        //     'nom': nomProspect,
                        //     'prenom': prenomProspect,
                        //     'sexe': sexeProspect,
                        //     'contact': contactProspect,
                        //     'email': emailProspect
                        // };

                        leProspect = {
                            'idPublic': idPublicProspect,
                            'nom': '',
                            'prenom': '',
                            'sexe': '',
                            // 'sigle': sigle.val(),
                            'raisonsociale': raisonProspect,
                            'contact': contactProspect,
                            'email': emailProspect,
                        };
                        // console.log(leProspect)
                        $('#prospectdemande').val(raisonProspect);
                        ModalProspect.modal('hide');
                        // $('#closemodaladdProspect').click();

                    });
                },
                error: function (error) {
                    btnrecherche.button('reset');
                    swal({
                        title: "Alert!",
                        icon: "error",
                        button: false,
                        text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                        timer: 1500
                    });
                    formPanel.html('');
                    btnrecherche.button('reset');
                }


            }).always(function () {
                btnrecherche.button('reset')
            });
        }
    }


    // ModalProspect.modal('show');
});

$("#btnadddemande").on('click', function (e) {
    e.preventDefault();
    var reference = $("#referencedemande");
    var idsociete = $("#societedemande");
    var idtypedemande = $("#typedemandedemande");
    var datereception = $("#datereceptiondemande");
    var prospect = $("#prospectdemande");
    var source = $("#sourcedemande");
    var observation = $("#observationdemande");
    var priorite = $("#prioritedemande");

    if (reference.val().trim() === '') {
        reference.notify(
            "Veuillez saisir la reference de la demande",
            {position: "right"},
            "warn"
        );
        reference.focus();
    } else {
        if (datereception.data("date") === '') {
            datereception.notify(
                "Veuillez selectionner une date et saisir une heure",
                {position: "right"},
                "warn"
            );
            datereception.focus();
        } else {
            if (idtypedemande.val() === null) {
                idtypedemande.notify(
                    "Veuillez selectionner le type de demande",
                    {position: "right"},
                    "warn"
                );
                idtypedemande.focus();
            } else {
                if (prospect.val() === null) {
                    prospect.notify(
                        "Veuillez selectionner le prospect",
                        {position: "right"},
                        "warn"
                    );
                    prospect.focus();
                } else {
                    if (source.val() === null) {
                        source.notify(
                            "Veuillez selectionner la source",
                            {position: "right"},
                            "warn"
                        );
                        source.focus();
                    } else {
                        if (priorite.val() === null) {
                            priorite.notify(
                                "Veuillez selectionner la priorite",
                                {position: "right"},
                                "warn"
                            );
                            priorite.focus();
                        } else {
                            if (idsociete.val() === null) {
                                idsociete.notify(
                                    "Veuillez selectionner la societe",
                                    {position: "right"},
                                    "warn"
                                );
                                idsociete.focus();
                            } else {
                                var btn = $(this);
                                var tab = {
                                    'reference': reference.val(),
                                    'idsociete': idsociete.val(),
                                    'idtypedemande': idtypedemande.val(),
                                    'datereception': datereception.data("date"),
                                    // 'prospect': prospect.val(),
                                    'idsource': source.val(),
                                    'observation': observation.val(),
                                    'idpriorite': priorite.val(),
                                };
                                // console.log(leProspect)
                                btn.button('loading');
                                $.ajax({
                                    url: demandeAddUrl,
                                    type: "POST",
                                    data: {
                                        'tab': tab,
                                        'prospect': leProspect,
                                    },
                                    success: function (data) {
                                        btn.button('reset');
                                        swal({
                                            title: "Alert!",
                                            icon: "success",
                                            button: false,
                                            text: "Enregistrement effectué avec succes",
                                            timer: 1500
                                        });
                                        // console.log(data);
                                        modalAjoutDemande.modal('hide');
                                        $('#ModalAjouterDemande').find('input').val('');
                                        $('#ModalAjouterDemande').find('select').val(0);
                                        $('#ModalAjouterDemande').find('#typedemandedemande').html('');
                                        $('#my-dropzone').html(contentmodalimage);

                                        var CheminComplet = document.location.href;
                                        // var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
                                        var NomDuFichier = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 );
                                        if (NomDuFichier === 'demande'){
                                            table.row.add([
                                                data.reference,
                                                data.dateReception + " " + data.heureReception,
                                                data.societe,
                                                data.typeDemande,
                                                data.prospect,
                                                data.transitionEnCours,
                                                "<td class=\"col-md-2\">\n" +
                                                "\n" +
                                                "                                    <a type=\"button\"\n" +
                                                "                                                class=\"btn btn-theme affect-demande\"\n" +
                                                "                                       data-loading-text=\"<i class='fa fa-refresh fa-spin'></i>\"\n" +
                                                "                                       id="+data.id+"\n" +
                                                "                                    >\n" +
                                                "                                        <i class=\"fa fa-forward\"></i></a>\n" +
                                                "                                    <a type=\"button\"\n" +
                                                "                                       class=\"btn btn-success affect-detail\"\n" +
                                                "                                       data-loading-text=\"<i class='fa fa-refresh fa-spin'></i>\"\n" +
                                                "                                       id="+data.id+"\n" +
                                                "                                    >\n" +
                                                "                                        <i class=\"fa fa-book\"></i></a>\n" +
                                                "                                    <a type=\"button\"\n" +
                                                "                                                class=\"btn btn-danger delete-demande\"\n" +
                                                "                                       data-loading-text=\"<i class='fa fa-refresh fa-spin'></i>\"\n" +
                                                "                                       id="+data.id+"\n" +
                                                "                                    >\n" +
                                                "                                        <i class=\"fa fa-trash\"></i></a>\n" +
                                                "                                </td>"
                                            ]).draw();
                                            btndemandeblindtable();
                                        }

                                    },
                                    error: function (error) {
                                        btn.button('reset');
                                        swal({
                                            title: "Alert!",
                                            icon: "error",
                                            button: false,
                                            text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                                            timer: 1500
                                        });
                                    }
                                }
                                ).always(function () {
                                    btn.button('reset')
                                });

                            }


                        }
                    }
                }


            }
        }
    }
});

$("#btnenregistrerprospect").on('click', function (e) {
    e.preventDefault();
    var nom = $("#nomprospectenreg");
    var prenom = $("#prenomprospectenreg");
    var telephone = $("#customer_phone");
    var sexe = $("#sexeprospectenreg");
    var mail = $("#emailprospectenreg");

    if (nom.val().trim() === '') {
        nom.notify(
            "Veuillez saisir le nom",
            {position: "right"},
            "warn"
        );
        nom.focus();
    } else {
        if (prenom.val().trim() === '') {
            prenom.notify(
                "Veuillez saisir le prenom",
                {position: "right"},
                "warn"
            );
            prenom.focus();
        } else {
            if (telephone.val().trim() === '') {
                telephone.notify(
                    "Veuillez saisir le telephone",
                    {position: "right"},
                    "warn"
                );
                telephone.focus();
            } else {
                if (sexe.val() === null) {
                    sexe.notify(
                        "Veuillez selectionner le sexe",
                        {position: "right"},
                        "warn"
                    );
                    sexe.focus();
                } else {
                    var btn = $(this);
                    var tab = {
                        'nom': nom.val(),
                        'prenom': prenom.val(),
                        'sexe': sexe.val(),
                        'telephone': telephone.val(),
                        'email': mail.val(),
                    };
                    btn.button('loading');
                    $.ajax({
                        url: prospectAddUrl,
                        type: "POST",
                        data: {
                            'prospect': tab,
                        },
                        success: function (data) {
                            btn.button('reset');
                            // $.notify("Enregistrement effectué avec succes !", "success");
                            swal({
                                title: "Alert!",
                                icon: "success",
                                button: false,
                                text: "Enregistrement effectué avec succes",
                                timer: 1500
                            });


                            // console.log(data)
                            // leProspect = {
                            //     'idPublic': data,
                            //     'nom': nom.val(),
                            //     'prenom': prenom.val(),
                            //     'sexe': sexe.val(),
                            //     'contact': telephone.val(),
                            //     'email': mail.val()
                            // };

                            leProspect = {
                                'idPublic': JSON.parse(data).code,
                                'nom': nom.val(),
                                'prenom': prenom.val(),
                                'sexe': sexe.val(),
                                // 'sigle': sigle.val(),
                                'raisonsociale': '',
                                'contact': telephone.val(),
                                'email': mail.val(),
                            };

                            // console.log(nom.val() + ' ' + prenom.val());
                            $('#prospectdemande').val(nom.val() + ' ' + prenom.val());
                            ModalAjoutProspect.modal('hide');
                            ModalProspect.modal('hide');
                            ModalAjoutProspect.find('input').val('');
                        },
                        error: function (data) {
                            btn.button('reset');
                            // $.notify("Enregistrement effectué avec succes !", "success");
                            swal({
                                title: "Alert!",
                                icon: "error",
                                button: false,
                                text: "Echec enregistrement",
                                timer: 1500
                            });
                            // modalAjoutDemande.modal('hide');
                        }
                    }
                    ).always(function () {
                        btn.button('reset')
                    });
                }


            }
        }
    }

});



$('#btnenregistrerprospectmoral').on('click', function (e) {
    e.preventDefault();
    var sigle = $('#cigleprospectenreg');
    var raisonsociale = $('#raisonsocialeprospectenreg');
    var telephone = $('#customerphoneprospectmoralenreg');
    // var sexe = $("#sexeprospectenreg");
    var mail = $('#emailprospectmoralenreg');

    if (sigle.val().trim() === '') {
        sigle.notify(
            "Veuillez saisir le cigle",
            {position: "right"},
            "warn"
        );
        sigle.focus();
    } else {
        if (raisonsociale.val().trim() === '') {
            raisonsociale.notify(
                "Veuillez saisir la raison sociale",
                {position: "right"},
                "warn"
            );
            raisonsociale.focus();
        } else {
            if (telephone.val().trim() === '') {
                telephone.notify(
                    "Veuillez saisir le telephone",
                    {position: "right"},
                    "warn"
                );
                telephone.focus();
            } else {
                // if (sexe.val() === null) {
                //     sexe.notify(
                //         "Veuillez selectionner le sexe",
                //         {position: "right"},
                //         "warn"
                //     );
                //     sexe.focus();
                // } else {
                    var btn = $(this);
                    var tab = {
                        'sigle': sigle.val(),
                        'raisonsociale': raisonsociale.val(),
                        // 'sexe': sexe.val(),
                        'telephone': telephone.val(),
                        'email': mail.val(),
                    };
                    btn.button('loading');
                    $.ajax({
                        url: prospectMoralAddUrl,
                        type: "POST",
                        data: {
                            'prospectmoral': tab,
                        },
                        success: function (data) {
                            btn.button('reset');
                            // $.notify("Enregistrement effectué avec succes !", "success");

                            // var icon = "succes";
                            // if (JSON.parse(data).code === "0"){
                            //     icon = "warning";
                            // }

                            swal({
                                title: "Alert!",
                                icon: "success",
                                button: false,
                                text: "Enregistrement effectué avec succes",
                                timer: 1500
                            });


                            // console.log()
                            leProspect = {
                                'idPublic': JSON.parse(data).code,
                                'nom': '',
                                'prenom': '',
                                'sexe': '',
                                // 'sigle': sigle.val(),
                                'raisonsociale': raisonsociale.val(),
                                'contact': telephone.val(),
                                'email': mail.val(),
                            };

                            $('#prospectdemande').val(raisonsociale.val());
                            ModalAjoutProspectMoral.modal('hide');
                            ModalProspect.modal('hide');
                            ModalAjoutProspectMoral.find('input').val('');
                        },
                        error: function (data) {
                            btn.button('reset');
                            // $.notify("Enregistrement effectué avec succes !", "success");
                            swal({
                                title: "Alert!",
                                icon: "error",
                                button: false,
                                text: "Echec enregistrement",
                                timer: 1500
                            });
                            // modalAjoutDemande.modal('hide');
                        }
                    }
                    ).always(function () {
                        btn.button('reset')
                    });
                // }


            }
        }
    }

});



$("#prospect-physique").find('label').on('click', function (e) {
    e.preventDefault();
    $(this).css('color','blue');

    $('#prospect-recherche').html('<div class="form-group col-md-10">\n' +
        '                            <label>Raison Sociale</label>\n' +
        '                            <input class="form-control" id="raisonsociale" name="raisonsociale"\n' +
        '                                   type="text"\n' +
        '                                   placeholder="">\n' +
        '                        </div>\n' +
        '                        </div>').fadeIn(3000);
    $("#prospect-moral").find('label').css('color','black');
    // $("#nomprospect").closest('.form-group').find('label').text('Cigle').fadeIn();
    // $("#prenomprospect").closest('.form-group').find('label').text('Raison sociale').fadeIn();
    prospectPhysiquePourRecherche = false;
});

$("#prospect-moral").find('label').on('click', function (e) {
    e.preventDefault();
    // alert('o')
    $(this).css('color','blue');
    $('#prospect-recherche').html('<div class="form-group col-md-5">\n' +
        '                            <label>Nom</label>\n' +
        '                            <input class="form-control" id="nomprospect" name="nomprospect"\n' +
        '                                   type="text"\n' +
        '                                   placeholder="">\n' +
        '                        </div>\n' +
        '                        <div class="form-group col-md-5">\n' +
        '                            <label>Prénoms</label>\n' +
        '                            <input class="form-control" id="prenomprospect" name="prenomprospect"\n' +
        '                                   type="text"\n' +
        '                                   placeholder="">\n' +
        '                        </div>\n' +
        '                        </div>').fadeIn(3000);
    $("#prospect-physique").find('label').css('color','black');

    prospectPhysiquePourRecherche = true;
    // $("#nomprospect").closest('.form-group').find('label').text('Nom').fadeIn();
    // $("#prenomprospect").closest('.form-group').find('label').text('Prénom').fadeIn();
});



// function btndemandeblindtable() {
//     $('#tablelistedemande').on('click', '.affect-demande', function (e) {
//         e.preventDefault();
//
//         // console.log('ok1')
//         boutonselectionner = $(this);
//         iddemande = $(this).attr('id');
//         boutonselectionner.button('loading');
//         $.ajax({
//             url: demandedetailworkflowListUrl.replace('lecode', iddemande),
//             type: "GET",
//             success: function (data) {
//                 boutonselectionner.button('reset');
//                 data.forEach(function (g) {
//                     $("#affecteworkflow").attr('data-id', g.id);
//                     $("#affecteworkflow").val(g.libelleworkflow);
//                     $("#delaiaffecteworkflow").val(g.delai);
//                 });
//                 var lelienaffichage = $(".show-workflow-demande").attr('data-href');
//                 $(".show-workflow-demande").attr('id', $("#affecteworkflow").attr('data-id'));
//                 $(".show-workflow-demande").attr('data-href', lelienaffichage.replace('lecode', $("#affecteworkflow").attr('data-id')));
//                 // console.log($(".show-workflow-demande").attr('id'),$(".show-workflow-demande").attr('data-href'))
//                 // ModalAffectDemande.removeClass('animated bounceOutDown').addClass('animated bounceInLeft');
//                 ModalAffectDemande.modal('show');
//                 //On recuperer la date
//                 var dateaffect = $("#datedebutaffecteworkflow");
//                 //On le met dans une variable de type date
//                 var hours = $("#delaiaffecteworkflow");
//                 var result = new Date(dateaffect.data("date"));
//                 var momentdate = moment(result, 'YYYY-MM-DD HH:mm').add(hours.val(), 'hours');
//                 $("#dateecheanceaffecteworkflow").datetimepicker({
//                     format: 'YYYY-MM-DD HH:mm',
//                     defaultDate: momentdate,
//                     locale: 'fr'
//                 });
//             },
//             error: function (error) {
//                 boutonselectionner.button('reset');
//                 swal({
//                     title: "Alert!",
//                     icon: "error",
//                     button: false,
//                     text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
//                     timer: 1500
//                 });
//             }
//         });
//
//
//     });
//
//     $('#tablelistedemande').on('click', '.delete-demande' , function (e) {
//         e.preventDefault();
//         iddemande = $(this).attr('id');
//         var btn = $(this);
//         swal({
//             title: "Confirmation !",
//             text: "Voulez-vous vraiment supprimer cet enregistrement!",
//             icon: "warning",
//             buttons: true,
//             dangerMode: true,
//         }).then(function (isConfirm) {
//             if (isConfirm) {
//                 btn.button('loading');
//                 $.ajax({
//                     url: demandeDeleteUrl.replace('lecode', iddemande),
//                     type: "GET",
//                     success: function (data) {
//
//                         btn.button('reset');
//                         // swal({
//                         //     title: "Alert!",
//                         //     icon: "success",
//                         //     button: false,
//                         //     text: "Suppression effectué avec succes",
//                         //     timer: 1500
//                         // });
//                         btn.closest('td').closest('tr').fadeOut(2000, function () {
//                             btn.closest('td').closest('tr').remove();
//                         });
//                     },
//                     error: function (error) {
//                         btn.button('reset');
//                         swal({
//                             title: "Alert!",
//                             icon: "error",
//                             button: false,
//                             text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
//                             timer: 1500
//                         });
//                     }
//                 });
//             }
//         });
//
//
//     });
//
// }



//Selection de combo
$("#societedemande").on('change', function (e) {
    e.preventDefault();
    var sous = "<option value='' disabled selected>Chargement...</option>";
    $("#typedemandedemande").html(sous);
    $.ajax({
        url: typedemandeListUrl.replace('lecode', $(this).val()),
        type: "GET",
        success: function (data) {
            // console.log(data);

            sous = "<option value='' disabled selected>Selectionnez le type de demande</option>";
            data.forEach(function (g) {
                var dureejour = 0;
                if (g.unitetemp === 'mois') {
                    // console.log(line.find('.delaietape').val()*720);
                    dureejour += parseInt(g.delai * 720);
                }
                if (g.unitetemp === 'jour') {
                    // console.log(line.find('.delaietape').val()*24);
                    dureejour += parseInt(g.delai * 24);
                }
                if (g.unitetemp === 'heure') {
                    // console.log(line.find('.delaietape').val());
                    dureejour += parseInt(g.delai);
                }

                sous += '<option data-duree="' + dureejour + '" value="' + g.id + '">' + g.libelletypedemande + '</option>'
            });

            $("#typedemandedemande").html(sous);

        }
    });
});


// function removeLine(nodeElement) {
//     var tr = nodeElement.closest('.line');
//     var id = tr.attr('id');
//     var tab = [];
//     allAgent.forEach(function (value) {
//         if (value['id'] !== parseInt(id)) {
//             tab.push(value)
//         }
//     });
//     allDelete.push(id);
//     allAgent = tab;
//     var nbrligne = $("#addligneagent").val();
//     $("#addligneagent").val(parseInt(nbrligne) - 1);
//     tr.remove();
// }


// $('.serviceaffectationdemande').on('change', function () {
//     affectDemande($(this));
// });








/* initialize the calendar
     -----------------------------------------------------------------*/
//Date for the calendar events (dummy data)
var date = new Date()
var d    = date.getDate(),
    m    = date.getMonth(),
    y    = date.getFullYear()
$('#calendar').fullCalendar({
    header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
    },
    buttonText: {
        today: 'today',
        month: 'month',
        week : 'week',
        day  : 'day'
    },
    //Random default events
    events    : [
        {
            title          : 'All Day Event',
            start          : new Date(y, m, 1),
            backgroundColor: '#f56954', //red
            borderColor    : '#f56954' //red
        },
        {
            title          : 'Long Event',
            start          : new Date(y, m, d - 5),
            end            : new Date(y, m, d - 2),
            backgroundColor: '#f39c12', //yellow
            borderColor    : '#f39c12' //yellow
        },
        {
            title          : 'Meeting',
            start          : new Date(y, m, d, 10, 30),
            allDay         : false,
            backgroundColor: '#0073b7', //Blue
            borderColor    : '#0073b7' //Blue
        },
        {
            title          : 'Lunch',
            start          : new Date(y, m, d, 12, 0),
            end            : new Date(y, m, d, 14, 0),
            allDay         : false,
            backgroundColor: '#00c0ef', //Info (aqua)
            borderColor    : '#00c0ef' //Info (aqua)
        },
        {
            title          : 'Birthday Party',
            start          : new Date(y, m, d + 1, 19, 0),
            end            : new Date(y, m, d + 1, 22, 30),
            allDay         : false,
            backgroundColor: '#00a65a', //Success (green)
            borderColor    : '#00a65a' //Success (green)
        },
        {
            title          : 'Click for Google',
            start          : new Date(y, m, 28),
            end            : new Date(y, m, 29),
            url            : 'http://google.com/',
            backgroundColor: '#3c8dbc', //Primary (light-blue)
            borderColor    : '#3c8dbc' //Primary (light-blue)
        }
    ],
    editable  : true,
    droppable : true, // this allows things to be dropped onto the calendar !!!
    drop      : function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            $(this).remove()
        }

    }
})