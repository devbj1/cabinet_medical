var loader = '<div class="loader text-center mt-3 mb-3"><i class="fa fa-spinner fa-spin fa-3x"></i></div>';
var load = '<i class="fa fa-spinner fa-spin fa-2x"></i>';
var modalSearchPatient = $("#modal-consultation");
var modalEnregistreMotif = $("#modal-motif");
var modalEnregistreExamenPara = $("#modal-examen-para");
var modalEnregistreExamenCli = $("#modal-examen-cli");
var modalTraitement = $("#modal-medicament");
var modalDebutConsultation = $("#modal-begin-consul");
var formPanel = $("#menu-consultation");
var formPanel1 = $("#detail-patient");
var idTicket = "";
var idConsultationADuplique = "";


//Datatable

var table = $("#tablelisteconsultation").DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});

var table1 = $("#tablelistedebutconsultation").DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    },
    columnDefs: [
        {
            targets: 0,
            className: 'dt-body-center'
        },
        {
            targets: 1,
            className: 'dt-body-center'
        },
        {
            targets: 3,
            className: 'dt-body-center'
        },
        {
            targets: 4,
            className: 'dt-body-center'
        },
        {
            targets: 5,
            className: 'dt-body-center'
        },
        {
            targets: 6,
            className: 'dt-body-center'
        }
    ]


});

/*$("#dateconsultation").datetimepicker({
    format: 'dd/mm/yyyy HH:mm',
    defaultDate: new Date(),
    locale: 'fr',
    todayHighlight:'TRUE',
    autoclose: true,
});*/

$("#consulter").on('click', function (e) {
    e.preventDefault();

    formPanel.hide(2000);
    formPanel1.hide(3000);


});


function recherchePatient() {
    var loaderPanel = $("#loaderecherchepatient");
    var listePatient = $("#listepatient");

    var nom = $('#nomrecherche');
    var prenom = $('#prenomrecherche');
    if (nom.val() === "" && prenom.val() === "") {
        loaderPanel.html("");

    } else {
        loaderPanel.html(loader);
        $.ajax({
            url: listPatientUrl,
            type: "POST",
            data: {
                nom: nom.val(),
                prenom: prenom.val(),
            },
            success: function (data) {
                loaderPanel.html("");
                listePatient.html(data).fadeIn('slow');
                /*$(".select-patient").on('click', function (e) {
                    e.preventDefault();
                    modalSearchPatient.modal('hide');
                    idPatient = $(this).attr('data-idpatient');
                    $.ajax({
                        url: infoPatientUrl,
                        type: "POST",
                        data: {
                            id: idPatient,
                        },
                        success: function (data) {
                            $("#nom").val(data['nom']);
                            $("#prenom").val(data['prenom']);
                            $("#tel").val(data['tel']);
                            $("#age").val(data['age']);
                            $("#sexe").val(data['sexe']);
                            $("#situafamili").val(data['situaFamili']);
                            $("#adress").val(data['adresse']);
                            $("#medetrait").val(data['medTrait']);
                            $("#antmedi").val(data['antPerMedic']);
                            $("#antfamili").val(data['antFamili']);
                            $("#antchir").val(data['antPercChir']);
                            $("#autres").val(data['autres']);

                            $("#conduiteatenir").removeAttr('disabled');
                            $("#checkboxordonnance").removeAttr('disabled');
                            $("#checkboxemail").removeAttr('disabled');
                            $("#btnenregistrer").removeAttr('disabled');

                            formPanel.show(2000);
                            formPanel1.show(3000);
                            $('#menu-consultation').removeAttr('hidden');
                        },
                        error: function (error) {
                        }
                    });
                })*/
                $(".select-patient").on('click', function (e) {
                    e.preventDefault();
                    var ligne = $(this);
                    idPatient = ligne.attr('data-idpatient');
                    var nom = ligne.find('.nom').html();
                    var prenom = ligne.find('.prenom').html();
                    var tel = ligne.find('.tel').html();
                    $('#nompatientselection').attr('data-id-patient', idPatient);
                    $('#nompatientselection').val(nom);
                    $('#prenompatientselection').val(prenom);
                    $('#telpatientselection').val(tel);
                    listePatient.html('');
                    /*$.ajax({
                        url: infoPatientUrl,
                        type: "POST",
                        data: {
                            id: idPatient,
                        },
                        success: function (data) {
                            $("#nom").val(data['nom']);
                            $("#prenom").val(data['prenom']);
                            $("#tel").val(data['tel']);
                            $("#age").val(data['age']);
                            $("#sexe").val(data['sexe']);
                            $("#situafamili").val(data['situaFamili']);
                            $("#adress").val(data['adresse']);
                            $("#medetrait").val(data['medTrait']);
                            $("#antmedi").val(data['antPerMedic']);
                            $("#antfamili").val(data['antFamili']);
                            $("#antchir").val(data['antPercChir']);
                            $("#autres").val(data['autres']);

                            $("#conduiteatenir").removeAttr('disabled');
                            $("#checkboxordonnance").removeAttr('disabled');
                            $("#checkboxemail").removeAttr('disabled');
                            $("#btnenregistrer").removeAttr('disabled');

                            formPanel.show(2000);
                            formPanel1.show(3000);
                            $('#menu-consultation').removeAttr('hidden');
                        },enregistrernewpatient
                        error: function (error) {
                        }
                    });
                */

                })

            },
            error: function (error) {
                loaderPanel.html("");
            }
        });


    }
}


function rechercheTicket() {
    var leticket = $('#ticket');
    var icon = $('#loadicon');
    if (leticket.val() === "") {
        formPanel.hide(2000);
        formPanel1.hide(3000);

    } else {


        icon.html(load);
        $.ajax({
            url: controleTicketUrl,
            type: "POST",
            data: {
                leticket: leticket.val(),
            },
            success: function (data) {

                if (data['success'] === true) {
                    $.ajax({
                        url: infoPatientUrl,
                        type: "POST",
                        data: {
                            id: leticket.val(),
                        },
                        success: function (data) {
                            idTicket = leticket.val();
                            $("#nom").val(data['nom']);
                            $("#prenom").val(data['prenom']);
                            $("#tel").val(data['tel']);
                            $("#age").val(data['age']);
                            $("#sexe").val(data['sexe']);
                            $("#situafamili").val(data['situaFamili']);
                            $("#adress").val(data['adresse']);
                            $("#medetrait").val(data['medTrait']);
                            $("#antmedipat").val(data['antPerMedic']);
                            $("#antfamilipat").val(data['antFamili']);
                            $("#antchirpat").val(data['antPercChir']);
                            $("#autrespat").val(data['autres']);
                            $("#poidsconsul").val(data['poids']);
                            $("#tempconsul").val(data['temp']);
                            $("#tensconsul").val(data['tens']);
                            $("#tailconsul").val(data['tail']);
                            $("#observconsul").val(data['observation']);

                            $("#conduiteatenir").removeAttr('disabled');
                            $("#checkboxordonnance").removeAttr('disabled');
                            $("#checkboxemail").removeAttr('disabled');
                            $("#btnenregistrer").removeAttr('disabled');
                            // $("#debuter-consultation").removeClass('hidden');
                            formPanel1.show(3000);
                            icon.fadeIn(3000, function () {
                                icon.html('<i class="fa fa-check-square-o fa-2x"></i>');
                            });
                            // bundleDebutBouton();
                            //formPanel1.removeClass('hidden');
                        },
                        error: function (error) {
                            idTicket = "";
                            $("#nom").val("");
                            $("#prenom").val("");
                            $("#tel").val("");
                            $("#age").val("");
                            $("#sexe").val("");
                            $("#situafamili").val("");
                            $("#adress").val("");
                            $("#medetrait").val("");
                            $("#antmedipat").val("");
                            $("#antfamilipat").val("");
                            $("#antchirpat").val("");
                            $("#autrespat").val("");
                            $("#poidsconsul").val("");
                            $("#tempconsul").val("");
                            $("#tensconsul").val("");
                            $("#observconsul").val("");

                            $("#conduiteatenir").val("");
                            $("#conduiteatenir").attr('disabled');
                            //$("#checkboxordonnance").removeAttr('disabled');
                            //$("#checkboxemail").removeAttr('disabled');
                            $("#btnenregistrer").attr('disabled');
                            // $("#debuter-consultation").removeClass('hidden');

                            icon.fadeIn(3000, function () {
                                icon.html('<i class="fa fa-warning fa-2x"></i>');
                            });
                            formPanel1.hide(3000);
                        }
                    });

                } else {

                    idTicket = "";
                    $("#nom").val("");
                    $("#prenom").val("");
                    $("#tel").val("");
                    $("#age").val("");
                    $("#sexe").val("");
                    $("#situafamili").val("");
                    $("#adress").val("");
                    $("#medetrait").val("");
                    $("#antmedipat").val("");
                    $("#antfamilipat").val("");
                    $("#antchirpat").val("");
                    $("#autrespat").val("");
                    $("#poidsconsul").val("");
                    $("#tempconsul").val("");
                    $("#tensconsul").val("");
                    $("#observconsul").val("");

                    $("#conduiteatenir").val("");
                    $("#conduiteatenir").attr('disabled');
                    //$("#checkboxordonnance").removeAttr('disabled');
                    //$("#checkboxemail").removeAttr('disabled');
                    $("#btnenregistrer").attr('disabled');

                    icon.fadeIn(3000, function () {
                        icon.html('<i class="fa fa-warning fa-2x"></i>');
                    });
                }
            },
            error: function (error) {
            }
        }).always(function () {

        });


    }

}


function bundleDebutBouton() {
    $("#debuter-consultation").on('click', function (e) {
        e.preventDefault();
        var id = $('#ticket').val();
        var btn = $("#debuter-consultation");
        btn.button('loading');
        $.ajax({
            context: this,
            url: debutConsultationUrl,
            type: "POST",
            data: {
                leticket: id,
            },
            success: function (data) {
                if (data['success'] === true) {

                    formPanel.show(2000);
                    $('#ticket').attr('disabled');
                    $(this).hide('slow');
                    idTicket = id;
                } else {
                    formPanel.hide(2000);
                    $('#ticket').removeAttr('disabled');
                    $(this).show('slow');
                }
            },
            error: function (error) {

            }
        }).always(function () {
            btn.button('reset');
            btn.hide('slow');

        });
    });


}

$('#ticket').keyup(function (e) {
    if (e.keyCode === 13) {
        rechercheTicket();
    }
});

$('#nomrecherche').keyup(function () {
    recherchePatient();
});


$('#prenomrecherche').keyup(function () {
    recherchePatient();
});


$('#monElement').click(function (event) {
    event.stopPropagation();
});

/*$('#ajoutmotif').on('click', function (e) {
    e.preventDefault();
    Swal.fire({
        title: 'Ajouter un nouveau motif',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Save',
        showLoaderOnConfirm: true,
        preConfirm: (login) => {
            return fetch(`//api.github.com/users/${login}`)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    return response.json()
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            //Swal.fire({
                //title: `${result.value.login}'s avatar`,
                //imageUrl: result.value.avatar_url
            //})
        }
    })

})*/


$('#ajoutligneexammedical').on("click", function (e) {
    e.preventDefault();

    var idexam = $("#comboexamenclinique option:selected").val();
    var libelleexam = $("#comboexamenclinique option:selected").text();
    var resultexam = $("#examresultcli").val();

    /* $("#comboexamenclinique").notify(
         "Veuillez selectionner l'examen clinique",
         {position: "right"},
         "warn"
     );*/
    if (idexam === "0") {
        $.notify("Veuillez selectionner l'examen clinique", 'warn');
        $("#comboexamenclinique").focus();

    } else {
        //Verifie si l'examen est déjà ajouter
        var trouve = false;
        $("#listeexamenclinique").find('tr').each(function () {
            if ($(this).find('.exam-code').text() === libelleexam) {
                trouve = true;
            }
            ;
        });

        if (trouve === false) {
            $("#listeexamenclinique").append("<tr>\n" +
                "                                        <td class=\"exam-code\" id-exam=" + idexam + ">" + libelleexam + "</td>\n" +
                "                                        <td class=\"exam-result\">" + resultexam + "</td>\n" +
                "                                        <td><span class=\"remove-ligne\">\n" +
                "                                            <button type=\"button\" class=\"btn btn-danger\"><i\n" +
                "                                                        class=\"fa fa-trash\"></i></button>\n" +
                "                                        </span></td>\n" +
                "                                    </tr>");

            $.notify('Ligne ajouter avec succes !', 'success');

            $('.remove-ligne').on("click", function (e) {
                e.preventDefault();
                //$('.remove-ligne').button('loaderBtn');
                $(this).closest('tr').fadeOut(1000, function () {
                    $(this).remove();
                });

            });

            $("#comboexamenclinique option:selected").val(0);
            $("#examresultcli").val('');
        } else {
            $.notify('Impossible d\'ajouter cet examen elle existe déja !', 'error');
        }
        ;
    }


});


$('#ajoutlignetraitement').on("click", function (e) {
    e.preventDefault();

    var idmedicament = $("#combomedicament option:selected").val();
    var libellemedicament = $("#combomedicament option:selected").text();
    var idforme = $("#comboforme option:selected").val();
    var libelleforme = $("#comboforme option:selected").text();
    var idposologie = $("#comboposologie option:selected").val();
    var libelleposologie = $("#comboposologie option:selected").text();
    var idduree = $("#comboduree option:selected").val();
    var libelleduree = $("#comboduree option:selected").text();
    var qte = $("#qte").val();

    /* $("#comboexamenclinique").notify(
         "Veuillez selectionner l'examen clinique",
         {position: "right"},
         "warn"
     );*/
    if (idmedicament === 0) {
        $.notify("Veuillez selectionner le medicament !", 'warn');
        $("#combomedicament").focus();
    } else {
        if (idforme === 0) {
            $.notify("Veuillez selectionner la forme !", 'warn');
            $("#comboforme").focus();
        } else {
            if (idposologie === 0) {
                $.notify("Veuillez selectionner la posologie !", 'warn');
                $("#comboposologie").focus();
            } else {
                if (idduree === 0) {
                    $.notify("Veuillez selectionner la duree de traitement !", 'warn');
                    $("#combodureeqte").focus();
                } else {

                    if (qte === 0) {
                        $.notify("Veuillez saisir la quantité !", 'warn');
                        $("#qte").focus();
                    } else {
                        //Verifie si l'examen est déjà ajouter
                        var trouve = false;
                        $("#listetraitement").find('tr').each(function () {
                            if ($(this).find('.medic-code').text() === libellemedicament) {
                                trouve = true;
                            }
                            ;
                        });

                        if (trouve === false) {
                            $("#listetraitement").append("<tr>\n" +
                                "                                        <td class=\"medic-code\" id-medi=" + idmedicament + ">" + libellemedicament + "</td>\n" +
                                "                                        <td class=\"medic-forme\" id-forme=" + idforme + ">" + libelleforme + "</td>\n" +
                                "                                        <td class=\"medic-posologie\" id-posologie=" + idposologie + ">" + libelleposologie + "</td>\n" +
                                "                                        <td class=\"medic-duree\" id-duree=" + idduree + ">" + libelleduree + "</td>\n" +
                                "                                        <td class=\"medic-qte\" qte=" + qte + ">" + qte + "</td>\n" +
                                "                                        <td><span class=\"remove-ligne-traitement\">\n" +
                                "                                            <button type=\"button\" class=\"btn btn-danger\"><i\n" +
                                "                                                        class=\"fa fa-trash\"></i></button>\n" +
                                "                                        </span></td>\n" +
                                "                                    </tr>");

                            $.notify('Ligne ajouter avec succes !', 'success');

                            $('.remove-ligne-traitement').on("click", function (e) {
                                e.preventDefault();
                                //$('.remove-ligne').button('loaderBtn');
                                $(this).closest('tr').fadeOut(1000, function () {
                                    $(this).remove();
                                });

                            });

                            $('#combomedicament').val(0).trigger('change');
                            $('#comboforme').val(0).trigger('change');
                            $('#comboposologie').val(0).trigger('change');
                            $('#comboduree').val(0).trigger('change');
                            $("#qte").val(0);
                            modalTraitement.modal('hide');

                        } else {
                            $.notify('Impossible d\'ajouter ce medicament elle existe déja !', 'error');

                        }
                        ;
                    }
                }
            }
        }
    }


});


$('#enregistrermotif').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var codemotif = $('#codmotif');
    var libmotif = $('#libmotif');

    // if (codemotif.val() === "") {
    //     codemotif.notify(
    //         "Veuillez saisir le code du motif",
    //         {position: "up"},
    //         "warn"
    //     );
    //     codemotif.focus();
    // } else {
    //
    // }

    if (libmotif.val() === "") {
        libmotif.notify(
            "Veuillez saisir le libelle du motif",
            {position: "up"},
            "warn"
        );
        libmotif.focus();
    } else {
        codemotif.attr('disabled', true);
        libmotif.attr('disabled', true);
        btn.button('loading');
        $.ajax({
            url: addMotifUrl,
            type: "POST",
            data: {
                'code': codemotif.val(),
                'libelle': libmotif.val(),
            },
            success: function (data) {
                if (data['success'] === true) {
                    $.notify('Motif enregistrer !', 'success');
                    $('#listmotif').append("<option style=\"background-color: #0b97c4\"\n" +
                        "                                        value=" + codemotif.val() + ">" + libmotif.val() + "</option>");

                    codemotif.val("");
                    libmotif.val("");
                } else {
                    $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                }
                //modalTraitement.modal('hide');
                codemotif.removeAttr('disabled');
                libmotif.removeAttr('disabled');
                btn.button('reset');

            },
            error: function (error) {
                btn.button('reset');
            }
        })


    }

});


$('#enregistrermedicament').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var codemedicament = $('#codmedicament');
    var libmedicament = $('#libmedicament');


    // console.log("ok")

    if (codemedicament.val() === "") {
        codemedicament.notify(
            "Veuillez saisir le code du motif",
            {position: "up"},
            "warn"
        );
        codemedicament.focus();
    } else {


        if (libmedicament.val() === "") {
            libmedicament.notify(
                "Veuillez saisir le libelle du medicament",
                {position: "up"},
                "warn"
            );
            libmedicament.focus();
        } else {
            codemedicament.attr('disabled', true);
            libmedicament.attr('disabled', true);
            btn.button('loading');
            $.ajax({
                url: addMedicamentUrl,
                type: "POST",
                data: {
                    'code': codemedicament.val(),
                    'libelle': libmedicament.val(),
                },
                success: function (data) {
                    if (data['success'] === true) {
                        $.notify('Medicament enregistrer !', 'success');
                        $('#combomedicament').append("<option\n" +
                            "value=" + codemedicament.val() + ">" + libmedicament.val() + "</option>");

                        codemedicament.val("");
                        libmedicament.val("");
                    } else {
                        $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                        codemedicament.attr('disabled', false);
                        libmedicament.attr('disabled', false);
                    }
                    //modalTraitement.modal('hide');
                    codemedicament.attr('disabled', false);
                    libmedicament.attr('disabled', false);
                    btn.button('reset');

                },
                error: function (error) {
                    btn.button('reset');
                }
            })


        }
    }
});


$('#enregistrerforme').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var libforme = $('#libforme');


        if (libforme.val() === "") {
            libforme.notify(
                "Veuillez saisir le libelle du forme",
                {position: "up"},
                "warn"
            );
            libforme.focus();
        } else {
            libforme.attr('disabled', true);
            btn.button('loading');
            $.ajax({
                url: addFormeUrl,
                type: "POST",
                data: {
                    'libelle': libforme.val(),
                },
                success: function (data) {
                    if (data['success'] === true) {
                        $.notify('Forme enregistrer !', 'success');
                        $('#comboforme').append("<option\n" +
                            "value=" + libforme.val() + ">" + libforme.val() + "</option>");

                        libforme.val("");
                    } else {
                        $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                        libforme.attr('disabled', false);
                    }
                    //modalTraitement.modal('hide');
                    libforme.attr('disabled', false);
                    btn.button('reset');

                },
                error: function (error) {
                    btn.button('reset');
                }
            })


        }
});


$('#enregistrerposologie').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var libposologie = $('#libposologie');

        if (libposologie.val() === "") {
            libposologie.notify(
                "Veuillez saisir le libelle du posologie",
                {position: "up"},
                "warn"
            );
            libposologie.focus();
        } else {
            libposologie.attr('disabled', true);
            btn.button('loading');
            $.ajax({
                url: addPosologieUrl,
                type: "POST",
                data: {
                    'code': codeposologie.val(),
                    'libelle': libposologie.val(),
                },
                success: function (data) {
                    if (data['success'] === true) {
                        $.notify('Posologie enregistrer !', 'success');
                        $('#comboposologie').append("<option\n" +
                            "value=" + codeposologie.val() + ">" + libposologie.val() + "</option>");

                        libposologie.val("");
                    } else {
                        $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                        codeposologie.attr('disabled', false);
                        libposologie.attr('disabled', false);
                    }
                    //modalTraitement.modal('hide');
                    libposologie.attr('disabled', false);
                    btn.button('reset');

                },
                error: function (error) {
                    btn.button('reset');
                }
            })


        }
});


$('#enregistrerduree').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var libduree = $('#libduree');


    if (libduree.val() === "") {
        libduree.notify(
            "Veuillez saisir la duree",
            {position: "up"},
            "warn"
        );
        libduree.focus();
    } else {
        libduree.attr('disabled', true);
        btn.button('loading');
        $.ajax({
            url: addDureeUrl,
            type: "POST",
            data: {
                'libelle': libduree.val(),
            },
            success: function (data) {
                if (data['success'] === true) {
                    $.notify('duree enregistrer !', 'success');
                    $('#comboduree').append("<option\n" +
                        "value=" + codeduree.val() + ">" + libduree.val() + "</option>");

                    libduree.val("");
                } else {
                    $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                    libduree.attr('disabled', false);
                }
                //modalTraitement.modal('hide');
                libduree.attr('disabled', false);
                btn.button('reset');

            },
            error: function (error) {
                btn.button('reset');
            }
        })


    }
});


$('#enregistrerexamenpara').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var codeexampara = $('#codexamenpara');
    var libexampara = $('#libexamenpara');

    if (libexampara.val() === "") {
        libexampara.notify(
            "Veuillez saisir le libelle de l'examen para medical",
            {position: "up"},
            "warn"
        );
        libexampara.focus();
    } else {
        codeexampara.attr('disabled', true);
        libexampara.attr('disabled', true);
        btn.button('loading');
        $.ajax({
            url: addExamenParaUrl,
            type: "POST",
            data: {
                'code': codeexampara.val(),
                'libelle': libexampara.val(),
            },
            success: function (data) {
                if (data['success'] === true) {
                    $.notify('Examen para medical enregistrer !', 'success');
                    $('#listexamenpara').append("<option style=\"\"\n" +
                        "                                        value=" + codeexampara.val() + ">" + libexampara.val() + "</option>");

                    codeexampara.val("");
                    libexampara.val("");
                } else {
                    $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                }
                modalTraitement.modal('hide');
                codeexampara.removeAttr('disabled');
                libexampara.removeAttr('disabled');
                btn.button('reset');
            },
            error: function (error) {
                btn.button('reset');
            }
        })


    }
    // if (codeexampara.val() === "") {
    //     codeexampara.notify(
    //         "Veuillez saisir le code de l'examen para medical",
    //         {position: "up"},
    //         "warn"
    //     );
    //     codeexampara.focus();
    // } else {
    //
    // }

});


$('#enregistrerexamencli').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var codeexamcli = $('#codexamencli');
    var libexamcli = $('#libexamencli');

    if (libexamcli.val() === "") {
        libexamcli.notify(
            "Veuillez saisir le libelle de l'examen clinique",
            {position: "up"},
            "warn"
        );
        libexamcli.focus();
    } else {
        codeexamcli.attr('disabled', true);
        libexamcli.attr('disabled', true);
        btn.button('loading');
        $.ajax({
            url: addExamenClinUrl,
            type: "POST",
            data: {
                'code': codeexamcli.val(),
                'libelle': libexamcli.val(),
            },
            success: function (data) {
                if (data['success'] === true) {
                    $.notify('Examen clinique enregistrer !', 'success');
                    $('#comboexamenclinique').append("<option style=\"\"\n" +
                        "                                        value=" + codeexamcli.val() + ">" + libexamcli.val() + "</option>");

                    codeexamcli.val("");
                    libexamcli.val("");
                } else {
                    $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                }
                //modalEnregistreExamenCli.modal('hide');
                codeexamcli.removeAttr('disabled');
                libexamcli.removeAttr('disabled');
                btn.button('reset');
            },
            error: function (error) {
                btn.button('reset');
            }
        })


    }

    // if (codeexamcli.val() === "") {
    //     codeexamcli.notify(
    //         "Veuillez saisir le code de l'examen clinique",
    //         {position: "up"},
    //         "warn"
    //     );
    //     codeexamcli.focus();
    // } else {
    //
    // }

});


$('#enregistrerdiagnostic').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var codediagnos = $('#coddiagnostic');
    var libdiagnos = $('#libdiagnostic');

    if (libdiagnos.val() === "") {
        libdiagnos.notify(
            "Veuillez saisir le libelle du diagnostic",
            {position: "up"},
            "warn"
        );
        libdiagnos.focus();
    } else {
        codediagnos.attr('disabled', true);
        libdiagnos.attr('disabled', true);
        btn.button('loading');
        $.ajax({
            url: addDiagnosticUrl,
            type: "POST",
            data: {
                'code': codediagnos.val(),
                'libelle': libdiagnos.val(),
            },
            success: function (data) {
                if (data['success'] === true) {
                    $.notify('Examen clinique enregistrer !', 'success');
                    $('#listdiagnostic').append("<option style=\"\"\n" +
                        "                                        value=" + codediagnos.val() + ">" + libdiagnos.val() + "</option>");

                    codediagnos.val("");
                    libdiagnos.val("");
                } else {
                    $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                }
                //modalEnregistreExamenCli.modal('hide');
                codediagnos.removeAttr('disabled');
                libdiagnos.removeAttr('disabled');
                btn.button('reset');
            },
            error: function (error) {
                btn.button('reset');
            }
        })


    }

    // if (codediagnos.val() === "") {
    //     codediagnos.notify(
    //         "Veuillez saisir le code du diagnostic",
    //         {position: "up"},
    //         "warn"
    //     );
    //     codediagnos.focus();
    // } else {
    //
    // }

});


$('#btnenregistrer').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);

    /* if ($(this).html() === 'Save') {

         var nom = $("#nom");
         var prenom = $("#prenom");
         var tel = $("#tel");
         var datenaissance = $("#age");
         var sexe = $("#sexe");
         var situafamil = $("#situafamili");
         var adress = $("#adress");
         var medecintrait = $("#medetrait");
         var antmedi = $("#antmedi");
         var antfamil = $("#antfamili");
         var antchir = $("#antchir");
         var autres = $("#autres");

     } else {*/

    //var dateconsultation = $('#datepicker').data("date");
    //var dateconsultation = $('#dateconsultation');
    var listmotif = [];
    var listdiagnostic = [];
    var listexamenparacli = [];
    var resultatexamenparacli = $('#examresult');
    var tableexamencli = [];
    var tabletraitement = [];
    var tabledetailpatient = [];

    //Liste Motif
    $("select[id='listmotif'] option:selected").each(function () {
        listmotif.push($(this).val());
    });

    //Liste diagnostic
    $("select[id='listdiagnostic'] option:selected").each(function () {
        listdiagnostic.push($(this).val());
    });

    //Liste Examen Para
    $("select[id='listexamenpara'] option:selected").each(function () {
        listexamenparacli.push($(this).val());
    });

    //Liste des examens cliniques
    $("#listeexamenclinique").find('tr').each(function () {
        tableexamencli.push({
            idexamen: $(this).find('.exam-code').attr('id-exam'),
            resultat: $(this).find('.exam-result').text()
        })
    });

    //Liste des traitement
    $("#listetraitement").find('tr').each(function () {
        tabletraitement.push({
            idmedicament: $(this).find('.medic-code').attr('id-medi'),
            idforme: $(this).find('.medic-forme').attr('id-forme'),
            idposologie: $(this).find('.medic-posologie').attr('id-posologie'),
            idduree: $(this).find('.medic-duree').attr('id-duree'),
            qte: $(this).find('.medic-qte').attr('qte')
        })
    });


    /*       if (idPatient === "") {
               $('#consulter').notify(
                   "Veuillez sélectionner le patient a consulter",
                   {position: "down"},
                   "warn"
               );
               $('#consulter').focus();
           } else {
               if (dateconsultation.data("date") === "") {
                   dateconsultation.notify(
                       "Veuillez sélectionner une date",
                       {position: "right"},
                       "warn"
                   );
                   dateconsultation.focus();
               } else {*/
    if (idTicket !== "") {

        swalWithBootstrapButtons.fire({
            title: 'Confirmation !',
            text: "Voulez-vous vraiment enregistrer cette consultation ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Oui !',
            cancelButtonText: 'Non !',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                btn.addClass('disabled', true);
                /*tabledetailpatient.push({
                    antmedical : $('#antmedipat').val(),
                    antchirugical : $('#antchirpat').val(),
                    antfamilial: $('#antfamilipat').val(),
                    autres: $('#autrespat').val(),
                });*/
                $.ajax({
                    url: addConsultationUrl,
                    type: "POST",
                    data: {
                        'idticket': idTicket,
                        'listmotif': listmotif,
                        'listdiagnostic': listdiagnostic,
                        'listexamenpara': listexamenparacli,
                        'resultatexampara': resultatexamenparacli.val(),
                        'listexamencli': tableexamencli,
                        'listtraitement': tabletraitement,
                        'antmedical': $('#antmedipat').val(),
                        'antchirugical': $('#antchirpat').val(),
                        'antfamilial': $('#antfamilipat').val(),
                        'autres': $('#autrespat').val(),
                        'poids': $('#poidsconsul').val(),
                        'temp': $('#tempconsul').val(),
                        'tens': $('#tensconsul').val(),
                        'obser': $('#observconsul').val(),
                        'imprimeordonance': $('#checkboxordonnance').val(),
                        'sendordonnance': $('#checkboxemail').val(),
                        'conduiteatenir': $('#conduiteatenir').val(),
                    },
                    success: function (data) {

                        if (data['success'] === true) {
                            swalWithBootstrapButtons.fire(
                                'Enregistrement!',
                                'Consultation enregistrer avec success ! Génération de l\'ordonnance en cours !',
                                'success'
                            );

                            $("#nom").val('');
                            $("#prenom").val('');
                            $("#tel").val('');
                            $("#age").val('');
                            $("#sexe").val('');
                            $("#situafamili").val('');
                            $("#adress").val('');
                            $("#medetrait").val('');
                            $("#antmedipat").val('');
                            $("#antfamilipat").val('');
                            $("#antchirpat").val('');
                            $("#autrespat").val('');
                            $("#examresult").val('');
                            $("#examresultcli").val('');
                            $("#poidsconsul").val('');
                            $("#tempconsul").val('');
                            $("#tensconsul").val('');
                            $("#observconsul").val('');
                            $("#conduiteatenir").val('');
                            $("#dateconsultation").data('');

                            //$('#listmotif').closest('.form-group').find("lu").remove();
                            //$('#listdiagnostic').closest('.form-group').find("lu").remove();
                            //$('#listexamenpara').closest('.form-group').find("lu").remove();
                            $('#listmotif').val(null).trigger('change');
                            $('#listdiagnostic').val(null).trigger('change');
                            $('#listexamenpara').val(null).trigger('change');
                            $('#comboexamenclinique').val(0).trigger('change');
                            $('#listeexamenclinique').html("<tr>\n" +
                                "                                        <th class=\"col-md-4\">Examen</th>\n" +
                                "                                        <th>Resultat</th>\n" +
                                "                                        <th style=\"width: 40px\">Sup</th>\n" +
                                "                                    </tr>");
                            $('#listetraitement').html("<tr>\n" +
                                "                                        <th class=\"col-md-2\">Medicament</th>\n" +
                                "                                        <th>Forme</th>\n" +
                                "                                        <th>Posologie</th>\n" +
                                "                                        <th>Durée</th>\n" +
                                "                                        <th>Qte</th>\n" +
                                "                                        <th style=\"width: 40px\">Sup</th>\n" +
                                "                                    </tr>");

                            $('#ticket').val('');
                            formPanel.hide('2000');
                            formPanel1.hide('3000');

                            $.ajax({
                                url: ordonnanceConsultationUrl.replace('code', idTicket),
                                type: "POST",
                                data: '',
                                success: function (data) {

                                    swalWithBootstrapButtons.fire(
                                        'Générer!',
                                        'Ordonnance générer avec succès !',
                                        'success'
                                    );

                                },
                                error: function (error) {

                                }
                            })

                        } else {
                            swalWithBootstrapButtons.fire(
                                'Enregistrement!',
                                'Echec de l\'enregistrement de la consultation!',
                                'error'
                            )

                            //$.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                        }
                        btn.removeClass('disabled');

                    },
                    error: function (error) {

                    }
                })

            }
        });

    } else {
        Swal.fire({
            "title": "Warning!",
            "html": `Veuillez vous assurer que le ticket est sélectionné !`,
            "type": "warning",
            "showConfirmButton": false,
            "showCloseButton": true,
            "allowEscapeKey": false,
            "allowOutsideClick": false
        });
        //});
        //     Swal.fire({
        //         "title":"Warning!",
        //         "html":`You can not use <b>cdn</b> and <b>local</b> together! <br />
        // You can only use <b>cdn</b> or <b>local</b>! <br />
        // If you want to use <b>cdn</b> goto <b>.env</b> and change <pre>SWEET_ALERT_LOCAL=false</pre>`,
        //         "type":"warning",
        //         "showConfirmButton":false,
        //         "showCloseButton":true,
        //         "allowEscapeKey":false,
        //         "allowOutsideClick":false
        //     });

        /*}
    });*/
        /*            }
                }*/
        //
    }

});


$('#tablelisteconsultation').on('click', '.dupliqueconsultation', function (e) {
    e.preventDefault();
    idConsultationADuplique = $(this).attr('data-id');
});

$('#btndupliquerconsultation').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var idpat = $('#idpatientduplique');

    if (idpat.val() === "0") {
        idpat.notify(
            "Veuillez sélectionner le patient pour lequel vous voulez dupliquer la consultation",
            {position: "down"},
            "warn"
        );
        idpat.focus();
    } else {
        swalWithBootstrapButtons.fire({
            title: 'Confirmation !',
            text: "Voulez-vous vraiment dupliquer cette consultation ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Oui !',
            cancelButtonText: 'Non !',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                btn.button('loading');
                idpat.addClass('disabled');
                $.ajax({
                    url: dupliqueConsultationUrl,
                    type: "POST",
                    data: {
                        'idpatient': idpat.val(),
                        'idconsultation': idConsultationADuplique,
                    },
                    success: function (data) {

                        if (data['success'] === true) {
                            swalWithBootstrapButtons.fire(
                                'Dupliquer!',
                                'Opération effectuée avec success !',
                                'success'
                            );


                        } else {
                            swalWithBootstrapButtons.fire(
                                'Enregistrement!',
                                'Echec de l\'enregistrement de la consultation!',
                                'error'
                            )

                            //$.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                        }

                    },
                    error: function (error) {

                    }
                }).always(function () {
                    btn.button('reset');
                    idpat.removeClass('disabled');

                });

            }
        });
    }

});

$('#nouveaupatient').on('click', function (e) {

    // const swalWithBootstrapButtons = Swal.mixin({
    //     customClass: {
    //         confirmButton: 'btn btn-success',
    //         cancelButton: 'btn btn-danger'
    //     },
    //     buttonsStyling: false
    // })
    //
    // swalWithBootstrapButtons.fire({
    //     title: 'Are you sure?',
    //     text: "You won't be able to revert this!",
    //     type: 'warning',
    //     showCancelButton: true,
    //     confirmButtonText: 'Yes, delete it!',
    //     cancelButtonText: 'No, cancel!',
    //     reverseButtons: true
    // }).then((result) => {
    //     if (result.value) {
    //         swalWithBootstrapButtons.fire(
    //             'Deleted!',
    //             'Your file has been deleted.',
    //             'success'
    //         )
    //     } else if (
    //         /* Read more about handling dismissals below */
    //         result.dismiss === Swal.DismissReason.cancel
    //     ) {
    //         swalWithBootstrapButtons.fire(
    //             'Cancelled',
    //             'Your imaginary file is safe :)',
    //             'error'
    //         )
    //     }
    // })
    e.preventDefault();
    modalSearchPatient.modal('hide');
    formPanel1.show(3000);
    $("#nom").removeAttr('disabled');
    $("#prenom").removeAttr('disabled');
    $("#tel").removeAttr('disabled');
    $("#age").removeAttr('disabled');
    $("#sexe").removeAttr('disabled');
    $("#situafamili").removeAttr('disabled');
    $("#adress").removeAttr('disabled');
    $("#medetrait").removeAttr('disabled');
    $("#antmedipat").removeAttr('disabled');
    $("#antfamilipat").removeAttr('disabled');
    $("#antchirpat").removeAttr('disabled');
    $("#autrespat").removeAttr('disabled');

    /*$("#consuiteatenir").removeAttr('disabled');
    $("#checkboxordonnance").removeAttr('disabled');
    $("#checkboxemail").removeAttr('disabled');*/
    $("#btnenregistrer").removeAttr('disabled');
    $("#btnenregistrer").text('Save');
});

$("#tablelisteconsultation").on('click', '.deletedconsultation', function (e) {
    e.preventDefault();
    var btn = $(this);
    var id = $(this).attr('data-id');


    swalWithBootstrapButtons.fire({
        title: 'Confirmation !',
        text: "Voulez-cous vraiment supprimer cette consultation ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            btn.button('loader');
            $.ajax({
                url: deletedConsultationUrl,
                type: "POST",
                data: {
                    id: id
                },
                success: function (data) {

                    if (data['success'] === true) {
                        swalWithBootstrapButtons.fire(
                            'Supprimer !',
                            'Consultation supprimer avec success !',
                            'success'
                        );
                        btn.closest('td').closest('tr').fadeOut(2000, function () {
                            btn.closest('td').closest('tr').remove();
                        });
                    } else {
                        swalWithBootstrapButtons.fire(
                            'Supprimer!',
                            'Echec de la suppression de la consultation!',
                            'error'
                        )

                        //$.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                    }

                },
                error: function (error) {

                }
            })

        }
    });
});


$("#tablelistedebutconsultation").on('click', '.deletedconsultation', function (e) {
    e.preventDefault();
    var btn = $(this);
    var id = $(this).attr('data-id');


    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "Voulez-cous vraiment supprimer cette consultation !",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui !',
        cancelButtonText: 'Non !',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            btn.button('loader');
            $.ajax({
                url: deletedConsultationUrl,
                type: "POST",
                data: {
                    id: id
                },
                success: function (data) {

                    if (data['success'] === true) {
                        swalWithBootstrapButtons.fire(
                            'Supprimer !',
                            'Consultation supprimer avec success !',
                            'success'
                        );
                        btn.closest('td').closest('tr').fadeOut(2000, function () {
                            btn.closest('td').closest('tr').remove();
                        });
                    } else {
                        swalWithBootstrapButtons.fire(
                            'Supprimer!',
                            'Echec de la suppression de la consultation!',
                            'error'
                        )

                        //$.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                    }

                },
                error: function (error) {

                }
            })

        }
    });
});

$("#tablelisteconsultation").on('click', '.imprimerordonnance', function (e) {
    e.preventDefault();
    var btn = $(this);
    var id = $(this).attr('data-id');

    btn.button('loading');
    $.ajax({
        url: ordonnanceConsultationUrl.replace('code', id),
        type: "POST",
        data: '',
        success: function (data) {

            btn.button('reset');

        },
        error: function (error) {
            btn.button('reset');

        }
    })


});


$('#savepreconsul').on('click', function (e) {
        e.preventDefault();

        //console.log("ok");
        var btn = $(this);
        var idPatient = $('#nompatientselection');
        var poids = $('#poids');
        var temp = $('#temp');
        var tension = $('#tens');
        var taille = $('#tail');
        var observation = $('#observa');
        console.log("ok");
        if (idPatient.attr('data-id-patient') === "") {
            idPatient.notify(
                "Veuillez sélectionner le patient a consulter",
                {position: "down"},
                "warn"
            );
            idPatient.focus();
        } else {
            if (poids.val() === "") {
                poids.notify(
                    "Veuillez saisir le poids",
                    {position: "right"},
                    "warn"
                );
                poids.focus();
            } else {
                if (temp.val() === "") {
                    temp.notify(
                        "Veuillez saisir la temperature",
                        {position: "right"},
                        "warn"
                    );
                    temp.focus();
                } else {
                    if (tension.val() === "") {
                        tension.notify(
                            "Veuillez saisir la tension",
                            {position: "right"},
                            "warn"
                        );
                        tension.focus();
                    } else {
                        if (taille.val() === "") {
                            taille.notify(
                                "Veuillez saisir la taille",
                                {position: "right"},
                                "warn"
                            );
                            taille.focus();
                        } else {

                            swalWithBootstrapButtons.fire({
                                title: 'Confirmation !',
                                text: "Voulez-vous vraiment enregistrer cette pré-consultation ?",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Oui !',
                                cancelButtonText: 'Non !',
                                reverseButtons: true
                            }).then((result) => {
                                if (result.value) {
                                    btn.button('loading');
                                    $('input').attr('disabled');
                                    $.ajax({
                                        url: addPreConsultationAdd,
                                        type: "POST",
                                        data: {
                                            'idpatient': idPatient.attr('data-id-patient'),
                                            'poids': poids.val(),
                                            'temp': temp.val(),
                                            'tension': tension.val(),
                                            'taille': taille.val(),
                                            'observation': observation.val(),
                                        },
                                        success: function (data) {

                                            if (data['ticket'] !== "") {
                                                swalWithBootstrapButtons.fire(
                                                    'Enregistrement!',
                                                    'Consultation enregistrer avec success !',
                                                    'success'
                                                );
                                                table1.row.add([
                                                    data['ticket'],
                                                    data['date'],
                                                    data['lepatient'],
                                                    poids.val(),
                                                    temp.val(),
                                                    tension.val(),
                                                    taille.val(),
                                                    "                            <a data-id=" + data['ticket'] + " type=\"button\"\n" +
                                                    "                               class=\"btn btn-danger deletedconsultation\"><i class=\"fa fa-trash\"></i>\n" +
                                                    "                            </a>"
                                                ]).draw();

                                                idPatient.val('');
                                                poids.val('');
                                                temp.val('');
                                                tension.val('');
                                                observation.val('');
                                                taille.val('');
                                                $('#nompatientselection').val('');
                                                $('#prenompatientselection').val('');
                                                $('#telpatientselection').val('');
                                                $('#nomrecherche').val('');
                                                $('#prenomrecherche').val('');


                                            } else {
                                                swalWithBootstrapButtons.fire(
                                                    'Enregistrement!',
                                                    'Echec de l\'enregistrement de la consultation!',
                                                    'error'
                                                )

                                                //$.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                                            }

                                            //$('input').removeAttr('disabled');
                                            btn.button('reset');
                                            modalDebutConsultation.modal('hide');

                                        },
                                        error: function (error) {
                                            btn.button('reset');

                                        }
                                    })

                                }
                            });
                        }
                    }
                }
            }
        }
    }
);


$('#enregistrernewpatient').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    var nompatient = $('#nompatient');
    var prenompatient = $('#prenompatient');
    var telephonepatient = $('#telpatient');
    var adressepatient = $('#adrespatient');
    var sexepatient = $("#combosexepatient option:selected");
    var typepatient = $("#combotypepatient option:selected");

    // if (codemotif.val() === "") {
    //     codemotif.notify(
    //         "Veuillez saisir le code du motif",
    //         {position: "up"},
    //         "warn"
    //     );
    //     codemotif.focus();
    // } else {
    //
    // }

    //console.log(sexepatient.val());
    if (nompatient.val() === "") {
        nompatient.notify(
            "Veuillez saisir le nom du patient",
            {position: "up"},
            "warn"
        );
        nompatient.focus();
    } else {
        if (prenompatient.val() === "") {
            prenompatient.notify(
                "Veuillez saisir le prénom du patient",
                {position: "up"},
                "warn"
            );
            prenompatient.focus();
        } else {
            if (sexepatient.val() === "0") {
                $("#combosexepatient").notify(
                    "Veuillez sélectionner le sexe du patient",
                    {position: "up"},
                    "warn"
                );
                $("#combosexepatient").focus();
            } else {
                if (typepatient.val() === "0") {
                    $("#combotypepatient").notify(
                        "Veuillez sélectionner le type du patient",
                        {position: "up"},
                        "warn"
                    );
                    $("#combotypepatient").focus();
                } else {
                    prenompatient.attr('disabled', true);
                    nompatient.attr('disabled', true);
                    sexepatient.attr('disabled', true);
                    typepatient.attr('disabled', true);
                    telephonepatient.attr('disabled', true);
                    adressepatient.attr('disabled', true);
                    btn.button('loading');
                    //console.log(nompatient.val(),prenompatient.val(),sexepatient.val(),typepatient.val());
                    $.ajax({
                        url: addPatientAdd,
                        type: "POST",
                        data: {
                            'nom': nompatient.val(),
                            'prenom': prenompatient.val(),
                            'sexe': sexepatient.val(),
                            'typepatient': typepatient.val(),
                            'telpatient': telephonepatient.val(),
                            'adrespatient': adressepatient.val(),
                        },
                        success: function (data) {
                            if (data['success'] === true) {
                                $.notify('Patient enregistrer !', 'success');

                                nompatient.val("");
                                prenompatient.val("");
                                telephonepatient.val("");
                                adressepatient.val("");
                                sexepatient.val(0).trigger('change');
                                typepatient.val(0).trigger('change');
                            } else {
                                $.notify('Erreur veuillez réessayer ! Si cela persiste contactez l\'administrateur !', 'error');
                            }
                            //modalTraitement.modal('hide');
                            nompatient.removeAttr('disabled');
                            prenompatient.removeAttr('disabled');
                            telephonepatient.removeAttr('disabled');
                            adressepatient.removeAttr('disabled');
                            sexepatient.removeAttr('disabled');
                            typepatient.removeAttr('disabled');
                            btn.button('reset');

                        },
                        error: function (error) {
                            btn.button('reset');
                        }
                    })


                }
            }
        }
    }


});


$('#tablelisteconsultation').on('click', '.detailconsultation', function (e) {
    e.preventDefault();
    idConsultationADuplique = $(this).attr('data-id');
});


/*$('#cacher-consultation').on('click', function (e) {
    e.preventDefault();

    if ($(this).text() === 'Cacher'){
        formPanel.hide(2000);
        $(this).text('Afficher');
    }else{
        formPanel.show(2000);
        $(this).text('Cacher');

    }



})*/
//$.notify('Veuillez saisir au moins le nom ou le prénom','error');