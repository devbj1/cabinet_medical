var loader = '<div class="loader text-center mt-3 mb-3"><i class="fa fa-spinner fa-spin fa-3x"></i></div>';
var dateDebut = "";
var dateFin = "";

$('#daterange-btn').daterangepicker(
    {
        ranges: {
            'Aujourd\'hui': [moment(), moment()],
            'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 derniers jours': [moment().subtract(6, 'days'), moment()],
            '30 derniers jours': [moment().subtract(29, 'days'), moment()],
            'Ce mois': [moment().startOf('month'), moment().endOf('month')],
            'Mois passé': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment()
    },
    function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
);


//Datatable

var table = $('#tablelistestatistiqueconsultation').DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    },
    'dom': 'Bfrtip',
    'buttons': [
        {
            extend: 'copyHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5 ]
            }
        },
        {
            extend: 'excelHtml5',
            title: 'Liste des Consultations',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            }
        },
        //'csv',
        {
            extend: 'pdfHtml5',
            title: 'Liste des Consultations',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5]
            }
        },
        'print'
    ],
    'extend': 'pdfHtml5',
    'title': 'Liste des consultations',
    'text': 'The text',
    orientation: 'landscape',
    'pageSize': 'A4',
    'exportOptions': {
        columns: [0, 1, 2, 3]
    },
    customize: function (doc) {
        doc.content[1].table.widths = [
            '10%',
            '20%',
            '20%',
            '10%',
            '30%',
            '10%'
        ]
    }

});


function renvoiemoisentier(moislettre) {
    moislettre = moislettre.trim();
    if (moislettre === 'January') {
        return '01';
    }
    if (moislettre === 'February') {
        return '02';
    }
    if (moislettre === 'March') {
        return '03';
    }
    if (moislettre === 'April') {
        return '04';
    }
    if (moislettre === 'May') {
        return '05';
    }
    if (moislettre === 'June') {
        return '06';
    }
    if (moislettre === 'July') {
        return '07';
    }
    if (moislettre === 'August') {
        return '08';
    }
    if (moislettre === 'September') {
        return '09';
    }
    if (moislettre === 'October') {
        return '10';
    }
    if (moislettre === 'November') {
        return '11';
    }
    if (moislettre === 'December') {
        return '12';
    }
}


function renvoiejourentier(jourlettre) {
    jourlettre = jourlettre.trim();
    if (jourlettre.length === 1) {
        return '0' + jourlettre;
    } else {
        return jourlettre;
    }
}

$('#consultationperiode').on('click', function (e) {
    e.preventDefault();

    var btn = $(this);
    var datechaine = $('#daterange-btn').find('span').html();

    var datechainecouper = datechaine.split(" - ");

    var datedebutchaine = datechainecouper[0];
    var datefinchaine = datechainecouper[1];

    var datedebutchaineslipt = datedebutchaine.split(" ");
    var datedebutchainemois = renvoiemoisentier(datedebutchaineslipt[0]);

    var datedebutchaineyear = datedebutchaineslipt[2];
    var datedebutchainejour = renvoiejourentier(datedebutchaineslipt[1].split(",")[0]);

    var datefinchaineslipt = datefinchaine.split(" ");
    var datefinchainemois = renvoiemoisentier(datefinchaineslipt[0]);
    var datefinchaineyear = datefinchaineslipt[2];
    var datefinchainejour = renvoiejourentier(datefinchaineslipt[1].split(",")[0]);

    var datedeb = datedebutchaineyear + "-" + datedebutchainemois + "-" + datedebutchainejour;
    var datefin = datefinchaineyear + "-" + datefinchainemois + "-" + datefinchainejour;

    btn.button('loading');
    $.ajax({
        url: listConsultationPeriode,
        type: "POST",
        data: {
            'datedeb': datedeb,
            'datefin': datefin,
        },
        success: function (data) {
            if (data.length !== 0){
                $('#tablelignestatistiqueconsultation').html('');
            }
            table.clear().draw();
            data.forEach(function (w) {
                table.row.add([
                    w.dateConsultation['date'].split(".")[0],
                    w.patient,
                    w.sexe ,
                    w.telPatient,
                    w.conduiteATenir,
                    w.consulterPar
                ]).draw();


/*                $('#tablelignestatistiqueconsultation').append("<tr>\n" +
                    "                    <td>" + w.dateConsultation + "</td>\n" +
                    "                    <td>" + w.patient + "</td>\n" +
                    "                    <td>" + w.sexe + "</td>\n" +
                    "                    <td>" + w.telPatient + "</td>\n" +
                    "                    <td>" + w.conduiteATenir + "</td>\n" +
                    "                    <td>" + w.consulterPar + "</td>\n" +
                    "                </tr>").hide().fadeIn(1000);*/
            });

            btn.button('reset');
        },
        error: function (error) {

        }

    }).always(function () {
        btn.button('reset')
    });
    //var sansespacedatedebutchaine = datedebutchaine.trim();
    //var sansespacedatefinchaine = datefinchaine.trim();

    //console.log(sansespacedatedebutchaine)
    //console.log(sansespacedatefinchaine)
    //console.log(datedebutchaine,datefinchaine)

});