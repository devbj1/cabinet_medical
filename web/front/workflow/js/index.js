$(function () {
    var transitionListHidden = true;
    var workFlowListHidden = true;
    var modalAjoutTransition = $('#ModalAjouterTransition');
    var modalAjoutWorkflow = $('#ModalAjouterWorkflow');
    var loader = '<div class="loader text-center mt-3 mb-3"><i class="fa fa-refresh fa-spin fa-3x"></i></div>'
    var allTransition = [];
    var allTransitionControle = [];
    var listeEtapeCombo = "";
    var allEtape = [];
    var allDelete = [];
    $('#title-list-transition').on('click', function (e) {
        e.preventDefault();
        transitionListHidden = showBody($(this), transitionListHidden, 'transition')
    });
    $('#title-list-workflow').on('click', function (e) {
        e.preventDefault();
        workFlowListHidden = showBody($(this), workFlowListHidden, 'workflow')
    });

    function videmodalEtape() {
        $("#libellemodetape").val('');
        $("#delaimodetape").val('');
    }


    function showBody(element, list, namelist) {
        var url = element.attr('data-url');
        var formPanel = element.closest('.row').find('.form-panel');
        if (list) {
            formPanel.show('slow');
            list = false;
            $.ajax({
                type: "GET",
                url: url,
                success: function (data) {
                    formPanel.html(data);

                    if (namelist === 'workflow') {
                        $('.edit-workflow').on('click', function (e) {
                            var btnworkflow = $(this);
                            btnworkflow.button('loading');
                            e.preventDefault();
                            $.ajax({
                                url: btnworkflow.attr('data-href'),
                                type: "GET",
                                success: function (data) {
                                    btnworkflow.button('reset');

                                    $('#content-workflow-edit').html(data);
                                    allTransition = JSON.parse($('#allTransitions').val())
                                    $('#ModalModifierWorkflow').modal('show');
                                    $('.removeLine-edit').on('click', function (e) {
                                        e.preventDefault();
                                        removeLine($(this));
                                    });

                                    $('#addlignetransition-edit').on('click', function () {
                                        var id = addTransition('edit');
                                        if (id !== false) {
                                            var b = parseInt(id) + 1;
                                            newDiv(b, 'edit');
                                            // $("#idDiv").val(b);
                                            $("#addlignetransition-edit").val(b);
                                            $("#removelignetransition-edit").val(b);
                                            // console.log(allTransition);

                                            $('.removeLine-edit').unbind('click');
                                            $('.removeLine-edit').on('click', function (e) {
                                                // console.log('ok')

                                                e.preventDefault();
                                                removeLine($(this));
                                            });
                                        }
                                        ;
                                    });
                                    $('#editworkflow').on('click', function () {
                                        var btnworkflow = $(this);
                                        allTransition = [];
                                        e.preventDefault();
                                        btnworkflow.button('loading');
                                        var libelleworkflow = $("#libellemodworkflow-edit");
                                        var idsociete = $("#societeworkflow-edit");
                                        var idtypedemande = $("#typedemandeworkflow-edit");

                                        if (libelleworkflow.val().trim() === '') {
                                            libelleworkflow.notify(
                                                "Veuillez saisir le libelle du workflow",
                                                {position: "right"},
                                                "warn"
                                            );
                                            libelleworkflow.focus();
                                        } else {
                                            if (idsociete.val() === null) {
                                                idsociete.notify(
                                                    "Veuillez selectionner la societe",
                                                    {position: "right"},
                                                    "warn"
                                                );
                                                idsociete.focus();
                                            } else {
                                                if (idtypedemande.val() === null) {
                                                    idtypedemande.notify(
                                                        "Veuillez selectionner le type de demande",
                                                        {position: "right"},
                                                        "warn"
                                                    );
                                                    idtypedemande.focus();
                                                } else {

                                                    // if (addTransition() !== false) {
                                                    fillEtapes();
                                                    // console.log(allDelete);
                                                    $.ajax({
                                                        url: $(this).attr('data-url'),
                                                        type: "POST",
                                                        data: {
                                                            libelle: libelleworkflow.val(),
                                                            societe: idsociete.val(),
                                                            typedemande: idtypedemande.val(),
                                                            transitions: allTransition,
                                                            transitionsdelete: allDelete,
                                                        },
                                                        success: function (data) {
                                                            btnworkflow.button('reset');
                                                            $.notify("Modification effectuée avec succes !", "success");
                                                            videmodalworflow();
                                                            $('#ModalModifierWorkflow').modal('hide');
                                                            $('#content-workflow-edit').html('');
                                                        }
                                                    })
                                                    // }
                                                }

                                            }
                                        }
                                        // else {
                                        //     $.notify("Veuillez saisir les transitions du workflow !", "error");
                                        // }
                                    });


                                }
                            })
                        })
                        $('.show-workflow').on('click', function (e) {
                            var btnworkflow = $(this);
                            btnworkflow.button('loading');
                            e.preventDefault();

                            $.ajax({
                                url: btnworkflow.attr('data-href'),
                                type: "GET",
                                success: function (data) {
                                    btnworkflow.button('reset');
                                    $('#content-workflow-apercu').html(data);
                                    $('#ModalApercuWorkflow').modal('show');
                                },
                                error: function (err) {
                                    btnworkflow.button('reset');
                                }
                            })
                        })
                    }

                    if (namelist === 'etape') {
                        $('.edit-etape').on('click', function (e) {
                            var btnetape = $(this);
                            btnetape.button('loading');
                            e.preventDefault();
                            $.ajax({
                                url: btnetape.attr('data-href'),
                                type: "GET",
                                success: function (data) {
                                    btnetape.button('reset');
                                    $('#content-etape-edit').html(data);
                                    $('#ModalModifierEtape').modal('show');
                                    $('#editetape').on('click', function () {
                                        var btn1 = $(this);
                                        btn1.button('loading');
                                        var libelle = $("#libellemodetape-edit");
                                        var service = $("#servicemodetape-edit");
                                        var delai = $("#delaimodetape-edit");

                                        if (libelle.val().trim() === '') {
                                            libelle.notify(
                                                "Veuillez saisir le libelle de l'etape",
                                                {position: "right"},
                                                "warn"
                                            );
                                            libelle.focus();
                                        } else {
                                            if (service.val() === 0) {
                                                service.notify(
                                                    "Veuillez selectionner le service",
                                                    {position: "right"},
                                                    "warn"
                                                );
                                                service.focus();
                                            } else {
                                                if (delai.val().trim() === null) {
                                                    delai.notify(
                                                        "Veuillez saisir le delai",
                                                        {position: "right"},
                                                        "warn"
                                                    );
                                                    delai.focus();
                                                } else {
                                                    var etape = {
                                                        libelleEtape: libelle.val(),
                                                        delaiEtape: delai.val(),
                                                        service: service.val(),
                                                    };
                                                    $.ajax({
                                                        url: $(this).attr('data-url'),
                                                        type: "POST",
                                                        data: {
                                                            tab: etape,
                                                        },
                                                        success: function (data) {
                                                            btn1.button('reset');
                                                            $.notify("Modification effectuée avec succes !", "success");
                                                            $('#ModalModifierEtape').modal('hide');
                                                            $('#content-etape-edit').html('');
                                                        }
                                                    })
                                                }

                                            }
                                        }
                                        // else {
                                        //     $.notify("Veuillez saisir les transitions du workflow !", "error");
                                        // }
                                    });

                                }
                            })
                        })
                    }

                    if (namelist === 'transition') {
                        $('.edit-transition').on('click', function (e) {
                            var btnetape = $(this);
                            btnetape.button('loading');
                            e.preventDefault();
                            $.ajax({
                                url: btnetape.attr('data-href'),
                                type: "GET",
                                success: function (data) {
                                    btnetape.button('reset');
                                    $('#content-etape-edit').html(data);
                                    $('#ModalModifierEtape').modal('show');
                                    $('#editetape').on('click', function () {
                                        var btn1 = $(this);
                                        btn1.button('loading');
                                        var libelle = $("#libellemodetape-edit");
                                        var service = $("#servicemodetape-edit");
                                        var delai = $("#delaimodetape-edit");

                                        if (libelle.val().trim() === '') {
                                            libelle.notify(
                                                "Veuillez saisir le libelle de l'etape",
                                                {position: "right"},
                                                "warn"
                                            );
                                            libelle.focus();
                                        } else {
                                            if (service.val() === 0) {
                                                service.notify(
                                                    "Veuillez selectionner le service",
                                                    {position: "right"},
                                                    "warn"
                                                );
                                                service.focus();
                                            } else {
                                                if (delai.val().trim() === null) {
                                                    delai.notify(
                                                        "Veuillez saisir le delai",
                                                        {position: "right"},
                                                        "warn"
                                                    );
                                                    delai.focus();
                                                } else {
                                                    var etape = {
                                                        libelleEtape: libelle.val(),
                                                        delaiEtape: delai.val(),
                                                        service: service.val(),
                                                    };
                                                    $.ajax({
                                                        url: $(this).attr('data-url'),
                                                        type: "POST",
                                                        data: {
                                                            tab: etape,
                                                        },
                                                        success: function (data) {
                                                            btn1.button('reset');
                                                            $.notify("Modification effectuée avec succes !", "success");
                                                            $('#ModalModifierEtape').modal('hide');
                                                            $('#content-etape-edit').html('');
                                                        }
                                                    })
                                                }

                                            }
                                        }
                                        // }
                                    });

                                }
                            })
                        })

                        $('.delete-transition').on('click', function (e) {
                            var btndelete = $(this);
                            btndelete.button('loading');
                            e.preventDefault();
                            var formPanel = $(this).closest('.row').find('.form-panel');
                            formPanel.hide('slow');
                            swal({
                                title: "Confirmation !",
                                text: "Voulez-vous vraiment supprimer cet enregistrement!",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $.ajax({
                                        url: btndelete.attr('data-href'),
                                        type: "GET",
                                        success: function (data) {
                                            swal({
                                                title: "Alert!",
                                                icon: "success",
                                                button: false,
                                                text: "Suppression effectué avec succès",
                                                timer: 1500
                                            });
                                        }
                                    })
                                }
                            });
                            btndelete.button('reset');
                            formPanel.html(loader);
                        });
                    }
                }
            });
        } else {
            list = true;
            formPanel.hide('slow')
        }
        return list;
    }


    $('.delete-transition').on('click', function (e) {
        var formPanel = $(this).closest('.row').find('.form-panel');
        formPanel.hide('slow');
        var btndelete = $(this);
        btndelete.button('loading');
        e.preventDefault();
        swal({
            title: "Confirmation !",
            text: "Voulez-vous vraiment supprimer cet enregistrement!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: btndelete.attr('data-href'),
                    type: "GET",
                    success: function (data) {
                        // alert(data);
                        // removeLineTableTransition($(this));
                        swal({
                            title: "Alert!",
                            icon: "success",
                            button: false,
                            text: "Suppression effectué avec succès",
                            timer: 1500
                        });
                    }
                })
            }
        });
        formPanel.html(loader);
        btndelete.button('reset');
    });


    $('#btnajouterTransition').on('click', function (e) {
        e.preventDefault();
        var formPanel = $(this).closest('.row').find('.form-panel');
        formPanel.hide('slow');
        // alert('Ok');
        var listeWorkflow = "<option value='' disabled selected>Chargement...</option>";
        listeEtapeCombo = [];
        allTransitionControle = [];
        $("#leworkflow").html(listeWorkflow);
        $.ajax({
            url: workflowListeTransitionUrl,
            type: "GET",
            success: function (data) {
                listeWorkflow = "<option value='' disabled selected>Selectionnez le workflow</option>";
                data.forEach(function (w) {
                    listeWorkflow += '<option value="' + w.id + '">' + w.libelle + '</option>'
                });
                $("#leworkflow").html(listeWorkflow);
            }
        });
        formPanel.html(loader);
        modalAjoutTransition.modal('show');
    });

    $('#btnajouterWorkflow').on('click', function (e) {
        e.preventDefault();
        var formPanel = $(this).closest('.row').find('.form-panel');
        formPanel.hide('slow');
        formPanel.html(loader);
        modalAjoutWorkflow.modal('show');
    })

    $('#editworkflow').on('click', function () {

        var libelleworkflow = $("#libellemodworkflow-edit");
        var idsociete = $("#societeworkflow-edit");
        var idtypedemande = $("#typedemandeworkflow-edit");

        if (libelleworkflow.val().trim() === '') {
            libelleworkflow.notify(
                "Veuillez saisir le libelle du workflow",
                {position: "right"},
                "warn"
            );
            libelleworkflow.focus();
        } else {
            if (idsociete.val() === null) {
                idsociete.notify(
                    "Veuillez selectionner la societe",
                    {position: "right"},
                    "warn"
                );
                idsociete.focus();
            } else {
                if (idtypedemande.val() === null) {
                    idtypedemande.notify(
                        "Veuillez selectionner le type de demande",
                        {position: "right"},
                        "warn"
                    );
                    idtypedemande.focus();
                } else {

                    if (addTransition() !== false) {
                        var btn1 = $(this);
                        btn1.button('loading');
                        $.ajax({
                            url: $(this).attr('data-url'),
                            type: "POST",
                            data: {
                                libelle: libelleworkflow.val(),
                                societe: idsociete.val(),
                                typedemande: idtypedemande.val(),
                                transitions: allEtape,
                            },
                            success: function (data) {
                                btn1.button('reset');
                                $.notify("Modification effectuée avec succes !", "success");
                                videmodalworflow();
                                $('#ModalModifierWorkflow').modal('hide');
                                $('#content-workflow-edit').html('');
                            }
                        })
                    }
                }

            }
        }
    });

    function controlecontenutransition() {
        allTransitionControle = [];
        $('.lineTransition').each(function () {
            var line = $(this);
            var libelle = line.find('.libelletransition').val();
            if (libelle !== '') {
                allTransitionControle.push({
                    id: line.find('.etapedebuttransition').val() + line.find('.etapefintransition').val(),
                })
            }
        })
    }

    //Mise en blanc des modales
    function videmodalworflow() {
        $('.libelleetape').each(function () {
            $(this).val('');
        });
        $('.delaietape').each(function () {
            $(this).val('');
        });
        $('.unitetempsetape').each(function () {
            $(this).val(0);
        });
        $('.fintransition').each(function () {
            $(this).val(0);
        });
        $('.typedemandeworkflow').html("<option value='' disabled selected>Selectionnez le type de demande</option>");
        $('.libelleworkflow').val('');
        $('.societeworkflow').each(function () {
            $(this).val(0);
        });
    }

    function videmodaltransition() {
        // $('.leworkflow').each(function () {
        //     $(this).html("<option value='' disabled selected>Selectionnez le type de demande</option>");
        // });
        $('.libelletransition').each(function () {
            $(this).val('');
        });
        $('.etapedebuttransition').each(function () {
            $(this).html("<option value='' disabled selected>Selectionnez le type de demande</option>");
            ;
        });
        $('.etapefintransition').each(function () {
            $(this).html("<option value='' disabled selected>Selectionnez le type de demande</option>");
            ;
        });
    }


    //Ajoute des lignes ( nouvelle div )
    $('#addligneetape').on('click', function () {
        var id = addEtape();
        if (id !== false) {
            var b = parseInt(id) + 1;
            newDiv(b);
            $("#idDiv").val(b);
            $("#addligneetape").val(b);

            $('.deleteLine').unbind('click');
            $('.deleteLine').on('click', function (e) {
                e.preventDefault();
                removeLine($(this));
            });
        }
        ;
    });

    $('#addlignetransition').on('click', function () {
        var id = addTransition();
        if (id !== false) {
            // console.log(id)
            controlecontenutransition();
            var b = parseInt(id) + 1;
            newDivTransition(b, listeEtapeCombo);
            $("#idDiv").val(b);
            $("#addlignetransition").val(b);

            $('.deleteLineTransition').unbind('click');
            $('.deleteLineTransition').on('click', function (e) {
                e.preventDefault();

                removeLineTransition($(this));
            });
        }
        ;
    });


    //Verifie le contenu des champs avant aout d'une ligne
    function addEtape(edit) {
        if (typeof edit !== "undefined") {
            edit = '-edit'
        } else {
            edit = ''
        }
        var idligne = $("#addligneetape" + edit).val();
        var libelle = $("#libelleetape" + edit + idligne);
        var service = $("#serviceworkflow" + edit + idligne);
        var delai = $("#delaietape" + edit + idligne);
        var unite = $("#unitetempsetape" + edit + idligne);
        if (libelle.val() === '') {
            libelle.notify(
                "Veuillez saisir le libelle de l'etape",
                {position: "right"},
                "warn"
            );
            libelle.focus();
            return false;
        } else {
            if (service.val() === null) {
                service.notify(
                    "Veuillez selectionner le service",
                    {position: "right"},
                    "warn"
                );
                service.focus();
                return false;
            } else {
                if (delai.val() === null) {
                    delai.notify(
                        "Veuillez saisir le delai",
                        {position: "right"},
                        "warn"
                    );
                    delai.focus();
                    return false;
                } else {
                    if (unite.val() === null) {
                        unite.notify(
                            "Veuillez saisir le unite",
                            {position: "right"},
                            "warn"
                        );
                        unite.focus();
                        return false;
                    } else {
                        return idligne;
                    }
                    ;
                }
                ;
            }
            ;
        }
        ;
    };

    function addTransition(edit) {
        if (typeof edit !== "undefined") {
            edit = '-edit'
        } else {
            edit = ''
        }
        var idligne = $("#addlignetransition" + edit).val();
        var libelle = $("#libelletransition" + edit + idligne);
        var etapedebut = $("#etapedebuttransition" + edit + idligne);
        var etapefin = $("#etapefintransition" + edit + idligne);
        var workflow = $("#leworkflow");
        // console.log(doublons);
        // var doublons = false;
        // allTransitionControle.forEach(function (value) {
        //     // console.log(value['id'],etapedebut.val()+etapefin.val())
        //     if (value['id'] === etapedebut.val()+etapefin.val()) {
        //         doublons = true;
        //     }
        // });
        //
        // // console.log(doublons);
        // if (doublons == false){
        if (workflow.val() === '') {
            workflow.notify(
                "Veuillez selectionner un workflow",
                {position: "right"},
                "warn"
            );
            workflow.focus();
            return false
        } else {
            if (libelle.val() === '') {
                libelle.notify(
                    "Veuillez saisir le libelle de la transition",
                    {position: "right"},
                    "warn"
                );
                libelle.focus();
                return false
            } else {
                if (etapedebut.val() === null) {
                    etapedebut.notify(
                        "Veuillez selectionner l'etape debut",
                        {position: "right"},
                        "warn"
                    );
                    etapedebut.focus();
                    return false
                } else {
                    if (etapefin.val() === null) {
                        etapefin.notify(
                            "Veuillez selectionner l'etape fin",
                            {position: "right"},
                            "warn"
                        );
                        etapefin.focus();
                        return false
                    } else {
                        return idligne;
                    }
                }
            }
        }

        // }else{
        //     $.notify("Une transition existe déjà avec cette meme étape début et fin !",
        //         "warn"
        //     );
        //     return false;
    }

    // }


    //Bouton supprimer
    function removeLine(nodeElement) {
        var tr = nodeElement.closest('.line');
        var id = tr.attr('id');
        var tab = [];
        allEtape.forEach(function (value) {
            if (value['id'] !== parseInt(id)) {
                tab.push(value)
            }
        });
        allDelete.push(id);
        allEtape = tab;
        // console.log(allDelete);
        tr.remove()
    }

    function removeLineTransition(nodeElement) {
        var tr = nodeElement.closest('.lineTransition');
        // alert('ok1');
        var id = tr.attr('id');
        var tab = [];
        allTransition.forEach(function (value) {
            if (value['id'] !== parseInt(id)) {
                tab.push(value)
            }
        });
        allDelete.push(id);
        allTransition = tab;
        // console.log(allDelete);
        tr.remove()
    }

    // function removeLineTableTransition(nodeElement) {
    //     // var tr = nodeElement.closest('.line-table-transition');
    //     var tr = nodeElement.parent().parent();
    //     // console.log(nodeElement);
    //     tr.remove();
    // }


    //Bouton enregistrer
    $('#addworkflow').on('click', function () {

        allEtape = [];
        var libelleworkflow = $("#libellemodworkflow");
        var idsociete = $("#societeworkflow");
        var idtypedemande = $("#typedemandeworkflow");

        if (libelleworkflow.val().trim() === '') {
            libelleworkflow.notify(
                "Veuillez saisir le libelle du workflow",
                {position: "right"},
                "warn"
            );
            libelleworkflow.focus();
        } else {
            if (idsociete.val() === null) {
                idsociete.notify(
                    "Veuillez selectionner la societe",
                    {position: "right"},
                    "warn"
                );
                idsociete.focus();
            } else {
                if (idtypedemande.val() === null) {
                    idtypedemande.notify(
                        "Veuillez selectionner le type de demande",
                        {position: "right"},
                        "warn"
                    );
                    idtypedemande.focus();
                } else {

                    //if (addTransition() !== false) {
                    var btn = $(this);
                    btn.button('loading');
                    var totalduree = fillEtapes();
                    var dureetypedemande = idtypedemande.find('option:selected').attr('data-duree');
                    if (totalduree > parseInt(dureetypedemande)) {
                        btn.button('reset');
                        $.notify("La durée des etapes ne peuvent dépasbtnajouterTransitionser celle du type de demande qui est de " + dureetypedemande + " jours", "warning");
                    } else {
                        $.ajax({
                            url: workflowAddUrl,
                            type: "POST",
                            data: {
                                libelle: libelleworkflow.val(),
                                societe: idsociete.val(),
                                typedemande: idtypedemande.val(),
                                etapes: allEtape,
                            },
                            success: function (data) {
                                btn.button('reset');
                                // $.notify("Enregistrement effectué avec succes !", "success");
                                swal({
                                    title: "Alert!",
                                    icon: "success",
                                    button: false,
                                    text: "Enregistrement effectué avec succes",
                                    timer: 1500
                                });
                                videmodalworflow();
                                $('#ModalAjouterWorkflow').modal('hide');

                                //Supprimer les ligne
                                $('.line').each(function () {
                                    var line = $(this);
                                    // var tr = line.parentElement();
                                    // console.log(line.attr('id'));
                                    if (line.attr('id') !== '1') {
                                        line.remove();
                                    }
                                })

                                // var id = tr.attr('id');
                                // allTransition.forEach(function (value) {
                                //     if (value['id'] !== parseInt(id)) {
                                //         tab.push(value)
                                //     }
                                // });
                                // allDelete.push(id);
                                // allTransition = tab;
                                // // console.log(allDelete);
                                // tr.remove()
                            }
                        });
                    }


                }
            }
        }
    });

    $('#addtransitions').on('click', function () {

        allTransition = [];
        var workflow = $("#leworkflow");
        // alert('')

        if (workflow.val() === '') {
            workflow.notify(
                "Veuillez selectionner le workflow",
                {position: "right"},
                "warn"
            );
            workflow.focus();
        } else {
            //if (addTransition() !== false) {
            var btn = $(this);
            btn.button('loading');

            controlecontenutransition();

            var idligne = $("#addlignetransition").val();
            var libelle = $("#libelletransition" + idligne);
            var etapedebut = $("#etapedebuttransition" + idligne);
            var etapefin = $("#etapefintransition" + idligne);
            var workflow = $("#leworkflow");
            var doublons = false;

            var find = 0;
            allTransitionControle.forEach(function (value) {
                allTransitionControle.forEach(function (value1) {
                    // console.log(value['id'],value1['id'])
                    if (value['id'] === value1['id']) {
                        find = find + 1;
                        if (find === 2) {
                            doublons = true;
                            find = 0;
                        }
                    }
                });
                find = 0;
            });

            if (doublons === false) {
                fillTransitions();
                // console.log(allEtape);
                $.ajax({
                    url: transitionworkflowAddUrl,
                    type: "POST",
                    data: {
                        workflow: workflow.val(),
                        transitions: allTransition,
                    },
                    success: function (data) {
                        btn.button('reset');
                        swal({
                            title: "Alert!",
                            icon: "success",
                            button: false,
                            text: "Enregistrement effectué avec succes",
                            timer: 1500
                        });
                        videmodaltransition();
                        $('#ModalAjouterTransition').modal('hide');

                        //Supprimer les ligne
                        $('.lineTransition').each(function () {
                            var line = $(this);
                            if (line.attr('id') !== '1') {
                                line.remove();
                            }
                        })
                    }
                })
            } else {
                btn.button('reset');
                $.notify("Une transition existe déjà avec cette meme étape début et fin !",
                    "warn"
                );
            }


        }
    });


    //Selection de combo
    $("#societeworkflow").on('change', function (e) {
        var sous = "<option value='' disabled selected>Chargement...</option>";
        $("#typedemandeworkflow").html(sous);
        $.ajax({
            url: typedemandeListUrl.replace('lecode', $(this).val()),
            type: "GET",
            success: function (data) {
                // console.log(data);

                sous = "<option value='' disabled selected>Selectionnez le type de demande</option>";
                data.forEach(function (g) {
                    var dureejour = 0;
                    if (g.unitetemp === 'mois') {
                        // console.log(line.find('.delaietape').val()*720);
                        dureejour += parseInt(g.delai * 720);
                    }
                    if (g.unitetemp === 'jour') {
                        // console.log(line.find('.delaietape').val()*24);
                        dureejour += parseInt(g.delai * 24);
                    }
                    if (g.unitetemp === 'heure') {
                        // console.log(line.find('.delaietape').val());
                        dureejour += parseInt(g.delai);
                    }

                    sous += '<option data-duree="' + dureejour + '" value="' + g.id + '">' + g.libelletypedemande + '</option>'
                });

                $("#typedemandeworkflow").html(sous);

            }
        });
    });

    $("#leworkflow").on('change', function (e) {
        listeEtapeCombo = "<option value='' disabled selected>Chargement...</option>";
        // allListeEtapeCombo = [];
        $(".etapedebuttransition").html(listeEtapeCombo);
        $(".etapefintransition").html(listeEtapeCombo);
        $.ajax({
            url: etapeworkflowListUrl.replace('lecode', $(this).val()),
            type: "GET",
            success: function (data) {
                // console.log(data);
                // allListeEtapeCombo = data;

                listeEtapeCombo = "<option value='' disabled selected>Selectionnez l'etape</option>";
                data.forEach(function (g) {
                    listeEtapeCombo += '<option value="' + g.id + '">' + g.libelle + '</option>'
                });
                $(".etapedebuttransition").html(listeEtapeCombo);
                $(".etapefintransition").html(listeEtapeCombo);

            }
        });
    });


    //Remplir les tableaux avant d'envoyer dans le controleur
    function fillTransitions() {

        allTransition = [];
        $('.lineTransition').each(function () {
            var line = $(this);
            var libelle = line.find('.libelletransition').val();
            if (libelle !== '') {
                allTransition.push({
                    id: line.attr('id'),
                    libelle: libelle,
                    debut: line.find('.etapedebuttransition').val(),
                    fin: line.find('.etapefintransition').val()
                });
            }
        })
    }

    function fillEtapes() {

        allEtape = [];
        var dureeEtapeHeure = 0;
        $('.line').each(function () {
            var line = $(this);
            var libelle = line.find('.libelleetape').val();
            if (line.find('.unitetempsetape').val() === 'mois') {
                // console.log(line.find('.delaietape').val()*720);
                dureeEtapeHeure += parseInt(line.find('.delaietape').val() * 720);
            }
            if (line.find('.unitetempsetape').val() === 'jour') {
                // console.log(line.find('.delaietape').val()*24);
                dureeEtapeHeure += parseInt(line.find('.delaietape').val() * 24);
            }
            if (line.find('.unitetempsetape').val() === 'heure') {
                // console.log(line.find('.delaietape').val());
                dureeEtapeHeure += parseInt(line.find('.delaietape').val());
            }

            if (libelle !== '') {
                allEtape.push({
                    id: line.attr('id'),
                    libelle: libelle,
                    service: line.find('.serviceworkflow').val(),
                    delai: line.find('.delaietape').val(),
                    unite: line.find('.unitetempsetape').val(),
                });
            }
            ;
        });

        // alert(dureeEtapeHeure);
        return dureeEtapeHeure;
    };


});