$(function () {
    var etapeListHidden = true;
    var workFlowListHidden = true;
    var modalAjoutEtape = $('#ModalAjouterEtape');
    var modalAjoutWorkflow = $('#ModalAjouterWorkflow');
    var loader = '<div class="loader text-center mt-3 mb-3"><i class="fa fa-refresh fa-spin fa-3x"></i></div>'
    var allTransition = [];
    var allDelete = [];
    $('#title-list-etape').on('click', function (e) {
        e.preventDefault();
        etapeListHidden = showBody($(this), etapeListHidden, 'etape')
    });
    $('#title-list-workflow').on('click', function (e) {
        e.preventDefault();
        workFlowListHidden = showBody($(this), workFlowListHidden, 'workflow')
    });

    function videmodalEtape() {
        $("#libellemodetape").val('');
        $("#delaimodetape").val('');
    }

    function videmodalworflow() {
        // $("#libelletransition").val('');
        // $("#libellemodworkflow").val('');
        // $("#etapedebut").val(0);
        // $("#societeworkflow").val(0);
        // $("#etapefin").val(0);
        // $("#typedemandeworkflow").val(0);
        $('.libelletransition').each(function () {
            $(this).val('');
        });
        $('.libelleworkflow').each(function () {
            $(this).val('');
        });
        $('.debuttransition').each(function () {
            $(this).val(0);
        });
        $('.fintransition').each(function () {
            $(this).val(0);
        });
        $('.typedemandeworkflow').each(function () {
            $(this).val(0);
        });
        $('.societeworkflow').each(function () {
            $(this).val(0);
        });
    }


    function showBody(element, list, namelist) {
        var url = element.attr('data-url');
        var formPanel = element.closest('.row').find('.form-panel');
        if (list) {
            formPanel.show('slow');
            list = false;
            $.ajax({
                type: "GET",
                url: url,
                success: function (data) {
                    formPanel.html(data);

                    if (namelist === 'workflow') {
                        $('.edit-workflow').on('click', function (e) {
                            var btnworkflow = $(this);
                            btnworkflow.button('loading');
                            e.preventDefault();
                            $.ajax({
                                url: btnworkflow.attr('data-href'),
                                type: "GET",
                                success: function (data) {
                                    btnworkflow.button('reset');

                                    $('#content-workflow-edit').html(data);
                                    allTransition = JSON.parse($('#allTransitions').val())
                                    $('#ModalModifierWorkflow').modal('show');
                                    $('.removeLine-edit').on('click', function (e) {
                                        e.preventDefault();
                                        removeLine($(this));
                                    });

                                    $('#addlignetransition-edit').on('click', function () {
                                        var id = addTransition('edit');
                                        if (id !== false) {
                                            var b = parseInt(id) + 1;
                                            newDiv(b, 'edit');
                                            // $("#idDiv").val(b);
                                            $("#addlignetransition-edit").val(b);
                                            $("#removelignetransition-edit").val(b);
                                            // console.log(allTransition);

                                            $('.removeLine-edit').unbind('click');
                                            $('.removeLine-edit').on('click', function (e) {
                                                // console.log('ok')

                                                e.preventDefault();
                                                removeLine($(this));
                                            });
                                        }
                                        ;
                                    });
                                    $('#editworkflow').on('click', function () {
                                        var btnworkflow = $(this);
                                        allTransition = [];
                                        e.preventDefault();
                                        btnworkflow.button('loading');
                                        var libelleworkflow = $("#libellemodworkflow-edit");
                                        var idsociete = $("#societeworkflow-edit");
                                        var idtypedemande = $("#typedemandeworkflow-edit");

                                        if (libelleworkflow.val().trim() === '') {
                                            libelleworkflow.notify(
                                                "Veuillez saisir le libelle du workflow",
                                                {position: "right"},
                                                "warn"
                                            );
                                            libelleworkflow.focus();
                                        } else {
                                            if (idsociete.val() === null) {
                                                idsociete.notify(
                                                    "Veuillez selectionner la societe",
                                                    {position: "right"},
                                                    "warn"
                                                );
                                                idsociete.focus();
                                            } else {
                                                if (idtypedemande.val() === null) {
                                                    idtypedemande.notify(
                                                        "Veuillez selectionner le type de demande",
                                                        {position: "right"},
                                                        "warn"
                                                    );
                                                    idtypedemande.focus();
                                                } else {

                                                    // if (addTransition() !== false) {
                                                        fillTransitions();
                                                        console.log(allDelete);
                                                        $.ajax({
                                                            url: $(this).attr('data-url'),
                                                            type: "POST",
                                                            data: {
                                                                libelle: libelleworkflow.val(),
                                                                societe: idsociete.val(),
                                                                typedemande: idtypedemande.val(),
                                                                transitions: allTransition,
                                                                transitionsdelete: allDelete,
                                                            },
                                                            success: function (data) {
                                                                btnworkflow.button('reset');
                                                                $.notify("Modification effectuée avec succes !", "success");
                                                                videmodalworflow();
                                                                $('#ModalModifierWorkflow').modal('hide');
                                                                $('#content-workflow-edit').html('');
                                                            }
                                                        })
                                                    // }
                                                }

                                            }
                                        }
                                        // else {
                                        //     $.notify("Veuillez saisir les transitions du workflow !", "error");
                                        // }
                                    });


                                }
                            })
                        })
                        $('.show-workflow').on('click', function (e) {
                            var btnworkflow = $(this);
                            btnworkflow.button('loading');
                            e.preventDefault();

                            $.ajax({
                                url: btnworkflow.attr('data-href'),
                                type: "GET",
                                success: function (data) {
                                    btnworkflow.button('reset');
                                    $('#content-workflow-apercu').html(data);
                                    $('#ModalApercuWorkflow').modal('show');
                                }
                            })
                        })
                    }

                    if (namelist === 'etape') {
                        $('.edit-etape').on('click', function (e) {
                            var btnetape = $(this);
                            btnetape.button('loading');
                            e.preventDefault();
                            $.ajax({
                                url: btnetape.attr('data-href'),
                                type: "GET",
                                success: function (data) {
                                    btnetape.button('reset');
                                    $('#content-etape-edit').html(data);
                                    $('#ModalModifierEtape').modal('show');
                                    $('#editetape').on('click', function () {
                                        var btn1 = $(this);
                                        btn1.button('loading');
                                        var libelle = $("#libellemodetape-edit");
                                        var service = $("#servicemodetape-edit");
                                        var delai = $("#delaimodetape-edit");

                                        if (libelle.val().trim() === '') {
                                            libelle.notify(
                                                "Veuillez saisir le libelle de l'etape",
                                                {position: "right"},
                                                "warn"
                                            );
                                            libelle.focus();
                                        } else {
                                            if (service.val() === 0) {
                                                service.notify(
                                                    "Veuillez selectionner le service",
                                                    {position: "right"},
                                                    "warn"
                                                );
                                                service.focus();
                                            } else {
                                                if (delai.val().trim() === null) {
                                                    delai.notify(
                                                        "Veuillez saisir le delai",
                                                        {position: "right"},
                                                        "warn"
                                                    );
                                                    delai.focus();
                                                } else {
                                                    var etape = {
                                                        libelleEtape: libelle.val(),
                                                        delaiEtape: delai.val(),
                                                        service: service.val(),
                                                    };
                                                    $.ajax({
                                                        url: $(this).attr('data-url'),
                                                        type: "POST",
                                                        data: {
                                                            tab: etape,
                                                        },
                                                        success: function (data) {
                                                            btn1.button('reset');
                                                            $.notify("Modification effectuée avec succes !", "success");
                                                            $('#ModalModifierEtape').modal('hide');
                                                            $('#content-etape-edit').html('');
                                                        }
                                                    })
                                                }

                                            }
                                        }
                                        // else {
                                        //     $.notify("Veuillez saisir les transitions du workflow !", "error");
                                        // }
                                    });

                                    // $('.removeLine-edit').on('click', function (e) {
                                    //     e.preventDefault();
                                    //     removeLine($(this));
                                    // });

                                    // $('#addlignetransition-edit').on('click', function () {
                                    //     var id = addTransition('edit');
                                    //     if (id !== false) {
                                    //         var b = parseInt(id) + 1;
                                    //         newDiv(b, 'edit');
                                    //         // $("#idDiv").val(b);
                                    //         $("#addlignetransition-edit").val(b);
                                    //         $("#removelignetransition-edit").val(b);
                                    //
                                    //         $('.removeLine-edit').unbind('click');
                                    //         $('.removeLine-edit').on('click', function (e) {
                                    //             // console.log('ok')
                                    //
                                    //             e.preventDefault();
                                    //             removeLine($(this));
                                    //         });
                                    //     }
                                    //     ;
                                    // });


                                }
                            })
                        })
                    }
                }
            })
        } else {
            list = true;
            formPanel.hide('slow')
        }
        return list;
    }

    $('#btnajouterEtape').on('click', function (e) {
        e.preventDefault();
        var formPanel = $(this).closest('.row').find('.form-panel');
        formPanel.hide('slow');
        formPanel.html(loader);
        modalAjoutEtape.modal('show');
    })

    $('#btnajouterWorkflow').on('click', function (e) {
        e.preventDefault();
        var formPanel = $(this).closest('.row').find('.form-panel');
        formPanel.hide('slow');
        formPanel.html(loader);
        modalAjoutWorkflow.modal('show');
    })


    // function verifienullitéchamp(idchamp){
    //     if ($("#"+idchamp+"").val() === "") {
    //         $("#"+idchamp+"").notify(
    //             "Veuillez renseigner",
    //             {position: "right"},
    //             "warn"
    //         );
    //         document.getElementById(idchamp).focus()
    //
    //     }
    // }

    $('#addetape').on('click', function (e) {
        var libelle = $("#libellemodetape").val();
        var delai = $("#delaimodetape").val();

        var url = $(this).attr('data-url')
        if (libelle === "") {
            $("#libellemodetape").notify(
                "Veuillez saisir le libelle de l'étape",
                {position: "right"},
                "warn"
            );
            document.getElementById(libellemodEtape).focus()

        } else {
            if (delai === "") {
                $("#delaimodetape").notify(
                    "Veuillez saisir le delai de l'étape",
                    {position: "right"},
                    "warn"
                );
                document.getElementById(delaimodEtape).focus()

            } else {
                var service = $("#serviceetape").val();
                // console.log(service);
                var etape = {
                    libelleEtape: libelle,
                    delaiEtape: delai,
                    service: service,
                };
                // console.log(etape)
                var btnaddetape = $('#addetape');
                btnaddetape.button('loading');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        tab: etape,
                    },
                    success: function (id_etape) {
                        btnaddetape.button('reset');
                        if (id_etape == 0) {
                            $.notify("Erreur enregistrement !", "error");

                        } else {
                            videmodalEtape();
                            modalAjoutEtape.modal('hide');
                            $.notify("Enregistrement effectué avec succes !", "success");
                        }

                    }
                });
            }
        }
    });

    $('#editworkflow').on('click', function () {
        var btn1 = $(this);
        btn1.button('loading');
        var libelleworkflow = $("#libellemodworkflow-edit");
        var idsociete = $("#societeworkflow-edit");
        var idtypedemande = $("#typedemandeworkflow-edit");

        if (libelleworkflow.val().trim() === '') {
            libelleworkflow.notify(
                "Veuillez saisir le libelle du workflow",
                {position: "right"},
                "warn"
            );
            libelleworkflow.focus();
        } else {
            if (idsociete.val() === null) {
                idsociete.notify(
                    "Veuillez selectionner la societe",
                    {position: "right"},
                    "warn"
                );
                idsociete.focus();
            } else {
                if (idtypedemande.val() === null) {
                    idtypedemande.notify(
                        "Veuillez selectionner le type de demande",
                        {position: "right"},
                        "warn"
                    );
                    idtypedemande.focus();
                } else {

                    if (addTransition() !== false) {
                        // console.log('Ok')
                        $.ajax({
                            url: $(this).attr('data-url'),
                            type: "POST",
                            data: {
                                libelle: libelleworkflow.val(),
                                societe: idsociete.val(),
                                typedemande: idtypedemande.val(),
                                transitions: allTransition,
                            },
                            success: function (data) {
                                btn1.button('reset');
                                $.notify("Modification effectuée avec succes !", "success");
                                videmodalworflow();
                                $('#ModalModifierWorkflow').modal('hide');
                                $('#content-workflow-edit').html('');
                            }
                        })
                    }
                }

            }
        }
        // else {
        //     $.notify("Veuillez saisir les transitions du workflow !", "error");
        // }
    });


    $('#addlignetransition').on('click', function () {
        var id = addTransition();
        if (id !== false) {
            var b = parseInt(id) + 1;
            newDiv(b);
            $("#idDiv").val(b);
            $("#addlignetransition").val(b);
            $("#removelignetransition").val(b);

            $('.deleteLine').unbind('click');
            $('.deleteLine').on('click', function (e) {
                e.preventDefault();
                removeLine($(this));
            });
        }
        ;
    });


    function addTransition(edit) {
        if (typeof edit !== "undefined") {
            edit = '-edit'
        } else {
            edit = ''
        }
        var idligne = $("#addlignetransition" + edit).val();
        var libelle = $("#libelletransition" + edit + idligne);
        var debut = $("#etapedebut" + edit + idligne);
        var fin = $("#etapefin" + edit + idligne);
        if (edit === '-edit') {
            if (libelle.val() === '') {
                libelle.notify(
                    "Veuillez saisir le libelle de la transition",
                    {position: "right"},
                    "warn"
                );
                libelle.focus();
                return false
            } else {
                if (debut.val() === null) {
                    debut.notify(
                        "Veuillez selectionner l'étape",
                        {position: "right"},
                        "warn"
                    );
                    debut.focus();
                    return false
                } else {
                    if (fin.val() === null) {
                        fin.notify(
                            "Veuillez selectionner l'étape",
                            {position: "right"},
                            "warn"
                        );
                        fin.focus();
                        return false
                    } else {
                        return idligne;
                    }
                }
            }
        } else {
            return idligne;
        }
    }

    /*$('#removelignetransition').on('click', function () {
        var numdernierediv = $("#removelignetransition").val();
        var elem = document.getElementById(numdernierediv);
        // console.log(numdernierediv)
        elem.parentNode.removeChild(elem);
        $("#addlignetransition").val(numdernierediv - 1);
        $("#removelignetransition").val(numdernierediv - 1);
    });

    $('.removeLineTransition').on('click', function (e) {
        e.preventDefault()
        var numdernierediv = $(this).val();
        var elem = document.getElementById(numdernierediv);
        // console.log(numdernierediv)
        elem.parentNode.removeChild(elem);
        $("#addlignetransition").val(numdernierediv - 1);
        $("#removelignetransition").val(numdernierediv - 1);
    })*/

    // $('.deleteLine').on('click', function (e) {
    //     e.preventDefault();
    //     removeLine($(this))
    // });

    function removeLine(nodeElement) {
        var tr = nodeElement.closest('.line');
        var id = tr.attr('id');
        var tab = [];
        allTransition.forEach(function (value) {
            if (value['id'] !== parseInt(id)) {
                tab.push(value)
            }
        });
        allDelete.push(id);
        allTransition = tab;
        // console.log(allDelete);
        tr.remove()
    }


    $('#addworkflow').on('click', function () {
        var btn = $(this);
        allTransition = [];
        btn.button('loading');
        var libelleworkflow = $("#libellemodworkflow");
        var idsociete = $("#societeworkflow");
        var idtypedemande = $("#typedemandeworkflow");

        if (libelleworkflow.val().trim() === '') {
            libelleworkflow.notify(
                "Veuillez saisir le libelle du workflow",
                {position: "right"},
                "warn"
            );
            libelleworkflow.focus();
        } else {
            if (idsociete.val() === null) {
                idsociete.notify(
                    "Veuillez selectionner la societe",
                    {position: "right"},
                    "warn"
                );
                idsociete.focus();
            } else {
                if (idtypedemande.val() === null) {
                    idtypedemande.notify(
                        "Veuillez selectionner le type de demande",
                        {position: "right"},
                        "warn"
                    );
                    idtypedemande.focus();
                } else {

                    //if (addTransition() !== false) {

                        fillTransitions();

                        $.ajax({
                            url: workflowAddUrl,
                            type: "POST",
                            data: {
                                libelle: libelleworkflow.val(),
                                societe: idsociete.val(),
                                typedemande: idtypedemande.val(),
                                transitions: allTransition,
                            },
                            success: function (data) {
                                btn.button('reset')
                                $.notify("Enregistrement effectué avec succes !", "success");
                                videmodalworflow();
                                $('#ModalAjouterWorkflow').modal('hide');
                            }
                        })
                    //}
                }
            }
        }
    });


    /*$('#addworkflow').on('click', function () {
        var libelleworkflow = $("#libellemodworkflow").val();
        // var idsociete = $("#societe").val();
        // var idtypedemande = $("#typedemande").val();
        var idligne = $('#addlignetransition').val();

        var idlibelle = "libelletransition" + idligne;

        var contenu = $("#"+idlibelle+"").val();

        if (contenu === "") {
            $("#"+idlibelle+"").notify(
                "Veuillez saisir le libelle de la transition",
                {position: "right"},
                "warn"
            );
            document.getElementById(idlibelle).focus()

        } else {
        if (libelleworkflow === "") {
            $("#libellemodworkflow").notify(
                "Veuillez saisir le workflow",
                {position: "right"},
                "warn"
            );
            document.getElementById(libellemodworkflow).focus()

        } else {
            // var tabworkflow = {
            //     libelleWorkflow:libelleworkflow,
            //     idSociete:idsociete,
            //     idTypeDemande:idtypedemande,
            // };


            var tabtransition = [];
            var numdernierediv = $("#removelignetransition").val();

            for (var i = 1; i <= numdernierediv; i++) {
                if(!!document.querySelector("#libelletransition" + i)) {
                    var libelle = $("#libelletransition" + i).val();
                    var etapedebut = $("#etapedebut" + i).val();
                    var etapefin = $("#etapefin" + i).val();
                    var idworkflow = $("#workflow").val();
                    var transition = {
                        etapeDebut: etapedebut,
                        etapeFin: etapefin,
                        workflow: idworkflow,
                        libelleTransition: libelle,
                    };
                    tabtransition.push(transition);
                }
            }
            // console.log(tabtransition)
            // console.log(tabworkflow, JSON.stringify(tabtransition));
            ;
            $.ajax({
                url: workflowAddUrl || "",
                type: 'POST',
                dataType: 'json',
                data: {
                    // tabworkflow : tabworkflow,
                    tabtransition: JSON.stringify(tabtransition),
                },
                success: function (data) {
                    // window.location.replace("http://localhost/gecd/gecd_client/web/app_dev.php/transition/");

                }


            });

        }
        }
    });*/

    function fillTransitions() {
        $('.line').each(function() {
            var line = $(this)
            var libelle = line.find('.libelletransition').val()
            if(libelle !== ''){
                allTransition.push({
                    id: line.attr('id'),
                    libelle: libelle,
                    debut: line.find('.debuttransition').val(),
                    fin: line.find('.fintransition').val()
                });
            }
        })
        // console.log(allTransition)
    }


});