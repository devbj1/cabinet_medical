var ModalTraitementDemande = $("#ModalTraitementDemande");
var iddemande = '';
var commentsArray = [];
var btntraite = '';

// $('#files-upload').on('click',function () {
//     console.log('1')
// })

// function readURL(input) {
//     if (input.files && input.files[0]) {
//         var reader = new FileReader();
//
//         reader.onload = function (e) {
//             // $('#blah').attr('src', e.target.result);
//             console.log(e.target.result)
//         }
//
//         reader.readAsDataURL(input.files[0]);
//     }
// }

// $('#files-upload').change(function(){
//     console.log(ok)
//
//     $.ajax({
//         url: addpiecejointeUrl.replace('lecode', iddemande),
//         type: "POST",
//         success: function (data) {
//             // $("#blockpiecejointe").text('');
//             console.log(data)
//             data.forEach(function (g) {
//                 newDivImage(g.id, g.libelle, g.chemin)
//             })
//         },
//         error: function (error) {
//
//         }
//     });
//     // readURL(this);
// });

// function readURL(input) {
//     if (input.files && input.files[0]) {
//         var reader = new FileReader();
//         // console.log('ok')
//         // reader.onload = function (e) {
//         //     $('#blah').attr('src', e.target.result);
//         // }
//
//         var file = input.files;
//         $.ajax({
//             url: addpiecejointeUrl.replace('lecode', iddemande),
//             type: "POST",
//             data: {
//                 file: file
//             },
//             success: function (data) {
//                 // $("#blockpiecejointe").text('');
//                 console.log(data)
//                 data.forEach(function (g) {
//                     newDivImage(g.id, g.libelle, g.chemin)
//                 })
//             },
//             error: function (error) {
//
//             }
//         });
//
//         reader.readAsDataURL(input.files[0]);
//     }
// }

$('#upload').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);
    // readURL(this);
    // console.log('ok')
    var file_data = $('#file-upload').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    // console.log(file_data);
    btn.button('loading');
    $.ajax({
        url: addpiecejointeUrl.replace('lecode', iddemande),
        type: "POST",
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        success: function (data)
        {
            btn.button('reset');
            var  g  = JSON.parse(data);
            newDivImage(g.id, g.libelle, g.chemin)
            // console.log(ok.libelle);
            // data.forEach(function (g) {
            //
            // })
        },
        error: function (error) {

        }
    });
});


//Datatable

var table = $("#tablelistedemandeutilisateur").DataTable({
    'order': [[0, 'desc']],
    "language": {
        "sProcessing": "Traitement en cours ...",
        "sLengthMenu": "Afficher _MENU_ lignes",
        "sZeroRecords": "Aucun résultat trouvé",
        "sEmptyTable": "Aucune donnée disponible",
        "sInfo": "Lignes _START_ à _END_ sur _TOTAL_",
        "sInfoEmpty": "Aucune ligne affichée",
        "sInfoFiltered": "(Filtrer un maximum de_MAX_)",
        "sInfoPostFix": "",
        "sSearch": "Chercher:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Chargement...",
        "oPaginate": {
            "sFirst": "Premier", "sLast": "Dernier", "sNext": "Suivant", "sPrevious": "Précédent"
        },
        "oAria": {
            "sSortAscending": ": Trier par ordre croissant", "sSortDescending": ": Trier par ordre décroissant"
        }
    }

});

$("#tablelistedemandeutilisateur_length").addClass('form-control');
$("#tablelistedemandeutilisateur_filter").find('input').addClass('form-control');

$("#tablelistedemandeutilisateur").on('click', '.traiter-demande', function (e) {
    e.preventDefault();
    btntraite = $(this);
    iddemande = btntraite.attr('id');
    var commentsArray = [];
    var usersArray = [];
    btntraite.button('loading');
    $.ajax({
        url: btntraite.attr('data-href'),
        type: "POST",
        data: iddemande,
        success: function (data) {
            btntraite.button('reset');
            $("#iddemande").attr("data-iddemande", data[0].id);
            $("#referencedemande").text(data[0].reference);
            $("#datereceptiondemande").text(data[0].datereception);
            $("#sourcedemande").text(data[0].source);
            $("#societedemande").text(data[0].societe);
            $("#typedemandedemande").text(data[0].typedemande);
            $("#prospectdemande").text(data[0].prospect);
            $("#prioritedemande").text(data[0].priorite);
            $("#observationdemande").text(data[0].observation);

            if(data[0].ordreprocedureexceptionnelle === '1'){
                $("#btnrejet").addClass("disabled");
            }else{
                if (data[0].ordreprocedureexceptionnelle === ''){
                    if(data[0].transitionencours === 'reception'){
                        $("#btnrejet").addClass("disabled");
                    }else{
                        $("#btnrejet").removeClass("disabled");
                    }
                }else{
                    $("#btnrejet").removeClass("disabled");
                }
            }



            //Liste des pieces jointes
            $.ajax({
                url: listepiecejointedemandeUrl.replace('lecode', iddemande),
                type: "POST",
                success: function (data) {
                    $("#blockpiecejointe").text('');
                    // console.log(data)
                    data.forEach(function (g) {
                        newDivImage(g.id, g.libelle, g.chemin)
                    })
                },
                error: function (error) {

                }
            });


            // Liste des commentaires
            $.ajax({
                url: listecommentairedemandeUrl.replace('lecode', iddemande),
                type: "POST",
                data: iddemande,
                success: function (data) {
                    commentsArray = data;
                    var saveComment = function (dataCommentaire) {
                        $.ajax({
                            url: addcommentaireUrl.replace('lecode', iddemande),
                            type: "POST",
                            dataType: "json",
                            data: dataCommentaire,
                            success: function (data) {
                            },
                            error: function (error) {

                            }
                        });
                        return dataCommentaire;
                    };
                    // var saveComment = function(data) {
                    //     console.log('pl')
                    //     // Convert pings to human readable format
                    //     $(Object.keys(data.pings)).each(function(index, userId) {
                    //         var fullname = data.pings[userId];
                    //         var pingText = '@' + fullname;
                    //         data.content = data.content.replace(new RegExp('@' + userId, 'g'), pingText);
                    //     });
                    //
                    //     return data;
                    // }


                    var updateComment = function (dataCommentaire) {
                        $.ajax({
                            url: updatecommentaireUrl.replace('lecode', iddemande),
                            type: "POST",
                            dataType: "json",
                            data: dataCommentaire,
                            success: function (data) {
                            },
                            error: function (error) {

                            }
                        });
                        return dataCommentaire;
                    };

                    $.ajax({
                        url: listutilisateurUrl,
                        type: "GET",
                        success: function (data) {
                            usersArray = data;
                            // console.log(usersArray);
                            // var usersArray = [{cheminPhotoProfil
                            //     id: 1,
                            //     fullname: $("#employerconnecter").text(),
                            //     email: "current.user@viima.com",
                            //     profile_picture_url: profilpictureUrl
                            // },
                            // ];
                            // console.log(usersArray);

                            $('#comments-container').comments({
                                // User
                                profilePictureURL: profilpictureUrl,
                                currentUserIsAdmin: false,
                                currentUserId: 14,

                                // Font awesome icon overrides
                                spinnerIconURL: '',
                                upvoteIconURL: '',
                                replyIconURL: replyiconUrl,
                                uploadIconURL: '',
                                attachmentIconURL: '',
                                fileIconURL: '',
                                noCommentsIconURL: '',

                                // Strings to be formatted (for example localization)
                                textareaPlaceholderText: 'Ajouter un commentaire',
                                newestText: 'Plus récent',
                                oldestText: 'Le plus ancien',
                                popularText: 'Populaire',
                                attachmentsText: ' Les pièces jointes',
                                sendText: 'Envoyer',
                                replyText: 'Repondre',
                                editText: 'Modifier',
                                editedText: 'Modifié',
                                youText: 'Vous',
                                saveText: 'Sauvegarder',
                                deleteText: 'Supprimer',
                                newText: 'Nouveau',
                                viewAllRepliesText: 'Afficher toutes les réponses',
                                hideRepliesText: 'Cacher la réponse',
                                noCommentsText: 'Pas de commentaire',
                                noAttachmentsText: 'Pas d' / 'attachement',
                                attachmentDropText: 'Deposez les fichiers ici',
                                textFormatter: function (text) {
                                    return text
                                },

                                // Functionalities
                                enableReplying: true,
                                enableEditing: false,
                                enableUpvoting: false,
                                enableDeleting: true,
                                enableAttachments: false,
                                enableHashtags: true,
                                enablePinging: true,
                                enableDeletingCommentWithReplies: false,
                                // enable<a href="https://www.jqueryscript.net/tags.php?/Navigation/">Navigation</a>: true,
                                postCommentOnEnter: true,
                                forceResponsive: false,
                                readOnly: false,
                                defaultNavigationSortKey: 'newest',

                                // Colors
                                highlightColor: '#2793e6',
                                deleteButtonColor: '#C9302C',

                                scrollContainer: this.$el,
                                roundProfilePictures: false,
                                textareaRows: 2,
                                textareaRowsOnFocus: 2,
                                textareaMaxRows: 5,
                                maxRepliesVisible: 2,

                                fieldMappings: {
                                    id: 'id',
                                    parent: 'parent',
                                    created: 'created',
                                    modified: 'modified',
                                    content: 'content',
                                    file: 'file',
                                    fileURL: 'file_url',
                                    fileMimeType: 'file_mime_type',
                                    pings: 'pings',
                                    creator: 'creator',
                                    fullname: 'fullname',
                                    profileURL: 'profile_url',
                                    profilePictureURL: 'profile_picture_url',
                                    isNew: 'is_new',
                                    createdByAdmin: 'created_by_admin',
                                    createdByCurrentUser: 'created_by_current_user',
                                    upvoteCount: 'upvote_count',
                                    userHasUpvoted: 'user_has_upvoted'
                                },
                                searchUsers: function (term, success, error) {
                                    setTimeout(function () {
                                        success(usersArray.filter(function (user) {
                                            var containsSearchTerm = user.fullname.toLowerCase().indexOf(term.toLowerCase()) != -1;
                                            // var isNotSelf = '';
                                            // if ($("#employerconnecter").text() === user.fullname ){
                                            //     isNotSelf = "Vous";
                                            // }else{
                                            //     isNotSelf = user.fullname
                                            // }

                                            var isNotSelf = user.id != 1;
                                            // console.log(user.fullname)
                                            return containsSearchTerm && isNotSelf;
                                        }));
                                    }, 500);
                                },
                                // getUsers: function (success, error) {
                                //     // console.log('1');
                                //     setTimeout(function () {
                                //         success(usersArray);
                                //     }, 500);
                                // },
                                getComments: function (success, error) {
                                    setTimeout(function () {
                                        success(commentsArray);
                                        // success(usersArray);
                                    }, 500);
                                },
                                postComment: function (data, success, error) {
                                    // console.log(data);
                                    setTimeout(function () {
                                        success(saveComment(data));
                                    }, 500);
                                },
                                putComment: function (data, success, error) {
                                    // console.log('4');
                                    setTimeout(function () {
                                        success(updateComment(data));
                                    }, 500);
                                },
                                deleteComment: function (data, success, error) {
                                    // console.log('5');
                                    setTimeout(function () {
                                        success();
                                    }, 500);
                                },
                                upvoteComment: function (data, success, error) {
                                    setTimeout(function () {
                                        // success(data);
                                    }, 500);
                                },
                                uploadAttachments: function (dataArray, success, error) {


                                    console.log(dataArray);
                                    setTimeout(function () {
                                        success(dataArray);
                                    }, 500);
                                },

                            });


                        },
                        error: function (error) {

                        }
                    });


                },
                error: function (error) {

                    swal({
                        title: "Alert!",
                        icon: "error",
                        button: false,
                        text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                        timer: 1500
                    });
                }
            });

            btntraite.button('reset');
            ModalTraitementDemande.modal('show');
            // ModalTraitementDemande.modal({ show: 'fadeIn()', duration: 1000 });
        },
        error: function (error) {
            btntraite.button('reset');
            swal({
                title: "Alert!",
                icon: "error",
                button: false,
                text: "Erreur " + error + " ! Veuillez contacter l'administrateur ou réessayer !",
                timer: 1500
            });
        }
    })
});


$('#btnaccord').on('click', function (e) {

    e.preventDefault();

    var btnaccord = $(this);
    var iddemande = $('#iddemande').attr('data-iddemande');
    swal({
        title: "Confirmation !",
        text: "Voulez-vous vraiment donné votre accord ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function (isConfirm) {
        if (isConfirm) {
            btnaccord.button('loading');
            $.ajax({
                url: affectdemandeutilisateurUrl,
                type: "POST",
                data: {
                    iddeman: iddemande,
                    type: 'next'
                },
                success: function (data) {
                    btnaccord.button('reset');
                    swal({
                        title: "Alert!",
                        icon: "success",
                        button: false,
                        text: "Affectation effectuée avec succès !",
                        timer: 1500
                    });
                    ModalTraitementDemande.modal('hide');
                    btntraite.closest('td').closest('tr').fadeOut(2000, function () {
                        btntraite.closest('td').closest('tr').remove();
                    });
                },
                error: function (error) {
                    btnaccord.button('reset');

                }
            });
        }

    })
});


$('#btnrejet').on('click', function (e) {

    e.preventDefault();
    var btnaccord = $(this);
    var iddemande = $('#iddemande').attr('data-iddemande');
    swal({
        title: "Confirmation !",
        text: "Voulez-vous vraiment rejeté ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function (isConfirm) {
        if (isConfirm) {
            btnaccord.button('loading');
            $.ajax({
                url: affectdemandeutilisateurUrl,
                type: "POST",
                data: {
                    iddeman: iddemande,
                    type: 'back'
                },
                success: function (data) {
                    btnaccord.button('reset');
                    swal({
                        title: "Alert!",
                        icon: "success",
                        button: false,
                        text: "Affectation effectuée avec succès !",
                        timer: 1500
                    });
                    ModalTraitementDemande.modal('hide');
                    btntraite.closest('td').closest('tr').fadeOut(2000, function () {
                        btntraite.closest('td').closest('tr').remove();
                    });

                },
                error: function (error) {
                    btnaccord.button('reset');

                }
            });
        }

    })
});






